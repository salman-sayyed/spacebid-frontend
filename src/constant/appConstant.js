export function replaceProtocol(url) {
  if (typeof window !== "undefined") {
    if (window.location.protocol == "https:" && url.indexOf("http://") == 0) {
      return url.replace("http://", "https://");
    }
    if (window.location.protocol == "http:" && url.indexOf("https://") == 0) {
      return url.replace("https://", "http://");
    }
  }
  return url;
}
//export const baseUrl = "http://18.225.5.132:8080";
export const baseUrl = replaceProtocol("http://localhost:8090");
// export const baseUrl = replaceProtocol("http://13.71.54.132/api");
//export const baseUrl = replaceProtocol("https://spacebid.bajajfinserv.in/api");
//export const baseUrl = "http://104.215.197.19:8080";
//export const baseUrl = replaceProtocol("http://10.184.4.4:8089");
//export const baseUrl = replaceProtocol("http://3.16.186.119:8090");
//export const baseUrl ="52.163.115.193:8080"
