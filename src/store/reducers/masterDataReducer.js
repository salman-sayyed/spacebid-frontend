import {
  GET_MASTER_DATA_SUCCESS,
  POST_MASTER_DATA_SUCCESS
} from "../constants/actionType";
import store from "../store";

const initialState = {
  data: {
    Headquarter: [],
    Domain: [],
    FundingStage: [],
    KeyInvestor: [],
    SolutionCloudProvider: [],
    SolutionType: [],
    SolutionDomain: [],
    SolutionRelevance: [],
    SolutionTechnologyStack: [],
    Country: []
  }
};

const masterDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MASTER_DATA_SUCCESS:
      return { ...state, data: action.payload };
    case POST_MASTER_DATA_SUCCESS:
      let storeData = state.data;
      storeData[action.payload.key].push(action.payload.label);
      return { ...state, data: storeData };
    default:
      return state;
  }
};

export default masterDataReducer;
