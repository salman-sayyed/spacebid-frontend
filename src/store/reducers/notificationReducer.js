import {
  GET_NOTIFICATION_SUCCESS,
  UPDATE_NOTIFICATION_SUCCESS
} from "../constants/actionType";

const initialState = {
  data: []
};

const notificationDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_NOTIFICATION_SUCCESS:
      return { ...state, data: action.payload };

    case UPDATE_NOTIFICATION_SUCCESS:
      return { ...state, data: action.payload };

    default:
      return state;
  }
};

export default notificationDataReducer;
