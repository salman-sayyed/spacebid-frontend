/** @flow */

/** import action types */
import {
  ON_SUBMIT,
  ON_GET_SUCCESS,
  ON_SUBMIT_SUCCESS,
  ON_SUBMIT_ERROR,
  ON_SUBMIT_WARNING,
  ON_SUBMIT_INFO,
  ON_TOAST_SUCCESS,
  ON_SUBMIT_RESET,
  ON_SUBMIT_FILE_UPLOAD,
  ON_SUBMIT_FILE_UPLOAD_SUCCESS,
  ON_TEXT_TO_VOICE,
  ON_VOICE_ENABLED,
  ON_DARK_THEME_ENABLED,
  ON_SUBMIT_LOGIN,
  ON_SUBMIT_LOGOUT,
  ON_VOICE_TO_TEXT,
  ON_MICE_ENABLED,
  STOP_TEXT_TO_VOICE,
  STEP,
  IS_SOLUTION_SERVICE,
  IS_PRODUCT,
  FILE_UPLOAD_MESSAGE
} from "../actions/commonActions";

/** set common default state */
const initialState = {
  toastr: {
    type: "",
    message: "",
    title: ""
  },
  spinner: {
    isSubmitting: false
  },
  isUploading: false,
  uploadingContents: [],
  isVoiceEnabled: false,
  voiceText: "",
  voiceToText: "",
  isMiceEnabled: false,
  isLoggedIn: Boolean,
  isDarkThemeEnabled: false,
  isAutoTimeBasedThemeSetting: true,
  isProduct: false,
  isSolutionService: false,
  fileUploadMessage: ""
};

const commonReducer = (state = initialState, action) => {
  switch (action.type) {
    case ON_SUBMIT:
      return {
        ...state,
        spinner: {
          isSubmitting: true
        },
        toastr: {
          type: "",
          message: "",
          title: ""
        }
      };
    case STEP:
      return {
        ...state,
        step: action.payload
      };
    case ON_GET_SUCCESS:
      return {
        ...state,
        spinner: {
          isSubmitting: false
        },
        toastr: {
          type: "",
          message: "",
          title: ""
        }
      };
    case ON_SUBMIT_SUCCESS:
      return {
        ...state,
        toastr: {
          type: "SUCCESS",
          message: action.payload,
          title: action.title
        },
        spinner: {
          isSubmitting: false
        }
      };
    case ON_TOAST_SUCCESS:
      return {
        ...state,
        toastr: {
          type: "",
          message: "",
          title: ""
        },
        spinner: {
          isSubmitting: false
        }
      };
    case ON_SUBMIT_ERROR:
      return {
        ...state,
        toastr: {
          type: "ERROR",
          message: action.payload,
          title: action.title
        },
        spinner: {
          isSubmitting: false
        }
      };
    case ON_SUBMIT_RESET:
      return {
        ...state,
        toastr: {
          type: "",
          message: action.payload,
          title: action.title
        },
        spinner: {
          isSubmitting: false
        }
      };
    case ON_SUBMIT_WARNING:
      return {
        ...state,
        toastr: {
          type: "WARNING",
          message: action.payload,
          title: action.title
        },
        spinner: {
          isSubmitting: false
        }
      };
    case ON_SUBMIT_INFO:
      return {
        ...state,
        toastr: {
          type: "INFO",
          message: action.payload,
          title: action.title
        },
        spinner: {
          isSubmitting: false
        }
      };
    case ON_SUBMIT_FILE_UPLOAD:
      let existingUploadingContentList = state.uploadingContents;
      existingUploadingContentList.push(action.payload);
      return {
        ...state,
        spinner: {
          isSubmitting: true
        },
        uploadingContents: existingUploadingContentList
      };
    case ON_SUBMIT_FILE_UPLOAD_SUCCESS:
      // TODO remove successfull upload key from array
      let uploadingContentList = state.uploadingContents;
      uploadingContentList = uploadingContentList.filter(content => {
        return content !== action.payload;
      });
      return {
        ...state,
        spinner: {
          isSubmitting: false
        },
        uploadingContents: uploadingContentList
      };
    case ON_TEXT_TO_VOICE:
      return {
        ...state,
        voiceText: action.payload,
        name: action.name
      };
    case STOP_TEXT_TO_VOICE:
      return {
        ...state,
        stopText: action.payload,
        name: action.name
      };
    case ON_VOICE_ENABLED:
      return {
        ...state,
        isVoiceEnabled: action.payload
      };
    case ON_DARK_THEME_ENABLED:
      return {
        ...state,
        isDarkThemeEnabled: action.payload,
        isAutoTimeBasedThemeSetting: action.isAutoTimeBasedThemeSetting
      };
    case ON_SUBMIT_LOGIN:
      return {
        ...state,
        isLoggedIn: true
      };
    case ON_SUBMIT_LOGOUT:
      let isLoggedIn = false;
      if (state.isLoggedIn === false) {
        isLoggedIn = "";
      }
      return {
        ...state,
        isLoggedIn: isLoggedIn
      };
    case ON_MICE_ENABLED:
      return {
        ...state,
        isMiceEnabled: action.payload
      };
    case ON_VOICE_TO_TEXT:
      return {
        ...state,
        voiceToText: action.payload,
        name: action.name
      };
    case IS_PRODUCT:
      return {
        ...state,
        isProduct: action.isProduct
      };
    case IS_SOLUTION_SERVICE:
      return {
        ...state,
        isSolutionService: action.isSolutionService
      };
    case FILE_UPLOAD_MESSAGE:
      return {
        ...state,
        messages: action.messages
      };
    default:
      return state;
  }
};

export default commonReducer;
