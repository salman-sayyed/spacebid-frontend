import { combineReducers } from "redux";
import registerSpocReducer from "./registerSpocReducer";
import masterDataReducer from "./masterDataReducer";
import dashboardDataReducer from "./dashboardDataReducer";
import notificationDataReducer from "./notificationReducer";
import commonReducer from "./commonReducer";
import restrictedDataReducer from "./restrictedDataReducer";
import stepReducer from "./stepReducer";
import queryDataReducer from "./queryDataReducer";

export default combineReducers({
  registerSpocReducer: registerSpocReducer,
  masterDataReducer: masterDataReducer,
  dashboardDataReducer: dashboardDataReducer,
  notificationDataReducer: notificationDataReducer,
  commonReducer: commonReducer,
  restrictedDataReducer: restrictedDataReducer,
  stepReducer: stepReducer,
  queryDataReducer: queryDataReducer
});
