import { REGISTER_STARTUP_SUCCESS } from "../constants/actionType";

const initialState = {
  data: {
    documents: {
      optional: []
    }
  }
};

const uploadDocumentReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_STARTUP_SUCCESS:
      return { ...state, data: action.payload, isOtpVerified: true };
    default:
      return state;
  }
};

export default uploadDocumentReducer;
