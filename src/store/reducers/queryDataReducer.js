import {
  GET_QUERY_DATA_SUCCESS,
  GET_SELECTED_QUERY_DATA,
  POST_QUERY_DATA_SUCCESS
} from "../constants/actionType";

const initialState = {
  data: [],
  selectedQuery: {}
};

const queryDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_QUERY_DATA_SUCCESS:
      return { ...state, data: action.payload };
    case GET_SELECTED_QUERY_DATA:
      return { ...state, selectedQuery: action.payload };
    case POST_QUERY_DATA_SUCCESS:
      let existingQueryData = state.data;
      existingQueryData.push(action.payload);
      return { ...state, data: existingQueryData };
    default:
      return state;
  }
};

export default queryDataReducer;
