import {
  PERSONAL_FORM_NEXT_STEP,
  PERSONAL_FORM_PREV_STEP,
  COMPANY_FORM_PREV_STEP,
  COMPANY_FORM_NEXT_STEP,
  SOLUTION_FORM_PREV_STEP,
  SOLUTION_FORM_NEXT_STEP,
  PERSONAL_FORM_INITIAL_STEP
} from "../constants/actionType";

const initialState = {
  personalFormCurrentStep: 1,
  companyFormCurrentStep: 1,
  solutionFormCurrentStep: 1
};

const stepReducer = (state = initialState, action) => {
  switch (action.type) {
    case PERSONAL_FORM_NEXT_STEP:
      return {
        ...state,
        personalFormCurrentStep: state.personalFormCurrentStep + 1
      };
    case PERSONAL_FORM_INITIAL_STEP:
      return {
        ...state,
        personalFormCurrentStep: 1
      };
    case PERSONAL_FORM_PREV_STEP:
      return {
        ...state,
        personalFormCurrentStep:
          state.personalFormCurrentStep > 1
            ? state.personalFormCurrentStep - 1
            : 1
      };
    case COMPANY_FORM_NEXT_STEP:
      return {
        ...state,
        companyFormCurrentStep: state.companyFormCurrentStep + 1
      };
    case COMPANY_FORM_PREV_STEP:
      return {
        ...state,
        companyFormCurrentStep:
          state.companyFormCurrentStep > 1
            ? state.companyFormCurrentStep - 1
            : 1
      };
    case SOLUTION_FORM_NEXT_STEP:
      return {
        ...state,
        solutionFormCurrentStep: state.solutionFormCurrentStep + 1
      };
    case SOLUTION_FORM_PREV_STEP:
      return {
        ...state,
        solutionFormCurrentStep:
          state.solutionFormCurrentStep > 1
            ? state.solutionFormCurrentStep - 1
            : 1
      };
    default:
      return state;
  }
};

export default stepReducer;
