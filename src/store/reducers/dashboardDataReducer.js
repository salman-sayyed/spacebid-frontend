import { GET_DASHBOARD_DATA_SUCCESS } from "../constants/actionType";

const initialState = {
  data: {
    partners: [],
    socialLinks: {
      linkdin: "",
      facebook: "",
      twitter: "",
      crunchBase: ""
    },
    documents: {
      optional: []
    },
    spoc: [
      {
        solutions: []
      }
    ]
  },
  isOtpVerified: false
};

const dashboardDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DASHBOARD_DATA_SUCCESS:
      return { ...state, data: action.payload, isOtpVerified: true };
    default:
      return state;
  }
};

export default dashboardDataReducer;
