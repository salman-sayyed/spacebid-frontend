import { GET_RESTRICTED_DATA_SUCCESS } from "../constants/actionType";

const initialState = {
  data: []
};

const restrictedDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_RESTRICTED_DATA_SUCCESS:
      return {
        ...state,
        data: action.payload
      };

    default:
      return state;
  }
};

export default restrictedDataReducer;
