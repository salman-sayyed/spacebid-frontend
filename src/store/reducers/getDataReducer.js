import { GET_DATA_SUCCESS } from "../constants/getDataConstants";

const initialState = {
  data: []
};

const getDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DATA_SUCCESS:
      return { ...state, data: action.payload };
    default:
      return state;
  }
};

export default getDataReducer;
