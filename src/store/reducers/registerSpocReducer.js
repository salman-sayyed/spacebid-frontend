import {
  REGISTER_STARTUP_SUCCESS,
  PUT_COMPANY_DETAILS_DATA_SUCCESS,
  OTP_VERIFIED_SUCCESS
} from "../constants/actionType";

const initialState = {
  data: {},
  isOtpVerified: false
};

const registerSpocReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_STARTUP_SUCCESS:
      return { ...state, data: action.payload };
    case PUT_COMPANY_DETAILS_DATA_SUCCESS:
      return { ...state, data: action.payload };
    case OTP_VERIFIED_SUCCESS:
      return { ...state, isOtpVerified: action.payload };
    default:
      return state;
  }
};

export default registerSpocReducer;
