import {
  GET_QUERY_DATA_SUCCESS,
  GET_SELECTED_QUERY_DATA,
  POST_QUERY_DATA_SUCCESS
} from "../constants/actionType";
import * as httpClient from "./httpClient";
import store from "../store";
export function getQueryDataSuccess(data) {
  return {
    type: GET_QUERY_DATA_SUCCESS,
    payload: data
  };
}

export function postQueryDataSuccess(data) {
  return {
    type: POST_QUERY_DATA_SUCCESS,
    payload: data
  };
}

export function getSelectedQueryDataSucess(data) {
  return {
    type: GET_SELECTED_QUERY_DATA,
    payload: data
  };
}

export function getQueryData() {
  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  };
  return dispatch => {
    return httpClient.get("/queries/user", config).then(response => {
      if (response) {
        dispatch(getQueryDataSuccess(response.data));
      }
    });
  };
}

export function getSelectedQueryData(data) {
  return dispatch => {
    dispatch(getSelectedQueryDataSucess(data));
  };
}

export function replyQueryDetailData(query, replyText, spoc) {
  var body = {
    from: spoc.officialEmail,
    name: spoc.fullName,
    text: replyText,
    user: localStorage.getItem("userId"),
    isAdmin: false
  };
  return dispatch => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    };
    return httpClient
      .put("/queries/" + query._id, body, config)
      .then(response => {
        if (response) {
          dispatch(getSelectedQueryData(response.data));
        }
      });
  };
}

export function createQuerty(text, subject, spoc) {
  var body = {
    userId: localStorage.getItem("userId"),
    subject: subject,
    hasAttachement: false,
    isAdminRead: false,
    isUserRead: false,
    messages: {
      user: localStorage.getItem("userId"),
      from: spoc.officialEmail,
      name: spoc.fullName,
      text: text,
      isAdmin: false
    }
  };
  return dispatch => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    };
    return httpClient.post("/queries", body, config).then(response => {
      if (response) {
        dispatch(postQueryDataSuccess(response.data));
        store.dispatch(getQueryData());
      }
    });
  };
}

export function uploadDocument(file, spocId, queryId) {
  const config = {
    headers: {
      "content-type": "multipart/form-data",
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  };
  const formData = new FormData();
  formData.append("file", file[0]);
  formData.append("spocId", spocId);

  return dispatch => {
    return httpClient
      .put("/queries/" + queryId + "/attachment", formData, config)
      .then(response => {
        if (response) {
          dispatch(getSelectedQueryDataSucess(response.data));
        }
      });
  };
}
