import { CLOSE_BROWSER } from "../constants/actionType";
import * as httpClient from "./httpClient";

export function oncloseBrowser(data) {
  return {
    type: CLOSE_BROWSER,
    payload: data
  };
}

export function closeBrowser(data) {
  return dispatch => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    };
    return httpClient
      .post("/spocs/closeBrowser", data, config)
      .then(response => {
        if (response) {
        }
      });
  };
}
