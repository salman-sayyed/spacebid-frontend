import { GET_DASHBOARD_DATA_SUCCESS } from "../constants/actionType";
import * as httpClient from "./httpClient";

export function getDashboardDataSuccess(data) {
  return {
    type: GET_DASHBOARD_DATA_SUCCESS,
    payload: data
  };
}

export function getDashboardData() {
  return dispatch => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    };
    return httpClient
      .get(
        "/startup/" +
          localStorage.getItem("companyId") +
          "/spoc/" +
          localStorage.getItem("userId"),
        config
      )
      .then(response => {
        if (response) {
          dispatch(getDashboardDataSuccess(response.data));
        }
      });
  };
}
