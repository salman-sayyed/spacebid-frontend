import { GET_RESTRICTED_DATA_SUCCESS } from "../constants/actionType";
import * as httpClient from "./httpClient";

export function getRestrictedDataSuccess(data) {
  return {
    type: GET_RESTRICTED_DATA_SUCCESS,
    payload: data
  };
}

export function getRestrictedData() {
  return dispatch => {
    return httpClient.get("/restrictedDomains").then(response => {
      if (response) {
        dispatch(getRestrictedDataSuccess(response.data));
      }
    });
  };
}
