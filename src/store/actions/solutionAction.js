import { PUT_SOLUTION_DETAILS_DATA_SUCCESS } from "../constants/actionType";
import * as httpClient from "./httpClient";

export function solutionDetailsSuccess(data) {
  return {
    type: PUT_SOLUTION_DETAILS_DATA_SUCCESS,
    payload: data
  };
}

export function solutionDetails(data, step) {
  var body = {
    //companyId: "5db93c4510e9512ce7232983",
    //spocId: "5db93c4510e9512ce7232982"
    companyId: localStorage.getItem("companyId"),
    spocId: localStorage.getItem("userId")
  };
  if (step) {
    body["solutionPage"] = step;
  }
  if (step > 2) {
    body["solutionId"] = localStorage.getItem("solutionId");
  }

  if (data.solutionName && data.solutionName !== "") {
    body["name"] = data.solutionName;
  }
  if (data.type && data.type !== "") {
    let type = data.type.map(value => {
      return value.label;
    });
    body["type"] = type;
  }
  if (data.domainserved && data.domainserved !== "") {
    let domainserved = data.domainserved.map(value => {
      return value.label;
    });
    body["domain"] = domainserved;
  }
  if (data.solutionRelavent && data.solutionRelavent !== "") {
    let solutionRelavent = data.solutionRelavent.map(value => {
      return value.label;
    });
    body["relevance"] = solutionRelavent;
  }
  if (data.technologyStack && data.technologyStack !== "") {
    let technologyStack = data.technologyStack.map(value => {
      return value.label;
    });
    body["stack"] = technologyStack;
  }
  if (data.briefDecription && data.briefDecription !== "") {
    body["description"] = data.briefDecription;
  }
  if (data.challengeSolving && data.challengeSolving !== "") {
    body["challenges"] = data.challengeSolving;
  }
  if (data.uspsSolution && data.uspsSolution !== "") {
    body["usp"] = data.uspsSolution;
  }
  if (data.benefite && data.benefite !== "") {
    body["impact"] = data.benefite;
  }
  if (data.sofarImplementation && data.sofarImplementation !== "") {
    body["totalImplementations"] = data.sofarImplementation;
  }
  if (data.indiaImplementation && data.indiaImplementation !== "") {
    body["implementations"] = data.indiaImplementation;
  }
  if (data.timelineImplementation && data.timelineImplementation !== "") {
    body["implementationsTimeline"] = data.timelineImplementation;
  }
  if (data.commercialModel && data.commercialModel !== "") {
    body["commercialModels"] = data.commercialModel;
  }
  if (data.futureRoadmap && data.futureRoadmap !== "") {
    body["futureRoadmap"] = data.futureRoadmap;
  }

  if (data.clients) {
    data.clients.map(value => {
      body["clients"]
        ? body["clients"].push({
            name: value.clientName,
            country: value.country,
            url: value.websiteURL
          })
        : (body["clients"] = [
            {
              name: value.clientName,
              country: value.country,
              url: value.websiteURL
            }
          ]);
    });
  }
  if (data.providerName) {
    let providerName = data.providerName.map(value => {
      return value.label;
    });
    body["provider"] = {
      name: providerName,
      reference: data.references,
      count: data.count
    };
  }

  return dispatch => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    };
    if (
      localStorage.getItem("solutionId") &&
      localStorage.getItem("solutionId") !== "undefined"
    ) {
      const solutionId = localStorage.getItem("solutionId");
      return httpClient
        .put("/solution/" + solutionId, body, config)
        .then(response => {
          if (response) {
            // if (step === 2) {
            //   if(!localStorage.getItem("solutionId")) {localStorage.setItem("solutionId", response.data._id); }
            // }
          }
        });
    } else {
      return httpClient.post("/solution", body, config).then(response => {
        if (response) {
          if (step === 2) {
            if (
              !localStorage.getItem("solutionId") ||
              localStorage.getItem("solutionId") === "undefined"
            ) {
              localStorage.setItem("solutionId", response.data._id);
            }
          }
        }
      });
    }
  };
}
