import {
  GET_NOTIFICATION_SUCCESS,
  UPDATE_NOTIFICATION_SUCCESS
} from "../constants/actionType";
import * as httpClient from "./httpClient";
import store from "../store";
import { getDashboardData } from "./dashboardDataAction";

export function updateNotification(data) {
  return {
    type: UPDATE_NOTIFICATION_SUCCESS,
    payload: data
  };
}

export function updateNotificationData() {
  return dispatch => {
    return httpClient
      .put(
        "/notifications/updateNotification",
        { id: localStorage.getItem("userId") },
        {
          headers: { "content-type": "application/json" },
          Authorization: "Bearer " + localStorage.getItem("token")
        }
      )
      .then(
        async response => {
          if (response) {
            return store.dispatch(getNotificationData());
          }
        },
        error => console.log("An error occurred", error)
      );
  };
}

export function notificationDataSuccess(data) {
  return {
    type: GET_NOTIFICATION_SUCCESS,
    payload: data
  };
}

export function getNotificationData() {
  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  };
  return dispatch => {
    return httpClient
      .get(
        "/notifications/list?userId=" + localStorage.getItem("userId"),
        config
      )
      .then(
        async response => {
          if (response) {
            let { data } = response;
            let data1 = await dispatch(notificationDataSuccess(data));
            return data1;
          }
        },
        error => console.log("An error occurred", error)
      );
  };
}
