import { REGISTER_STARTUP_SUCCESS } from "../constants/actionType";

import * as httpClient from "./httpClient";
import store from "../store";
import {
  onSubmitFileUpload,
  onSubmitFileUploadSucess,
  onSubmitError,
  fileuploadMessage
} from "./commonActions";
import { updateCompanyDetailsSuccess } from "../../store/actions/updateCompanyAction";
import { getDashboardData } from "./dashboardDataAction";

export function uploadDocumentSuccess(data) {
  return {
    type: REGISTER_STARTUP_SUCCESS,
    payload: data
  };
}

export function uploadDocument(file, title, mandatory) {
  const config = {
    headers: {
      "content-type": "multipart/form-data",
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  };
  const formData = new FormData();
  if (mandatory === true) {
    formData.append("file", file);
    formData.append("title", title);
    formData.append("mandatory", mandatory);
    formData.append("spocId", localStorage.getItem("userId"));
    formData.append("companyId", localStorage.getItem("companyId"));
    formData.append("solutionId", localStorage.getItem("solutionId"));
  } else {
    formData.append("file", file);
    formData.append("title", title);
    formData.append("spocId", localStorage.getItem("userId"));
    formData.append("companyId", localStorage.getItem("companyId"));
    formData.append("solutionId", localStorage.getItem("solutionId"));
  }

  return dispatch => {
    return httpClient.post("/upload/solution", formData, config).then(
      response => {
        if (response.data) {
          dispatch(uploadDocumentSuccess(response.data));
          return "success";
        } else {
          return "error";
        }
      },
      error => console.log("an error", error)
    );
  };
}

export function uploadCompanyDocument(
  file,
  title,
  isMandatory,
  uploadingContentKey
) {
  store.dispatch(onSubmitFileUpload(uploadingContentKey));
  const config = {
    headers: {
      "content-type": "multipart/form-data",
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  };
  const formData = new FormData();
  if (isMandatory) {
    formData.append("file", file);
    formData.append("title", title);
    formData.append("mandatory", isMandatory);
    formData.append("companyId", localStorage.getItem("companyId"));
  } else {
    formData.append("file", file);
    formData.append("title", title);
    formData.append("companyId", localStorage.getItem("companyId"));
  }

  return dispatch => {
    return httpClient.post("/upload/company", formData, config).then(
      response => {
        if (response && response.data) {
          dispatch(updateCompanyDetailsSuccess(response.data));
          dispatch(getDashboardData());
          dispatch(onSubmitFileUploadSucess(uploadingContentKey));
          return "success";
        } else {
          return "error";
        }
      },
      error => {
        console.log(error);
      }
    );
  };
}
