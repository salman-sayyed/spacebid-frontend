import Axios from "axios";
import * as httpClient from "./httpClient";
import { getDashboardData } from "./dashboardDataAction";

export function deleteSolutionDocument(
  doc_id,
  mandatory,
  company_id,
  solution_id,
  spoc_id
) {
  return dispatch => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    };
    let url = "";
    if (mandatory) {
      url =
        "/upload/" +
        doc_id +
        "/solution" +
        "?mandatory=" +
        mandatory +
        "&companyId=" +
        company_id +
        "&solutionId=" +
        solution_id +
        "&spocId=" +
        spoc_id;
    } else {
      url =
        "/upload/" +
        doc_id +
        "/solution" +
        "?companyId=" +
        company_id +
        "&solutionId=" +
        solution_id +
        "&spocId=" +
        spoc_id;
    }
    return httpClient.remove(url, config).then(
      response => {
        dispatch(getDashboardData());
      },
      error => console.log("An error occurred", error)
    );
  };
}

export function deleteCompanyDocument(doc_id, mandatory, company_id) {
  return dispatch => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    };
    let url = "";
    if (mandatory) {
      url =
        "/upload/" +
        doc_id +
        "/company" +
        "?mandatory=" +
        mandatory +
        "&companyId=" +
        company_id;
    } else {
      url = "/upload/" + doc_id + "/company" + "?companyId=" + company_id;
    }
    return httpClient.remove(url, config).then(response => {
      if (response) {
        dispatch(getDashboardData());
      }
    });
  };
}
