import {
  PERSONAL_FORM_NEXT_STEP,
  PERSONAL_FORM_PREV_STEP,
  COMPANY_FORM_PREV_STEP,
  COMPANY_FORM_NEXT_STEP,
  SOLUTION_FORM_PREV_STEP,
  SOLUTION_FORM_NEXT_STEP,
  PERSONAL_FORM_INITIAL_STEP
} from "../constants/actionType";

export function onPersonalFormNextStepChange() {
  return {
    type: PERSONAL_FORM_NEXT_STEP
  };
}

export function onPersonalFormPrevStepChange() {
  return {
    type: PERSONAL_FORM_PREV_STEP
  };
}
export function onPersonalInitilStep() {
  return {
    type: PERSONAL_FORM_INITIAL_STEP
  };
}

export function onCompanyFormNextStepChange() {
  return {
    type: COMPANY_FORM_NEXT_STEP
  };
}

export function onCompanyFormPrevStepChange() {
  return {
    type: COMPANY_FORM_PREV_STEP
  };
}

export function onSolutionFormNextStepChange() {
  return {
    type: SOLUTION_FORM_NEXT_STEP
  };
}

export function onSolutionFormPrevStepChange() {
  return {
    type: SOLUTION_FORM_PREV_STEP
  };
}
