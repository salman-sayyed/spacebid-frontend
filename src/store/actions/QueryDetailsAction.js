import {
  GET_QUERY_DETAILS_DATA_SUCCESS,
  REPLY_QUERY_DATA_SUCCESS
} from "../constants/actionType";
import * as httpClient from "./httpClient";

import axios from "axios";

export function getQueryDetailsDataSuccess(data) {
  return {
    type: GET_QUERY_DETAILS_DATA_SUCCESS,
    payload: data
  };
}

export function replyQueryDetailsDataSuccess(data) {
  return {
    type: REPLY_QUERY_DATA_SUCCESS,
    payload: data
  };
}

export function getQueryDetailData(queryId) {
  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  };
  return dispatch => {
    return httpClient.get("/queries?id=" + queryId, config).then(response => {
      if (response) {
        dispatch(getQueryDetailsDataSuccess(response.data.messages));
      }
    });
  };
}
