import axios from "axios";
import { baseUrl } from "../../constant/appConstant";
import {
  onSubmitError,
  onLogoutSuccess,
  onTextToVoice,
  fileuploadMessage
} from "./commonActions";
import store from "../store";

export function post(url, body, config) {
  if (!config) {
    config = {
      headers: {
        "Content-Type": "application/json"
      }
    };
  }
  return axios
    .post(baseUrl + url, body, config)
    .then(response => {
      return response;
    })
    .catch(error => {
      if (error.response && error.response.status === 401) {
        localStorage.clear();
        store.dispatch(
          onSubmitError("Error!", "Unauthorized User...Logging out")
        );
        store.dispatch(onTextToVoice("Unauthorized User...Logging out"));
        store.dispatch(onLogoutSuccess());
        return;
      } else if (error.response && error.response.status === 422) {
        store.dispatch(
          onSubmitError("Error!", error.response.data.errors[0].msg)
        );
        store.dispatch(onTextToVoice(error.response.data.errors[0].msg));
        return;
      } else {
        store.dispatch(onSubmitError("Error!", error.response.data.message));
        store.dispatch(onTextToVoice(error.response.data.message));
        return;
      }
    });
}

export function put(url, body, config) {
  if (!config) {
    config = {
      headers: {
        "Content-Type": "application/json"
      }
    };
  }
  return axios
    .put(baseUrl + url, body, config)
    .then(response => {
      return response;
    })
    .catch(error => {
      if (error.response && error.response.status === 401) {
        localStorage.clear();
        store.dispatch(onSubmitError("Error!", "Please login"));
        store.dispatch(onTextToVoice("Please login"));
        store.dispatch(onLogoutSuccess());
        return;
      } else if (error.response && error.response.status === 422) {
        store.dispatch(
          onSubmitError("Error!", error.response.data.errors[0].msg)
        );
        store.dispatch(onTextToVoice(error.response.data.errors[0].msg));
        return;
      } else if (error.response && error.response.status === 400) {
        store.dispatch(onSubmitError("Error!", error.response.data.message));
      }
    });
}

export function get(url, config) {
  if (!config) {
    config = {
      headers: {
        "Content-Type": "application/json"
      }
    };
  }
  return axios
    .get(baseUrl + url, config)
    .then(response => {
      return response;
    })
    .catch(error => {
      if (error.response && error.response.status === 401) {
        localStorage.clear();
        store.dispatch(onSubmitError("Error!", "Please login"));
        store.dispatch(onTextToVoice("Please login"));
        store.dispatch(onLogoutSuccess());
        return;
      }
    });
}

export function remove(url, config) {
  return axios
    .delete(baseUrl + url, config)
    .then(response => {
      return response;
    })
    .catch(error => {
      if (error.response && error.response.status === 401) {
        localStorage.clear();
        store.dispatch(onSubmitError("Error!", "Please login"));
        store.dispatch(onTextToVoice("Please login"));
        store.dispatch(onLogoutSuccess());
        return;
      }
    });
}
