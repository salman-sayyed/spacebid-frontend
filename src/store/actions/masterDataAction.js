import {
  GET_MASTER_DATA_SUCCESS,
  POST_MASTER_DATA_SUCCESS
} from "../constants/actionType";
import * as httpClient from "./httpClient";
import store from "../store";
import masterDataReducer from "../reducers/masterDataReducer";

export function getMasterDataSuccess(data) {
  return {
    type: GET_MASTER_DATA_SUCCESS,
    payload: data
  };
}

export function postMasterDataSuccess(data) {
  return {
    type: POST_MASTER_DATA_SUCCESS,
    payload: data
  };
}

export function getMasterData(keys) {
  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  };
  return dispatch => {
    return httpClient
      .get("/masterdata/list?stringValue=true&keys=" + keys, config)
      .then(response => {
        if (response) {
          dispatch(getMasterDataSuccess(response.data));
        }
      });
  };
}

export function postMasterData(key, value) {
  const body = {
    label: value,
    value: value
  };

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  };

  return dispatch => {
    return httpClient
      .post("/masterData/list?key=" + key, body, config)
      .then(response => {
        if (response) {
          response.data.key = key;

          dispatch(postMasterDataSuccess(response.data));
        }
      });
  };
}
