import { POST_SUBMIT_FORM__DATA_SUCCESS } from "../constants/actionType";
import * as httpClient from "./httpClient";

export function submitFormSuccess(data) {
  return {
    type: POST_SUBMIT_FORM__DATA_SUCCESS,
    payload: data
  };
}

export function submitForm(data) {
  return dispatch => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    };
    return httpClient
      .put("/spocs/" + data.spoc[0]._id + "/submit", data, config)
      .then(response => {
        if (response) {
        }
      });
  };
}
