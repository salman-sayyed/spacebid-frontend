import { PUT_COMPANY_DETAILS_DATA_SUCCESS } from "../constants/actionType";
import * as httpClient from "./httpClient";
import store from "../store";
import { isSolutionService, isProduct } from "./commonActions";
import { getDashboardData } from "./dashboardDataAction";
export function updateCompanyDetailsSuccess(data) {
  return {
    type: PUT_COMPANY_DETAILS_DATA_SUCCESS,
    payload: data
  };
}

export function updateCompanyDetails(data) {
  var body = {
    spocId: localStorage.getItem("userId"),
    socialLinks: {
      facebook: data.facebook && data.facebook != "" ? data.facebook : "",
      linkdin: data.linkdin && data.linkdin != "" ? data.linkdin : "",
      twitter: data.twitter && data.twitter != "" ? data.twitter : "",
      crunchBase:
        data.crunchBase && data.crunchBase != "" ? data.crunchBase : "",
      other: data.otherLink && data.otherLink != "" ? data.otherLink : ""
    }
  };
  if (data.step) {
    body["companyPage"] = data.step;
  }
  if (data.partners) {
    data.partners.map(value => {
      body["partners"]
        ? body["partners"].push({
            name: value.partnerName,
            location: value.location,
            url: value.websiteURL2
          })
        : (body["partners"] = [
            {
              name: value.partnerName,
              location: value.location,
              url: value.websiteURL2
            }
          ]);
    });
  }

  if (
    data.headquarter &&
    data.headquarter !== "" &&
    data.headquarter !== "Select"
  ) {
    body["headquarter"] = data.headquarter;
  }
  if (data.isProduct || data.isSolutionServices) {
    let value = [];
    if (data.isProduct) {
      value.push("Product");
    }
    if (data.isSolutionServices) {
      value.push("Solution Service");
    }
    body["companyType"] = value;
  }
  if (data.keyBusinessArea) {
    let keyBusinessArea = data.keyBusinessArea.map(option => {
      return option.label;
    });
    body["keyBusinessArea"] = keyBusinessArea;
  }
  if (data.keyTechnologyArea) {
    let keyTechnologyArea = data.keyTechnologyArea.map(option => {
      return option.label;
    });
    body["keyTechnologyArea"] = keyTechnologyArea;
  }
  if (data.companyDomain) {
    let companyData = data.companyDomain.map(value => {
      return value.label;
    });
    body["domains"] = companyData;
  }
  if (data.operational) {
    if (data.operational === true) {
      body["isOperationalInIndia"] = "Yes";
    }
    if (data.operational === false) {
      body["isOperationalInIndia"] = "No";
    }
  }
  if (data.websiteURL && data.websiteURL !== "") {
    body["website"] = data.websiteURL;
  }
  if (data.incorporation && data.incorporation !== "0000") {
    body["incorporationYear"] = data.incorporation;
  }
  if (data.employees && data.employees !== "") {
    body["employeeCount"] = data.employees;
  }
  if (data.aboutCompany && data.aboutCompany !== "") {
    body["aboutCompany"] = data.aboutCompany;
  }
  if (
    data.stageFunding &&
    data.stageFunding !== "" &&
    data.stageFunding !== "Select"
  ) {
    body["fundingStage"] = data.stageFunding;
  }
  if (data.capitalCurrency && data.capitalCurrency !== "") {
    body["capitalCurrency"] = data.capitalCurrency;
  }
  if (data.capitalRaised && data.capitalRaised !== "") {
    body["capital"] = data.capitalRaised;
  }
  if (data.currencyIn && data.currencyIn !== "") {
    body["currencyIn"] = data.currencyIn;
  }

  if (data.keyInvestors && data.keyInvestors !== "") {
    let investors = data.keyInvestors.map(option => {
      return option.label;
    });
    body["investors"] = investors;
  }
  if (data.step) {
    body["page"] = data.step;
  }

  // if (data.facebook && data.facebook !== "") {
  //   body["facebook"] = data.facebook;
  // }
  // if (data.linkedIn && data.linkedIn !== "") {
  //   body["linkdin"] = data.linkedIn;
  // }
  // if (data.twitter && data.twitter !== "") {
  //   body["twitter"] = data.twitter;
  // }
  // if (data.crunchBase && data.crunchBase !== "") {
  //   body["crunchBase"] = data.crunchBase;
  // }

  return dispatch => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    };
    return httpClient
      .put("/startup/" + localStorage.getItem("companyId"), body, config)
      .then(response => {
        if (response) {
          dispatch(updateCompanyDetailsSuccess(response.data));
          store.dispatch(getDashboardData());
        }
      });
  };
}
