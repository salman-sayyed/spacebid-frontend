import { PUT_COMPANY_DETAILS_DATA_SUCCESS } from "../constants/actionType";

import axios from "axios";
import { baseUrl } from "../../../src/constant/appConstant";
import * as httpClient from "./httpClient";
export function updateCompanyDetailsSuccess(data) {
  return {
    type: PUT_COMPANY_DETAILS_DATA_SUCCESS,
    payload: data
  };
}

export function updateSpocDetails(data, spocId, companyId) {
  var body = {};
  if (data.fullName) {
    body["fullName"] = data.fullName;
  }
  if (data.designation) {
    body["designation"] = data.designation;
  }
  if (data.mobile) {
    body["mobile"] = data.mobile;
  }
  if (data.alternativeMobileNumber) {
    body["alternativeMobileNumber"] = data.alternativeMobileNumber;
  }
  if (data.alternativeEmail && data.alternativeEmail !== "") {
    body["alternativeEmail"] = data.alternativeEmail;
  }
  if (data.name) {
    body["name"] = data.name;
  }
  if (data.officialEmail) {
    body["officialEmail"] = data.officialEmail;
  }
  if (data.consent) {
    body["consent"] = data.consent;
  }
  if (companyId) {
    body["companyId"] = companyId;
  }

  return dispatch => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    };
    return httpClient.put("/spocs/" + spocId, body, config).then(response => {
      if (response) {
        dispatch(updateCompanyDetailsSuccess(response.data));
      }
    });
  };
}
