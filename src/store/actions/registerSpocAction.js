import {
  REGISTER_STARTUP_SUCCESS,
  OTP_VERIFIED_SUCCESS
} from "../constants/actionType";

import * as httpClient from "./httpClient";
import { getDashboardData } from "./dashboardDataAction";
import { onPersonalFormNextStepChange } from "./stepAction";
import { onTextToVoice } from "./commonActions";
import { onLoginSuccess, onSubmitError } from "./commonActions";
import store from "../../store/store";

export function registerStartupSuccess(data) {
  return {
    type: REGISTER_STARTUP_SUCCESS,
    payload: data
  };
}

export function otpVerifiedSuccess() {
  return {
    type: OTP_VERIFIED_SUCCESS,
    payload: true
  };
}

const axiosConfig = {
  headers: {
    "Content-Type": "application/json"
  }
};

export function registerStartupData(data) {
  var body = {};
  if (data.fullName) {
    body["fullName"] = data.fullName;
  }
  if (data.designation) {
    body["designation"] = data.designation;
  }
  if (data.mobile) {
    body["mobile"] = data.mobile;
  }
  if (data.alternativeMobileNumber) {
    body["alternativeMobileNumber"] = data.alternativeMobileNumber;
  }
  if (data.alternativeEmail && data.alternativeEmail !== "") {
    body["alternativeEmail"] = data.alternativeEmail;
  }
  if (data.name) {
    body["name"] = data.name;
  }
  if (data.officialEmail) {
    body["officialEmail"] = data.officialEmail;
  }
  if (data.consent) {
    body["consent"] = data.consent;
  }

  return dispatch => {
    return httpClient.post("/startup/register", body, axiosConfig).then(
      response => {
        if (response) {
          dispatch(registerStartupSuccess(response.data));
          dispatch(onPersonalFormNextStepChange());
          dispatch(
            onTextToVoice(
              "A verification code is probably sitting in your inbox now - please enter the code"
            )
          );
          localStorage.setItem("emailId", data.officialEmail);
          localStorage.setItem("token", response.data.token);
        }
      },
      error => console.log("an error", error)
    );
  };
}

export function verifyOtp(otp, isLogin) {
  let body = {
    email: localStorage.getItem("emailId"),
    otp: otp
  };
  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  };
  if (!isLogin) {
    body.type = "isRegister";
  }
  return dispatch => {
    return httpClient.post("/login/verify", body, config).then(response => {
      if (response) {
        if (response.status === 200) {
          localStorage.setItem("userId", response.data.userId);
          localStorage.setItem("companyId", response.data.companyId);
          localStorage.setItem("userName", response.data.spocName);
          localStorage.setItem("token", response.data.token);
          localStorage.setItem(
            "expiresAt",
            new Date().getTime() + response.data.expiresIn * 1000
          );

          dispatch(onLoginSuccess());
          //console.log(response.data)
          //store.dispatch(onSubmitError("Error!","hey"))
          if (
            response &&
            response.data.message ===
              "Your OTP is incorrect. Please enter correct one."
          ) {
            store.dispatch(onSubmitError("Error!", response.data.message));
            store.dispatch(onTextToVoice("response.data.message"));
          } else {
            dispatch(getDashboardData());
            if (!isLogin) {
              store.dispatch(onPersonalFormNextStepChange());
            }
          }
        }
      }
    });
  };
}
