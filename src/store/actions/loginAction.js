import * as httpClient from "./httpClient";
import { onLogoutSuccess, onSubmitError } from "./commonActions";
import store from "../../store/store";
import { onPersonalInitilStep } from "./stepAction";

export function sendotp(emailId) {
  const body = {
    email: emailId
  };
  return dispatch => {
    return httpClient.post("/login", body).then(response => {
      if (response) {
        localStorage.setItem("token", response.data.token);
        localStorage.setItem("emailId", emailId);
        return { status: true };
      } else if (response === undefined) {
        return { status: false };
      }
    });
  };
}

export function logout() {
  store.dispatch(onLogoutSuccess());
  store.dispatch(onPersonalInitilStep());
  localStorage.clear();
}

export function resendOtp(email, value) {
  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  };
  // console.log(value);
  var body = "";
  if (value === "isRegister") {
    body = {
      email: localStorage.getItem("emailId"),
      isRegister: true
    };
  } else {
    body = {
      email: localStorage.getItem("emailId")
    };
  }
  return dispatch => {
    return httpClient.post("/login/resend", body, config).then(response => {
      if (response) {
        if (response.data.otpCount === 0) {
          store.dispatch(onSubmitError("Error!", response.data.message));
          setTimeout(() => {
            const urls = window.location.href;
            const origins = window.location.origin;
            window.location.replace(origins);
          }, 1000);
        } else if (response.data.message) {
          store.dispatch(onSubmitError("Error!", response.data.message));
        }
      }
    });
  };
}

export function isUserLoggedIn() {
  if (localStorage.getItem("token")) {
    if (new Date().getTime() < parseInt(localStorage.getItem("expiresAt"))) {
      return true;
    }
  }

  return false;
}
