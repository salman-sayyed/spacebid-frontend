export const ON_SUBMIT = "ON_SUBMIT";
export const ON_SUBMIT_SUCCESS = "ON_SUBMIT_SUCCESS";
export const ON_SUBMIT_ERROR = "ON_SUBMIT_ERROR";
export const ON_SUBMIT_WARNING = "ON_SUBMIT_WARNING";
export const ON_GET_SUCCESS = "ON_GET_SUCCESS";
export const ON_TOAST_SUCCESS = "ON_TOAST_SUCCESS";
export const ON_NOTIFICATION_RECEIVED = "ON_NOTIFICATION_RECEIVED";
export const ON_SUBMIT_INFO = "ON_SUBMIT_INFO";
export const ON_SUBMIT_RESET = "ON_SUBMIT_RESET";
export const ON_SUBMIT_FILE_UPLOAD = " ON_SUBMIT_FILE_UPLOAD";
export const ON_SUBMIT_FILE_UPLOAD_SUCCESS = " ON_SUBMIT_FILE_UPLOAD_SUCCESS";
export const ON_TEXT_TO_VOICE = "ON_TEXT_TO_VOICE";
export const STOP_TEXT_TO_VOICE = "STOP_TEXT_TO_VOICE";
export const ON_VOICE_ENABLED = "ON_VOICE_ENABLED";
export const ON_DARK_THEME_ENABLED = "ON_DARK_HEME_ENABLED";
export const ON_SUBMIT_LOGIN = "ON_SUBMIT_LOGIN";
export const ON_SUBMIT_LOGOUT = "ON_SUBMIT_LOGOUT";
export const ON_VOICE_TO_TEXT = "ON_VOICE_TO_TEXT";
export const ON_MICE_ENABLED = "ON_MICE_ENABLED";
export const STEP = "STEP";
export const IS_PRODUCT = "IS_PRODUCT";
export const IS_SOLUTION_SERVICE = "IS_SOLUTION_SERVICE";
export const FILE_UPLOAD_MESSAGE = "FILE_UPLOAD_MESSAGE";
/**
 * On Submit action to show the spinner
 * @returns {{type: string}}
 */
export function onSubmit() {
  return {
    type: ON_SUBMIT
  };
}

/**
 * On Submit action to show the spinner
 * @returns {{type: string}}
 */
export function onGetSuccess() {
  return {
    type: ON_GET_SUCCESS
  };
}

/**
 * On Submit Success Action to hide the spinner
 * @param title
 * @param message
 * @returns {{type: string, payload: *, title: *}}
 */
export function onSubmitSuccess(title, message) {
  return {
    type: ON_SUBMIT_SUCCESS,
    payload: message,
    title: title
  };
}

/**
 * On Toast Success Action to hide the Toastr
 * @returns {{type: string}}
 */
export function onToastSuccess() {
  return {
    type: ON_TOAST_SUCCESS
  };
}

/**
 * On Submit Error action to show the server side errors on Submit
 * @param title
 * @param message
 * @returns {{type: string, payload: *, title: *}}
 */
export function onSubmitError(title, message) {
  return {
    type: ON_SUBMIT_ERROR,
    payload: message,
    title: title
  };
}

/**
 * On Submit Error action to show the server side errors on Submit
 * @param title
 * @param message
 * @returns {{type: string, payload: *, title: *}}
 */
export function onSubmitReset() {
  return {
    type: ON_SUBMIT_RESET,
    payload: "",
    title: ""
  };
}

/**
 *  On Submit Warning action to show the warnings before submitting the request to server
 * @param title
 * @param message
 * @returns {{type: string, payload: *, title: *}}
 */
export function onSubmitWarning(title, message) {
  return {
    type: ON_SUBMIT_WARNING,
    payload: message,
    title: title
  };
}

/**
 *  On Submit Warning action to show the warnings before submitting the request to server
 * @param title
 * @param message
 * @returns {{type: string, payload: *, title: *}}
 */
export function onSubmitInfo(title, message) {
  return {
    type: ON_SUBMIT_INFO,
    payload: message,
    title: title
  };
}

export function onSubmitFileUpload(key) {
  return {
    type: ON_SUBMIT_FILE_UPLOAD,
    payload: key
  };
}

export function onSubmitFileUploadSucess(key) {
  return {
    type: ON_SUBMIT_FILE_UPLOAD_SUCCESS,
    payload: key
  };
}

export function onVoiceEnabled(flag) {
  return {
    type: ON_VOICE_ENABLED,
    payload: flag
  };
}
export function onSetStep(step) {
  return {
    type: STEP,
    payload: step
  };
}

export function onDarkThemeEnabled(flag, isAutoTimeBasedThemeSetting) {
  return {
    type: ON_DARK_THEME_ENABLED,
    payload: flag,
    isAutoTimeBasedThemeSetting: isAutoTimeBasedThemeSetting
  };
}

export function onTextToVoice(text) {
  return {
    type: ON_TEXT_TO_VOICE,
    payload: text
  };
}
export function stopTextToVoice(flag) {
  return {
    type: STOP_TEXT_TO_VOICE,
    payload: flag
  };
}
export function onLoginSuccess() {
  return {
    type: ON_SUBMIT_LOGIN
  };
}

export function onLogoutSuccess() {
  return {
    type: ON_SUBMIT_LOGOUT
  };
}

export function onMiceEnabled(flag) {
  return {
    type: ON_MICE_ENABLED,
    payload: flag
  };
}

export function onVoiceToText(voice, name) {
  return {
    type: ON_VOICE_TO_TEXT,
    payload: voice,
    name: name
  };
}
export function isProduct(flag) {
  return {
    type: IS_PRODUCT,
    isProduct: flag
  };
}
export function isSolutionService(flag) {
  return {
    type: IS_SOLUTION_SERVICE,
    isSolutionService: flag
  };
}

export function fileuploadMessage(messages) {
  return {
    type: FILE_UPLOAD_MESSAGE,
    messages: messages
  };
}
