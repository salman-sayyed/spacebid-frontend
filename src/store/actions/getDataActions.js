import { GET_DATA_SUCCESS } from "../constants/getDataConstants";

export function getDataSuccess(data) {
  return {
    type: GET_DATA_SUCCESS,
    payload: data
  };
}

export function getData() {
  return function(dispatch) {
    return fetch("https://demo5893308.mockable.io/dynamic-form").then(
      response => {
        dispatch(getDataSuccess(response));
      },
      error => console.log("An error occurred", error)
    );
  };
}
