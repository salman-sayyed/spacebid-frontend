import React, { Component } from "react";
import "./LoginForm.scss";
import LeftSideDrawer from "../common/leftSideDrawer/LeftSideDrawer";
import ButtonToggle from "../common/buttonToggle/ButtonToggle";
import Stars from "../common/stars/Stars";
import "../common/stars/Stars.scss";
import Clouds from "../common/clouds/Clouds";
import "../common/clouds/Clouds.scss";
import AOS from "aos";
import "aos/dist/aos.css";
import store from "../../store/store";
import ComponentGenerator from "../common/formDetails/componentGenerator/ComponentGenerator";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { FormBuilder, FieldGroup, Validators } from "react-reactive-form";
import { sendotp, resendOtp } from "../../store/actions/loginAction";
import { verifyOtp } from "../../store/actions/registerSpocAction";
import Validator from "../common/leftSideDrawer/Validator";
import {
  onSubmitError,
  onVoiceEnabled
} from "../../store/actions/commonActions";

const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));
const isEmpty = (control: AbstractControl) => {
  return sleep(1000).then(() => {
    if (control.value === "") {
      throw { notExist: true };
    } else {
      return null;
    }
  });
};

AOS.init({
  delay: 100,
  mirror: true
});
class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollValue: 0,
      userName: "",
      email: "",
      password: "",
      showSigin: false,
      isLoggedIn: localStorage.getItem("isLoggedIn")
    };
  }
  componentDidMount() {
    store.dispatch(onVoiceEnabled(false));
  }
  componentWillReceiveProps(nextProps) {
    if (!nextProps.isLoggedIn) {
      return;
    }
    if (nextProps.isOtpVerified === true) {
      if (
        nextProps.dashboardData.spoc &&
        nextProps.dashboardData.spoc[0] &&
        nextProps.dashboardData.spoc[0].formStatus === "drafted"
      ) {
        if (
          nextProps.dashboardData.spoc[0].companyPage >= 0 &&
          nextProps.dashboardData.spoc[0].companyPage !== 8
        ) {
          this.props.history.push("/company-form");
        } else if (
          nextProps.dashboardData.spoc[0].solutions[0] &&
          nextProps.dashboardData.spoc[0].solutions[0].solutionPage >= 0 &&
          nextProps.dashboardData.spoc[0].solutions[0].isCompleted === false
        ) {
          if (nextProps.dashboardData.spoc[0].solutions[0].solutionPage >= 0) {
            localStorage.setItem(
              "solutionId",
              nextProps.dashboardData.spoc[0].solutions[0]._id
            );
          }

          this.props.history.push("/solution-form");
        } else if (nextProps.dashboardData.spoc[0].companyPage === 8) {
          this.props.history.push("/solution-form");
        }
      } else {
        this.props.history.push("/dashboard");
      }
    }
  }

  subscription = () => {
    this.loginForm.get("email").valueChanges.subscribe(value => {
      this.setState({
        email: value
      });
    });
    this.loginForm.get("password").valueChanges.subscribe(value => {
      this.setState({
        password: value
      });
    });
  };

  loginForm = FormBuilder.group({
    email: ["", Validators.required, isEmpty],
    password: ["", Validators.required, isEmpty]
  });

  handleClick = async () => {
    if (this.state.email === "") {
      store.dispatch(
        onSubmitError("Error !", "Email field should not be empty")
      );
    } else {
      let res = await store.dispatch(sendotp(this.state.email));
      if (res.status) this.setState({ showSigin: true });
    }
  };

  handleResendOtpClick = () => {
    store.dispatch(onSubmitError("Error !", "OTP is sent again"));
    store.dispatch(resendOtp(this.state.email, "isLogin"));
  };

  handleVerifyOtpClick = () => {
    if (this.state.email === "") {
      store.dispatch(
        onSubmitError("Error !", "Email field should not be empty")
      );
    } else if (this.state.password === "") {
      store.dispatch(onSubmitError("Error !", "OTP field should not be empty"));
    } else {
      store.dispatch(verifyOtp(this.state.password, true));
    }
  };

  render() {
    this.subscription();
    let optButton;
    if (this.state.showSigin) {
      optButton = (
        <>
          <button
            onClick={this.handleVerifyOtpClick.bind(this)}
            // disabled={this.state.password.length > 0 ? false : true}
          >
            Sign in
          </button>
          {this.props.toastr && this.props.toastr.type === "" ? (
            <div className="Validation resend">
              <div className="resendOTP">
                <div className="validationmsgContainer">
                  <div className="validationmsg">
                    Didn't receive verification Code?
                    <div
                      className="resendLink"
                      onClick={this.handleResendOtpClick.bind(this)}
                      style={{ cursor: "pointer" }}
                    >
                      {" "}
                      Resend Now
                    </div>
                  </div>

                  <img
                    src={require("../../assets/images/clippy.png")}
                    alt="Validation"
                  />
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </>
      );
    } else {
      optButton = (
        <button onClick={this.handleClick.bind(this)}>Send OTP</button>
      );
    }

    return (
      <div className="loginForm">
        <div
          className={
            this.props.isDarkThemeEnabled ? "dark-theme" : "light-theme"
          }
        >
          <div className="topBgGradient"></div>
          <nav>
            <LeftSideDrawer />
            <ButtonToggle checkedValue={this.checkedValue} type="theme" />
          </nav>
          <Stars />
          <Clouds />
          {/* <div>Error Message: {this.props.toastr.message}</div> */}

          <div className="loginForm-Container">
            <Validator />
            <div className="loginForm-box">
              <FieldGroup
                key="loginForm"
                control={this.loginForm}
                render={({ get, invalid }) => (
                  <form>
                    <div className="emailotp">
                      <ComponentGenerator
                        type="input"
                        placeholder="Enter Email Address"
                        name="email"
                        label="Email Address"
                        inputType="text"
                        maxlength={100}
                      />
                    </div>
                    <div className="emailotp">
                      <ComponentGenerator
                        type="input"
                        placeholder="******"
                        name="password"
                        label="Enter OTP"
                        inputType="password"
                        maxlength={6}
                      />
                    </div>
                  </form>
                )}
              />
              {/* <div className="forgotLink">
                <a href="">Forgot Password</a>
              </div> */}

              <div className="signinBtn">
                {optButton}
                {/* <button
                  onClick={this.handleVerifyOtpClick.bind(this)}
                  // disabled={this.state.password.length > 0 ? false : true}
                >
                  Sign in
                </button> */}
              </div>
              {/* <div className="cancelBtn">
                <a href="">Cancel</a>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    );
  }

  componentWillUnmount() {
    this.loginForm.get("email").valueChanges.unsubscribe();
    this.loginForm.get("password").valueChanges.unsubscribe();
  }
}

LoginForm.contextTypes = {
  router: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    isLoggedIn: state.commonReducer.isLoggedIn,
    isOtpVerified: state.dashboardDataReducer.isOtpVerified,
    toastr: state.commonReducer.toastr,
    voiceText: state.commonReducer.voiceText,
    dashboardData: state.dashboardDataReducer.data,
    isDarkThemeEnabled: state.commonReducer.isDarkThemeEnabled
  };
};

export default connect(mapStateToProps)(LoginForm);
