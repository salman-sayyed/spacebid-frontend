import React, { Component } from 'react'
import Slider from "infinite-react-carousel";
import "./Carousel.scss"

export class Carousel extends Component {
    render() {
        const setting  ={
            dots :true,
            arrows :false,
            autoplay : true,
            autoplaySpeed : 5000
        }
        return (
            <div className="carousel">
                <Slider {...setting}>
                    <div>
                        <img src={require("../../assets/images/TIC/screen/screen1.png")} />
                    </div>
                    <div>
                        <img src={require("../../assets/images/TIC/screen/screen2.png")} />
                    </div>
                    <div>
                        <img src={require("../../assets/images/TIC/screen/screen3.png")} />
                    </div>
                    <div>
                        <img src={require("../../assets/images/TIC/screen/screen4.png")} />
                    </div>
                </Slider>
            </div>
        )
    }
}

export default Carousel
