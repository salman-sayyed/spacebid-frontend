import React, { Component } from "react";
import "./LearnAboutTIC.scss";
import LeftSideDrawer from "../common/leftSideDrawer/LeftSideDrawer";
import ButtonToggle from "../common/buttonToggle/ButtonToggle";
import Stars from "../common/stars/Stars";
import Clouds from "../common/clouds/Clouds";
import "../common/stars/Stars.scss";
import "../common/clouds/Clouds.scss";
import AOS from "aos";
import "aos/dist/aos.css";
import { connect } from "react-redux";
import InovationSlider from "../common/carouselInovation/CarouselInovation";
import Carousel from "./Carousel";

AOS.init({
  delay: 100,
  mirror: true
});

class LearnAboutTIC extends Component {
  constructor(props) {
    super(props);

    this.state = {
      elementValue: 1
    };
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    var synth = window.speechSynthesis;
    synth.cancel();
  }
  render() {
    return (
      <div className="learnAboutTIC">
        <div
          className={
            this.props.isDarkThemeEnabled ? "dark-theme" : "light-theme"
          }
        >
          <div className="topBgGradient"></div>
          <nav>
            <LeftSideDrawer />
            <ButtonToggle checkedValue={this.checkedValue} type="theme" />
          </nav>
          <Stars />
          <Clouds />
          <section className="section1 parallax">
            <div className="coolSpace">
              <div className="spaceTitle">
                <h2>
                  Just Hop in <br />
                  The Coolest <br />
                  <span>Tech Space Ever!</span>
                </h2>
              </div>
              <div className="spaceImg">
                <div className="spacegirl">&nbsp;</div>
              </div>
            </div>
            <div className="gradientRight"></div>
          </section>
          <section className="section2 parallax">
            <div className="labConatner">
              <div
                className="labTitle"
                data-aos="slide-left"
                data-aos-delay="100"
                data-aos-duration="2000"
                data-aos-easing="ease-in-out"
              >
                <h2>
                  <span>Technology Innovation & Collaboration Centre</span> at
                  BFL
                </h2>

                <p>Technology is as bright as a button!</p>
                <p>
                  TICC is created with a vision to be at the helm of the
                  fast-paced FinTech revolution. In our quest to be ‘The Master
                  Innovator’ in the industry, our constant pursuit to develop
                  new cutting-edge solutions is undying. We are ready to
                  collaborate with the best-in-class start-ups and Fintechs
                  around the ecosystem to explore their ideas and hit the
                  bull’s-eye.
                </p>
                <p>
                  Are we confined to any specific technology at TICC? Nah – AI,
                  ML, IoT, AR/VR, Cloud Tech, Mobility, and more. Bring it on!
                  Just leap at the opportunity.
                </p>
              </div>
              <div
                className="labImg"
                data-aos-delay="100"
                data-aos-duration="1000"
                data-aos-easing="ease-in-out"
              >
                <img
                  src={require("../../assets/images/TIC/lab.png")}
                  alt="lab"
                ></img>
              </div>
            </div>
            <div className="gradientLeft">
              <img
                src={require("../../assets/images/gradients/bubble.png")}
                alt="bubble"
              ></img>
            </div>
          </section>
          <section className="section3 parallax">
            <div
              className="titleInovation"
              data-aos="fade-up"
              data-aos-anchor-placement="center-bottom"
              data-aos-delay="100"
              data-aos-duration="2000"
            >
              <h2>
                A Unique Space for you at <span>TICC</span>
              </h2>
              <h3>
                Experience Digital Innovation in Action. You can see the New,
                Now!
                <br />
                We pleased to introduce you to the BFL cutting-edge “Technology
                Innovation & Collaboration Centre” where ideas turn into
                reality.
              </h3>
            </div>
            <div className="gridInovation">
              <div
                className="inovationItem"
                data-aos="zoom-in"
                data-aos-delay="100"
                data-aos-duration="3000"
              >
                Accelerate Innovation Journey - From
                <br /> Prototyping to
                <br /> Production <br />
                Deployment
                <img
                  src={require("../../assets/images/TIC/why1.png")}
                  alt="why1"
                ></img>
              </div>
              <div
                className="inovationItem"
                data-aos="zoom-in"
                data-aos-delay="100"
                data-aos-duration="3000"
              >
                Collaborative
                <br /> Infrastructure for
                <br /> Solution Showcase &<br /> Stakeholder
                <br />
                Engagement
                <img
                  src={require("../../assets/images/TIC/why2.png")}
                  alt="why2"
                ></img>
              </div>
              <div
                className="inovationItem"
                data-aos="zoom-in"
                data-aos-delay="100"
                data-aos-duration="3000"
              >
                Ecosystem of <br />
                Cross-Capability
                <br /> Innovation
                <img
                  src={require("../../assets/images/TIC/why3.png")}
                  alt="why3"
                ></img>
              </div>
              <div
                className="inovationItem"
                data-aos="zoom-in"
                data-aos-delay="100"
                data-aos-duration="3000"
              >
                Deep
                <br /> Engagement
                <br /> Platform with
                <br /> Innovative <br />
                Fintechs
                <img
                  src={require("../../assets/images/TIC/why4.png")}
                  alt="why4"
                ></img>
              </div>
              <div
                className="inovationItem"
                data-aos="zoom-in"
                data-aos-delay="100"
                data-aos-duration="3000"
              >
                Maximize the Efficacy
                <br /> of Opportunity &<br /> Sales Management
                <br />
                Lifecycle
                <img
                  src={require("../../assets/images/TIC/why5.png")}
                  alt="why5"
                ></img>
              </div>
              <div
                className="inovationItem"
                data-aos="zoom-in"
                data-aos-delay="100"
                data-aos-duration="3000"
              >
                {" "}
                Faster Pivots on
                <br /> Technology Bets
                <br /> - Discover Value &<br /> Create Growth
                <img
                  src={require("../../assets/images/TIC/why6.png")}
                  alt="why6"
                ></img>
              </div>
            </div>
            <InovationSlider />
          </section>
          <section className="section4 parallax">
            <div
              className="whyChooseText"
              data-aos="zoom-in-up"
              data-aos-delay="100"
              data-aos-duration="3000"
            >
              <h2>
                Why <span>Engage</span> with us at <span>TICC?</span>
              </h2>
              <p>
                We offer a promising head start that your Startup or Fintech
                might just be looking for.
                <br />
                Not just trifling words, but your solution stands a chance to be
                deployed at BFL – that can be huge.
                <br />
                Time to jump on the bandwagon.
                <br />
                The business synergy and technology deployment is
                once-in-a-lifetime opening that you can lay your hands on.
                <br />
                The kind of exposure you receive here, with a stellar company
                like Bajaj Finserv is unprecedented.
                <br />
                You also gain good ground in the Indian market and compete for
                attention in the BFSI industry branding arena.
                <br />
                The most intriguing part is that you will be stationed at our
                TICC lab (upon selection) – which means you get to experience
                the best infrastructure, and interact with the best of the
                startups and Fintechs <br />
                What more could you ask for?
              </p>
            </div>
            <div className="whyChooseImg">
              <img
                src={require("../../assets/images/TIC/boy.png")}
                alt="boy"
                data-aos="zoom-in-right"
                data-aos-delay="100"
                data-aos-duration="3000"
              ></img>
              <img
                src={require("../../assets/images/TIC/screen.png")}
                alt="screen"
                data-aos="zoom-in-up"
                data-aos-delay="100"
                data-aos-duration="3000"
              ></img>
              <div
                className="carouselContainer"
                data-aos="zoom-in-up"
                data-aos-delay="100"
                data-aos-duration="3000"
              >
                <Carousel />
              </div>
              {/* <img
                src={require("../../assets/images/TIC/screenMobile.png")}
                alt="screen"
                data-aos="zoom-in-up"
                data-aos-delay="100"
                data-aos-duration="3000"
              ></img> */}
              <img
                src={require("../../assets/images/TIC/girl.png")}
                alt="girl"
                data-aos="zoom-in-left"
                data-aos-delay="100"
                data-aos-duration="3000"
              ></img>
            </div>
            <div className="gradientRight"></div>
          </section>
          <footer className="footerTic parallax">
            <h6 className="needHelp">
              I'd be glad to lend a hand.
              <br />
              <a href="mailto:ticc@bajajfinserv.in">Let's touchbase!</a>
            </h6>
            <div className="socialIconTic">
              <a href="https://www.facebook.com/bajajfinserv/">
                <span className="icon-icons8-facebook-f"></span>
              </a>
              <a href="https://twitter.com/bajaj_finserv">
                <span className="icon-icons8-twitter"></span>
              </a>
            </div>
          </footer>
          <div className="applyNowBtn">
            <a href="/personal-detail-form">Apply Now</a>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isDarkThemeEnabled: state.commonReducer.isDarkThemeEnabled
  };
};

export default connect(mapStateToProps)(LearnAboutTIC);
