import React, { Component } from "react";
import "./WhySpaceBid.scss";
import LeftSideDrawer from "../common/leftSideDrawer/LeftSideDrawer";
import ButtonToggle from "../common/buttonToggle/ButtonToggle";
import Footer from "../common/footer/Footer";
import Stars from "../common/stars/Stars";
import "../common/stars/Stars.scss";
import Clouds from "../common/clouds/Clouds";
import "../common/clouds/Clouds.scss";
import AOS from "aos";
import "aos/dist/aos.css";
import { connect } from "react-redux";
import TakeawaySlider from "../common/carouselTakeaway/CarouselTakeaway";

AOS.init({
  delay: 100,
  mirror: true
});
class WhySpaceBid extends Component {
  constructor(props) {
    super(props);

    this.state = {
      next: 0
    };
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    var synth = window.speechSynthesis;
    synth.cancel();
  }
  nextScroll = () => {
    //document.getElementById("card2").id="card"
  };
  render() {
    return (
      <div className="whySpaceBid">
        <div
          className={
            this.props.isDarkThemeEnabled ? "dark-theme" : "light-theme"
          }
        >
          <nav>
            <LeftSideDrawer />
            <ButtonToggle checkedValue={this.checkedValue} type="theme" />
          </nav>
          <Stars />
          <Clouds />
          <section className="section1">
            <div className="titleText">
              <h2>Let’s Unravel SpaceBid</h2>
              <h4>
                BFL (Bajaj Finance Ltd.) is focused to get an eyeball on
                sterling solutions that are real-world problem-solvers.
                <br /> If you as a venture have out-of-the-box solutions, you
                are invited to engage with us. If you as a venture think like
                there’s no box – even better.
              </h4>
            </div>
            <div className="rocket" />
            <div className="topGrandient" />
            <div className="planet" />
            <div className="rotatedBackground" />
            <div className="nightCloud" />
            <div className="curtain2" />
          </section>
          <section className="section2">
            <div
              className="infoText"
              data-aos="slide-left"
              data-aos-delay="1000"
              data-aos-duration="800"
              data-aos-easing="ease-in-out"
            >
              <h2>
                If your startup can shoot well, we have got enough{" "}
                <span>firepower for you.</span>{" "}
              </h2>
              <p>
                Can you identify yourself with any of these words: Cutting-edge,
                Disruptive, Unconventional, Unorthodox, and Innovative?{" "}
                <p className="pcenter">If yes, what are you waiting for?</p>
                <p className="pcenter"> Let's Engage.</p>
              </p>
              <p>
                SpaceBid is the next-level platform to showcase your core
                competency to the world. Time for you to steal the limelight.{" "}
              </p>
              <p>
                A handful of best-in-class solutions stand a chance to get
                deployed at BFL, known for its remarkable strides in business,
                customer convenience and accolades.{" "}
              </p>
              <h4> Pumped up already? </h4>
            </div>
            <div className="SpotlightManpodiumGroup">
              <div
                className="manpodium"
                data-aos="slide-right"
                data-aos-delay="800"
                data-aos-duration="1000"
                data-aos-easing="ease-in-out"
              />
              <div
                className="spotlightbox"
                data-aos="fade-in"
                data-aos-delay="100"
                data-aos-duration="1000"
                data-aos-easing="ease-in-out"
              />
              <div
                className="spotlight"
                data-aos="fade-in"
                data-aos-delay="500"
                data-aos-duration="2000"
                data-aos-easing="ease-in-out"
              />
              <div className="curtain3" />
            </div>
          </section>
          <section
            className="section3"
            data-aos="fade-in"
            data-aos-offset="200"
            data-aos-delay="10"
            data-aos-duration="1000"
            data-aos-easing="ease-in-out"
          >
            <div className="instructTitle">
              <h2>
                The Easy Four-Step <span>Journey</span>{" "}
              </h2>
            </div>
            <div className="fourStepContainer">
              <div className="svgContainer">
                <svg
                  preserveAspectRatio="xMidYMid meet"
                  viewBox="0 0 1100 500"
                  width="1100"
                  height="500"
                >
                  <defs>
                    <path
                      d="M1065.45 31.36L960.91 137.42L947.27 228.33L674.55 378.33L662.42 237.42L453.33 151.06L291.21 467.73L98.79 373.79L39.7 438.94"
                      id="a2h8Y9urmQ"
                    ></path>
                  </defs>
                  <g>
                    <g>
                      <g>
                        <g>
                          <use
                            xlinkHref="#a2h8Y9urmQ"
                            opacity="1"
                            fillOpacity="0"
                            stroke="#000000"
                            strokeWidth="1"
                            strokeOpacity="1"
                          ></use>
                        </g>
                      </g>
                    </g>
                  </g>
                </svg>
              </div>
              <div className="star1">
                <img
                  src={require("../../assets/images/spacebid/smallstar.png")}
                  alt="smallstar"
                ></img>
              </div>
              <div className="star2">
                <img
                  src={require("../../assets/images/spacebid/mediumstar.png")}
                  alt="mediumstar"
                ></img>
              </div>
              <div className="star3">
                <img
                  src={require("../../assets/images/spacebid/mediumstar.png")}
                  alt="mediumstar1"
                ></img>
              </div>
              <div className="star4">
                <img
                  src={require("../../assets/images/spacebid/mediumstar.png")}
                  alt="mediumstar2"
                ></img>
              </div>
              <div className="star5">
                <img
                  src={require("../../assets/images/spacebid/smallstar.png")}
                  alt="smallstar"
                ></img>
              </div>
              <div className="star6">
                <img
                  src={require("../../assets/images/spacebid/smallstar.png")}
                  alt="smallstar1"
                ></img>
              </div>
              <div className="star7">
                <img
                  src={require("../../assets/images/spacebid/mediumstar.png")}
                  alt="smallstar2"
                ></img>
              </div>
              <div className="star8">
                <img
                  src={require("../../assets/images/spacebid/smallstar.png")}
                  alt="smallstar3"
                ></img>
              </div>
              <div className="instructs1">
                <img
                  src={require("../../assets/images/spacebid/step-1.png")}
                  alt="step-1"
                  alt="step 1"
                />
                <h6>PRE- EVALUATION</h6>
                <p>
                  A preliminary screening of the <br />
                  solution(s) proposed by startups.
                </p>
              </div>
              <div className="instructs2">
                <img
                  src={require("../../assets/images/spacebid/step-2.png")}
                  alt="step-2"
                  alt="step 2"
                />
                <h6>ENGAGEMENT & EVALUATION</h6>
                <p>
                  All the details around solution(s) deployment, <br />
                  delivery & support will be discussed.
                </p>
              </div>
              <div className="instructs3">
                <img
                  src={require("../../assets/images/spacebid/step-3.png")}
                  alt="step-3"
                  alt="step 3"
                />
                <h6>ASSESSMENT & TICC DEPLOYMENT</h6>
                <p>
                  Upon a thorough assessment of the
                  <br /> proposed solution, PoC & Prototyping <br />
                  of the solution will kick-in.
                </p>
              </div>
              <div className="instructs4">
                <img
                  src={require("../../assets/images/spacebid/step-4.svg")}
                  alt="step-4"
                  alt="step 3"
                />
                <h6>CONVERSIONS & ROLLOUTS</h6>
                <p>
                  If the solution(s) is selected, the internal <br />
                  stakeholders will engage with you on a <br />
                  detailed plan for a rollout.
                </p>
              </div>
            </div>
            <div className="curtain4" />
          </section>
          <section className="section4">
            <div className="optionTitle">
              <h2>
                Key <span>Takeaways</span>{" "}
              </h2>
            </div>
            <div className="cardContainer">
              <div className="cardItems1">
                <div className="card">
                  <div className="tooltip">
                    <img
                      src={require("../../assets/images/spacebid/takeaways1.png")}
                      alt="An Exciting Revenue - Sharing Opportunity"
                    />
                    <h4>Opportunities for Pilots & Experiments</h4>
                    <div className="text">
                      Through our well-established process, you can showcase
                      your offerings and capture opportunities for working on
                      pilots, POCs, and MVPs to create a loud buzz.{" "}
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="tooltip">
                    <img
                      src={require("../../assets/images/spacebid/takeaways2.png")}
                      alt="An Exciting Revenue - Sharing Opportunity"
                    />
                    <h4>Optimization of your Pre-Sales Efforts</h4>
                    <div className="text">
                      Optimize your time, effort and money spent for making
                      conversions using our platform as the one-stop showcase
                      for all stakeholders.{" "}
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="tooltip">
                    <img
                      src={require("../../assets/images/spacebid/takeaways3.png")}
                      alt="An Exciting Revenue - Sharing Opportunity"
                    />
                    <h4>Drive Business Growth & Development</h4>
                    <div className="text">
                      Sky is the limit. Showcase wide range of use cases that
                      are relevant for BFL and group companies. Leave it for us
                      to decide what is relevant.{" "}
                    </div>
                  </div>
                </div>
              </div>
              <div className="cardItems2">
                <div className="card">
                  <div className="tooltip">
                    <img
                      src={require("../../assets/images/spacebid/takeaways4.png")}
                      alt="An Exciting Revenue - Sharing Opportunity"
                    />
                    <h4>Showcase Latest Work & Innovations</h4>
                    <div className="text">
                      Through SpaceBid, get your dedicated window to show us
                      your offerings – disruptive enough? Let us see.{" "}
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="tooltip">
                    <img
                      src={require("../../assets/images/spacebid/takeaways5.png")}
                      alt="An Exciting Revenue - Sharing Opportunity"
                    />
                    <h4>Collaboration Space for Partners & Employees</h4>
                    <div className="text">
                      Use our phygital platforms to identify next level business
                      opportunity in collaboration with BFL stakeholders and
                      business leaders.{" "}
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="tooltip">
                    <img
                      src={require("../../assets/images/spacebid/takeaways6.png")}
                      alt="An Exciting Revenue - Sharing Opportunity"
                    />
                    <h4>Workshops & Sessions - Design Thinking and More</h4>
                    <div className="text">
                      Engage with decision makers and other stakeholders via
                      workshops, tech-events & drive the new business use cases
                      to make various market-ready innovative solutions.{" "}
                    </div>
                  </div>
                </div>
              </div>
              <TakeawaySlider />
              {/* <a className="prev" onClick={this.prevScroll}>
                <span className="icon-prev"></span>
              </a>
              <a className="next" onClick={this.nextScroll}>
                <span className="icon-next"></span>
              </a> */}
            </div>
          </section>
          <Footer />
          <div className="applyNowBtn">
            <a href="/personal-detail-form">Apply Now</a>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isDarkThemeEnabled: state.commonReducer.isDarkThemeEnabled
  };
};

export default connect(mapStateToProps)(WhySpaceBid);
