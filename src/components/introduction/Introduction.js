import React, { Component } from "react";
import "./Introduction.scss";
import LeftSideDrawer from "../common/leftSideDrawer/LeftSideDrawer";
import ButtonToggle from "../common/buttonToggle/ButtonToggle";
import Footer from "../common/footer/Footer";
import Stars from "../common/stars/Stars";
import "../common/stars/Stars.scss";
import { connect } from "react-redux";
import Clouds from "../common/clouds/Clouds";
import "../common/clouds/Clouds.scss";
import AOS from "aos";
import "aos/dist/aos.css";
import TypeTextToVoice from "../common/textToVoice/TypeTextToVoice";
import store from "../../store/store";
import {
  onTextToVoice,
  onVoiceEnabled
} from "../../store/actions/commonActions";
import SimpleSlider from "../common/carouselStrategic/CarouselStrategic";
import CarouselGroupStructure from "../common/carouselGroupStructure/CarouselGroupStructure";
import SimpleSliderMobile from "../common/carouselStrategicMobile/CarouselStrategicMobile";

AOS.init({
  delay: 100,
  mirror: true
});
let direction;
class Introduction extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scrollValue: 0
    };
  }
  componentDidMount() {
    window.scrollTo(0, 0);

    var synth = window.speechSynthesis;
    synth.pause();
    synth.cancel();
  }
  nextScrollTree = () => {
    const carousel = document.querySelector(".groupStructureContainer");
    const slider = document.querySelector(".groupStructure");
  };
  render() {
    return (
      <div className="introTOBajaj">
        <div
          className={
            this.props.isDarkThemeEnabled ? "dark-theme" : "light-theme"
          }
        >
          <div className="topBgGradient"></div>
          <nav>
            <LeftSideDrawer />
            <ButtonToggle checkedValue={this.checkedValue} type="theme" />
          </nav>
          <Stars />
          <Clouds />
          <TypeTextToVoice stop={true} />
          <section className="section1">
            <div className="introTop">
              <div className="titleText">
                <h1>
                  Hello there!
                  <br /> Bajaj Finserv <br />
                  <span> Welcomes you </span>
                </h1>
              </div>
              <div className="topImage">
                <img
                  src={require("../../assets/images/bajajpage/Top-Image.png")}
                ></img>
              </div>
            </div>
            <div className="ticBgBuilding" />
            <div className="curtain2" />
          </section>
          <section className="section2">
            <div
              className="infoText"
              data-aos="zoom-in-up"
              data-aos-delay="100"
              data-aos-duration="3000"
            >
              <h2>
                How about we blow our <span> horn for a while? </span>{" "}
              </h2>
              <p>
                In 2007, we embarked on a journey to create the most profitable
                and diversified deposit-taking Non-Banking Financial Company
                (NBFC-D).
              </p>
              <p>
                We saw the change and crafted the right business model to become
                one of the largest players in the burgeoning consumer finance
                segment in India.
              </p>
              <p>
                Our portfolio is concentrated around the entire spectrum of
                financial services to customer segments covering commercial &
                rural lending, consumer deposits & payments, insurance
                distribution, mortgages segments, securities & wealth
                management, and SME financing.
              </p>
              <p>
                We focused on becoming the pioneer in introducing interest-free
                EMI financing options to our clients.
              </p>
              <p>
                We sensed new paradigms "Digitalization" taking shape and spent
                time on continuous innovation in our product features and
                digital technologies to deliver the best financial performance
                backed by high standards of analytics, risk management,
                technology investments and customer service.
              </p>
            </div>
            <div className="curtain3" />
          </section>
          <section className="section3">
            <div
              className="title"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
              data-aos-delay="100"
              data-aos-duration="2000"
            >
              <h2>
                The Game of <span>Numbers</span>{" "}
              </h2>
            </div>
            <div className="cardContainer">
              <div className="cardItems1">
                <div
                  className="card"
                  data-aos="zoom-in"
                  data-aos-delay="100"
                  data-aos-duration="3000"
                >
                  <div className="number">
                    <h4>32</h4>
                  </div>
                  <div className="text">
                    <h4>Years Old NBFC</h4>
                  </div>
                </div>
                <div
                  className="card"
                  data-aos="zoom-in"
                  data-aos-delay="100"
                  data-aos-duration="3000"
                >
                  <div className="number">
                    <h4>986</h4>
                  </div>
                  <div className="text">
                    <h4>Urban Locations</h4>
                  </div>
                </div>
                <div
                  className="card"
                  data-aos="zoom-in"
                  data-aos-delay="100"
                  data-aos-duration="3000"
                >
                  <div className="number">
                    <h4>1193</h4>
                  </div>
                  <div className="text">
                    <h4>Rural Locations</h4>
                  </div>
                </div>
              </div>
              <div className="cardItems2 item2">
                <div
                  className="card1"
                  data-aos="zoom-in"
                  data-aos-delay="100"
                  data-aos-duration="3000"
                >
                  <div className="number">
                    <h4>41% </h4>
                  </div>
                  <div className="text">
                    <h4 style={{ margin: "unset" }}>
                      Consolidated (1,15,888 Cr)
                    </h4>
                    <h4 style={{ marginTop: "unset" }}>
                      Assets Under Management
                    </h4>
                  </div>
                </div>
                <div
                  className="card1"
                  data-aos="zoom-in"
                  data-aos-delay="100"
                  data-aos-duration="3000"
                >
                  <div className="number">
                    <h4>1,07,100+</h4>
                  </div>
                  <div className="text">
                    <h4>Active Distribution Network</h4>
                  </div>
                </div>
                <div
                  className="card1"
                  data-aos="zoom-in"
                  data-aos-delay="100"
                  data-aos-duration="3000"
                >
                  <div className="number">
                    <h4>22.5% (2019)</h4>
                  </div>
                  <div className="text returnequity">
                    <h4>Return on Equity</h4>
                  </div>
                </div>
              </div>
              <div className="cardItems2 item3">
                <div
                  className="card1"
                  data-aos="zoom-in"
                  data-aos-delay="100"
                  data-aos-duration="3000"
                >
                  <div className="number">
                    <h4>4.2% (2019)</h4>
                  </div>
                  <div className="text">
                    <h4>Return on Assets</h4>
                  </div>
                </div>
                <div
                  className="card2"
                  data-aos="zoom-in"
                  data-aos-delay="100"
                  data-aos-duration="3000"
                >
                  <div className="text">
                    <h4>Customer Franchise </h4>
                  </div>
                  <div className="number">
                    <h4>40.38 Million</h4>
                    <h6>people</h6>
                  </div>
                </div>
              </div>
              <div
                className="girl"
                data-aos="fade-right"
                data-aos-delay="100"
                data-aos-duration="3000"
              />
              <div
                className="man1"
                data-aos="fade-up-left"
                data-aos-delay="100"
                data-aos-duration="3000"
              />
              <div
                className="man2"
                data-aos="fade-up-left"
                data-aos-delay="100"
                data-aos-duration="3000"
              />
            </div>
          </section>
          <section className="section4">
            <div className="planet1"></div>
            <h2
              data-aos="zoom-in-up"
              data-aos-delay="100"
              data-aos-duration="3000"
            >
              The Bajaj <span> Family Group Structure </span>{" "}
            </h2>
            <h3
              data-aos="zoom-in-up"
              data-aos-delay="100"
              data-aos-duration="3000"
            >
              The <span>Oldest</span> & most <span>Respected</span> business
              houses
            </h3>

            <div className="groupStructureContainer">
              <CarouselGroupStructure />
            </div>
          </section>
          <section className="section5">
            <h2
              data-aos="zoom-in-up"
              data-aos-delay="100"
              data-aos-duration="3000"
            >
              You can explore our <span> Product Suite </span> here <br />
            </h2>
            <h3
              data-aos="zoom-in-up"
              data-aos-delay="100"
              data-aos-duration="3000"
            >
              All in one place. Think it. Done
            </h3>
            <div
              className="bflTableContainer"
              data-aos="zoom-in-up"
              data-aos-delay="100"
              data-aos-duration="3000"
            >
              <div className="title">BAJAJ FINANCE LIMITED</div>
              <div className="table">
                <table cellPadding="0" cellSpacing="0">
                  <thead>
                    <tr>
                      <th>Consumer</th>
                      <th>SME</th>
                      <th>Commercial</th>
                      <th>Rural</th>
                      <th>Deposits</th>
                      <th>Partnership & Services</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <ul>
                          <li>Consumer Durable Loans</li>
                          <li>Digital Product Loans (E)</li>
                          <li>Lifestyle Product Loans</li>
                          <li>Lifecare financing</li>
                          <li>EMI Cards</li>
                          <li>2-Wheeler & 3-Wheeler Loans</li>
                          <li>Personal Loan Cross-Sell</li>
                          <li> Salaried Personal Loans (E)</li>
                          <li>E-Commerce - Consumer Finance</li>
                          <li>Retailer Finance</li>
                        </ul>
                      </td>
                      <td>
                        <ul>
                          <li>Unsecured Working Capital Loans (E)</li>
                          <li>Loans to self employed and Professionals (E)</li>
                          <li>Secured Enterprise Loans</li>
                          <li>Used-car financing</li>
                        </ul>
                      </td>
                      <td>
                        <ul>
                          <li>Loan against securities</li>
                          <li>Large Value Lease Rental Discounting </li>
                          <li>
                            Vendor financing to auto component manufacturers
                          </li>
                          <li>Financial Institutions Lending</li>
                          <li>Light Engineering Lending</li>
                          <li>Specialty Chemicals Lending</li>
                          <li>Corporate Finance Loans</li>
                        </ul>
                      </td>
                      <td>
                        <ul>
                          <li>Consumer Durable Loans</li>
                          <li>Digital Product Loans</li>
                          <li>Personal Loans Cross Sell</li>
                          <li>Salaried Personal Loan</li>
                          <li>Gold Loans</li>
                          <li>Loans to Professionals</li>
                        </ul>
                      </td>
                      <td>
                        <ul>
                          <li>Retail Term Deposits</li>
                          <li>Corporate Term Deposits</li>
                        </ul>
                      </td>
                      <td>
                        <ul>
                          <li>Life insurance distribution</li>
                          <li>General Insurance Distribution</li>
                          <li>Health Insurance Distributio</li>
                          <li>Pocket Insurance</li>
                          <li>Co-Branded Credit Card</li>
                          <li>Co-Branded Wallet</li>
                          <li>Financial Fitness Report</li>
                        </ul>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="title">BAJAJ HOUSING FINANCE LIMITED</div>
              <div className="table">
                <table cellPadding="0" cellSpacing="0" className="table">
                  <thead>
                    <tr>
                      <th>Consumer</th>
                      <th>SME</th>
                      <th>Commercial</th>
                      <th>Rural</th>
                      <th>Deposits</th>
                      <th>Partnership & Services</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <ul>
                          <li>Salaried Home Loans (E)</li>
                          <li>Salaried Loan Against Property</li>
                        </ul>
                      </td>
                      <td>
                        <ul>
                          <li>Loan Against Property</li>
                          <li>Self Employed Home Loans</li>
                          <li>Lease Rental Discounting</li>
                        </ul>
                      </td>
                      <td>
                        <ul>
                          <li>Developer Finance</li>
                        </ul>
                      </td>
                      <td>
                        <ul>
                          <li>Loan Against Property</li>
                          <li>Home Loans</li>
                          <li>Secured Enterprise Loans</li>
                        </ul>
                      </td>
                      <td>
                        <ul></ul>
                      </td>
                      <td>
                        <ul>
                          <li>Property search services</li>
                          <li>Life insurance distribution</li>
                        </ul>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </section>
          <section className="section6">
            <div
              className="text"
              data-aos="zoom-in-up"
              data-aos-delay="100"
              data-aos-duration="3000"
            >
              <h2>
                Excited about <span>SpaceBid!</span>{" "}
              </h2>
              <p>We believe the world has changed.</p>
              <p>
                Thousands of Startups and Fintech companies are creating and
                innovating everyday to integrate technology with business
                product offerings, thus disrupting the traditional financial
                ecosystem. The untapped market has a huge potential to
                revolutionize the financial industry.
              </p>
              <p>
                We at BFL are excited to explore the future acceleration of
                digital transformation and disruption on Startups and Fintech
                companies to have better efficiency in terms of operational
                costs, faster Go-to-market, and personalized customer
                experience.
              </p>
              <p>We are always looking for solutions that make us better.</p>
              <p>
                If you think you have a prodigious solution that can help us
                spread our wings wider, we would love to invite you.
              </p>
            </div>
            <div
              className="rocket1"
              data-aos="fade-up"
              data-aos-delay="100"
              data-aos-duration="3000"
            >
              <img
                src={require("../../assets/images/bajajpage/rocket.png")}
              ></img>
            </div>
            <div className="building" />
            <div className="rocketsmoke" />
            <div className="rocketshadow" />
            <div className="dividerbelowrocket" />
          </section>
          <section className="section7">
            <div className="title">
              <h3>Now that we are here...</h3>
              <h2>
                Let's dive into the key technology areas that are of <br />
                <span>Strategic Importance </span>at <span>BFL</span>{" "}
              </h2>
            </div>
            <SimpleSlider className="desktop" />
            <SimpleSliderMobile className="mobile" />
          </section>
          <Footer />
          <div className="applyNowBtn">
            <a href="/personal-detail-form">Apply Now</a>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isDarkThemeEnabled: state.commonReducer.isDarkThemeEnabled
  };
};

export default connect(mapStateToProps)(Introduction);
