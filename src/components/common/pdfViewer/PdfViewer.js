import React, { Component } from "react";
// import { Page } from "react-pdf";
// import { Document } from 'react-pdf/dist/entry.webpack';
import { Document, Page, pdfjs } from "react-pdf";
import { Button } from "@material-ui/core";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
class PdfViewer extends Component {
  state = {
    numPages: null,
    pageNumber: 1
  };

  onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages });
  };

  hidePdf = () => {
    this.props.hidePdf();
  };

  printPDF = el => {
    var url = "/terms-and-conditions-template.pdf";
    var w = window.open(url);
    w.window.print();
  };
  render() {
    const { pageNumber, numPages } = this.state;
    return (
      <div className="PDFContainer">
        <div className="PDFContainer_action">
          <a onClick={this.hidePdf}>
            <span className="icon-close"></span> Close
          </a>
          <div className="icon">
            <Button
              className="printer"
              onClick={this.printPDF.bind(this, "pdf")}
            >
              <span className="icon-printer"></span>
            </Button>
            <a
              className="printer"
              href="/terms-and-conditions-template.pdf"
              download
            >
              <span className="icon-download"></span>
            </a>
          </div>
        </div>
        <div id="pdf">
          <Document
            file="/terms-and-conditions-template.pdf"
            onLoadSuccess={this.onDocumentLoadSuccess}
          >
            <Page pageNumber={pageNumber} />
          </Document>

          <p>
            Page {pageNumber} of {numPages}
          </p>
        </div>
      </div>
    );
  }
}

export default PdfViewer;
