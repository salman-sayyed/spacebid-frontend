import React from "react";
import Slider from "infinite-react-carousel";
import "./CarouselInovation.scss";

const InovationSlider = () => (
  <Slider
    className="innovationMobile"
    data-aos="zoom-in"
    data-aos-delay="100"
    data-aos-duration="3000"
  >
    <div
      className="inovationItem"
      // data-aos="zoom-in"
      // data-aos-delay="100"
      // data-aos-duration="3000"
    >
      <div className="cardtext">
        Accelerate Innovation Journey - From Prototyping to Production
        Deployment
      </div>
      <img
        className="why1"
        src={require("../../../assets/images/TIC/why1.png")}
        alt="why1"
      ></img>
    </div>
    <div
      className="inovationItem"
      // data-aos="zoom-in"
      // data-aos-delay="100"
      // data-aos-duration="3000"
    >
      <div className="cardtext">
        Collaborative Infrastructure for Solution Showcase & Stakeholder
        Engagement{" "}
      </div>
      <img
        className="why2"
        src={require("../../../assets/images/TIC/why2.png")}
        alt="why2"
      ></img>
    </div>
    <div
      className="inovationItem"
      // data-aos="zoom-in"
      // data-aos-delay="100"
      // data-aos-duration="3000"
    >
      <div className="cardtext">Ecosystem of Cross-Capability Innovation</div>
      <img
        className="why3"
        src={require("../../../assets/images/TIC/why3.png")}
        alt="why3"
      ></img>
    </div>
    <div
      className="inovationItem"
      // data-aos="zoom-in"
      // data-aos-delay="100"
      // data-aos-duration="3000"
    >
      <div className="cardtext">
        {" "}
        Deep Engagement Platform with Innovative Fintechs
      </div>
      <img
        className="why4"
        src={require("../../../assets/images/TIC/why4.png")}
        alt="why4"
      ></img>
    </div>
    <div
      className="inovationItem"
      // data-aos="zoom-in"
      // data-aos-delay="100"
      // data-aos-duration="3000"
    >
      <div className="cardtext">
        Maximize the Efficacy of Opportunity & Sales Management Lifecycle
      </div>
      <img
        className="why5"
        src={require("../../../assets/images/TIC/why5.png")}
        alt="why5"
      ></img>
    </div>
    <div
      className="inovationItem"
      // data-aos="zoom-in"
      // data-aos-delay="100"
      // data-aos-duration="3000"
    >
      {" "}
      <div className="cardtext">
        Faster Pivots on Technology Bets - Discover Value & Create Growth
      </div>
      <img
        className="why6"
        src={require("../../../assets/images/TIC/why6.png")}
        alt="why6"
      ></img>
    </div>
  </Slider>
);

export default InovationSlider;
