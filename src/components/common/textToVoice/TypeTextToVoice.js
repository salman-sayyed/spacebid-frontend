import React, { Component } from "react";
import { connect } from "react-redux";
import store from "../../../store/store";
import { onVoiceEnabled } from "../../../store/actions/commonActions";
class TypeTextToVoice extends Component {
  toRotate;
  el;
  loopNum;
  period;
  txt = "";
  isDeleting;
  synth = window.speechSynthesis;
  constructor(props) {
    super(props);
    this.state = {
      text: [],
      playVoiceFlag: false
    };
  }
  componentDidMount() {
    this.sayText();
    if (this.props.isVoiceEnabled) {
      this.setState({
        playVoiceFlag: true
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isVoiceEnabled) {
      this.setState({
        playVoiceFlag: true
      });
    }
  }

  sayText() {
    var elements = document.getElementsByClassName("typewrite");
    for (var i = 0; i < elements.length; i++) {
      var toRotate = this.props.text;
      var period = elements[i].getAttribute("data-period");
      if (toRotate) {
        this.TxtType(elements[i], JSON.parse(toRotate), period);
      }
    }
    // INJECT CSS
    var css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
    document.body.appendChild(css);
  }
  TxtType(el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = 2000;

    this.txt = "";
    this.tick();
    this.isDeleting = false;
  }
  tick() {
    var i = this.loopNum % this.toRotate.length;
    var fullTxt = this.toRotate[i];
    if (this.loopNum === this.toRotate.length) {
      this.terminateSpeech();
      return;
    }
    if (this.isDeleting) {
      // if condition is added to stop the deleting last line and providing delay
      if (this.loopNum === this.toRotate.length - 1) {
        this.txt = fullTxt;
        this.terminateSpeech();
        return;
      } else {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
      }
    } else {
      this.txt = fullTxt.substring(0, this.txt.length + 1);
    }
    if (3 === this.txt.length && !this.isDeleting) {
      if (window.location.pathname === "/") {
        this.say(fullTxt);
      }
    }
    this.el.innerHTML = '<span className="wrap">' + this.txt + "</span>";
    var that = this;
    var delta = 126 - Math.random() * 100;
    if (this.isDeleting) {
      delta /= 4;
    }
    if (!this.isDeleting && this.txt === fullTxt) {
      delta = this.period;
      this.isDeleting = true;
    } else if (this.isDeleting && this.txt === "") {
      this.isDeleting = false;
      this.loopNum++;
      delta = 500;
    }
    setTimeout(function() {
      that.tick();
    }, delta);
  }
  sleep = milliseconds => {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  };
  terminateSpeech = async () => {
    await this.sleep(1000);
    if (this.props.onSpeechEnd) {
      this.props.onSpeechEnd();
    }
    return;
  };
  say(text) {
    var sayswho = (function() {
      var ua = navigator.userAgent,
        tem,
        M =
          ua.match(
            /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i
          ) || [];
      if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return "IE " + (tem[1] || "");
      }
      if (M[1] === "Chrome") {
        tem = ua.match(/\b(OPR|Edge?)\/(\d+)/);
        if (tem != null)
          return tem
            .slice(1)
            .join(" ")
            .replace("OPR", "Opera")
            .replace("Edg ", "Edge ");
      }

      M = M[2] ? [M[1], M[2]] : [navigator.appName, "-?"];

      return M[0];
    })();
    if (speechSynthesis.speaking) {
      speechSynthesis.cancel();
    }
    if (this.props.isVoiceEnabled) {
      const ut = new SpeechSynthesisUtterance(text);
      var deviceDetect = navigator.platform;
      var desktopArr = ["Win32", "Win64"];
      var appleDevicesArr = [
        "MacIntel",
        "MacPPC",
        "Mac68K",
        "Macintosh",
        "iPhone",
        "iPod",
        "iPad",
        "iPhone Simulator",
        "iPod Simulator",
        "iPad Simulator",
        "Pike v7.6 release 92",
        "Pike v7.8 release 517"
      ];
      // If on Apple device
      if (appleDevicesArr.includes(deviceDetect)) {
        // Execute code
        ut.lang = "en-US";
        let selectedVoice = "Alex";
        ut.voice = speechSynthesis.getVoices().filter(function(voice) {
          return voice.name === selectedVoice;
        })[0];
      } else if (desktopArr.includes(deviceDetect)) {
        // Execute code
        ut.lang = "en-US";
        let selectedVoice = "Microsoft Zira Desktop - English (United States)";
        ut.voice = speechSynthesis.getVoices().filter(function(voice) {
          return voice.name === selectedVoice;
        })[0];
        //console.log("In MAC ::::::::::", ut);
      } else if (sayswho.includes("Edge")) {
        ut.lang = "en-US";
        let selectedVoice = "Microsoft Zira - English (United States)";
        ut.voice = speechSynthesis.getVoices().filter(function(voice) {
          return voice.name === selectedVoice;
        })[0];
      }
      // If NOT on Apple device
      else {
        // Execute code
        ut.lang = "hi-IN";
      }
      // if (sayswho.includes("Edge")) {
      //     ut.lang = "en-IN";
      // } else if (sayswho.includes("Chrome")) {
      //     ut.lang = "hi-IN";
      // } else if (sayswho.includes("Safari")) {
      //     ut.lang = "hi-IN";
      // } else {
      //     ut.lang = "en-IN";
      // }
      /*
         speechSynthesis.getVoices().forEach(voice => {
         })
         speechSynthesis.speak(ut); */
      speechSynthesis.speak(ut);
    }
  }

  render() {
    return <div className="header">{}</div>;
  }
}

const mapStateToProps = state => {
  return {
    isVoiceEnabled: state.commonReducer.isVoiceEnabled,
    toaster: state.commonReducer.toastr,
    stopVoice: state.commonReducer.stopText
  };
};

export default connect(mapStateToProps)(TypeTextToVoice);
