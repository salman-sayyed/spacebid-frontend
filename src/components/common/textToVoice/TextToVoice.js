import React, { Component } from "react";
import { connect } from "react-redux";
import store from "../../../store/store";
import { onSubmitError } from "../../../store/actions/commonActions";
class TextToVoice extends Component {
  toRotate;
  el;
  loopNum;
  period;
  txt = "";
  isDeleting;
  synth = window.speechSynthesis;
  constructor(props) {
    super(props);
    this.state = {
      text: [],
      playVoiceFlag: true
    };
  }
  componentDidMount() {}

  componentWillReceiveProps(nextProps) {
    if (nextProps.isVoiceEnabledd) {
      this.say(nextProps.voiceText); 
    }
  }

  say(text) {
    var sayswho = (function() {
      var ua = navigator.userAgent,
        tem,
        M =
          ua.match(
            /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i
          ) || [];
      if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return "IE " + (tem[1] || "");
      }
      if (M[1] === "Chrome") {
        tem = ua.match(/\b(OPR|Edge?)\/(\d+)/);
        if (tem != null)
          return tem
            .slice(1)
            .join(" ")
            .replace("OPR", "Opera")
            .replace("Edg ", "Edge ");
      }

      M = M[2] ? [M[1], M[2]] : [navigator.appName, "-?"];

      return M[0];
    })();
    if (speechSynthesis.speaking) {
      speechSynthesis.cancel();
    }
    // if (this.props.isVoiceEnabledd) {
    const ut = new SpeechSynthesisUtterance(text);
    var deviceDetect = navigator.platform;
    var desktopArr = ["Win32", "Win64"];
    var appleDevicesArr = [
      "MacIntel",
      "MacPPC",
      "Mac68K",
      "Macintosh",
      "iPhone",
      "iPod",
      "iPad",
      "iPhone Simulator",
      "iPod Simulator",
      "iPad Simulator",
      "Pike v7.6 release 92",
      "Pike v7.8 release 517"
    ];
    // If on Apple device
    if (appleDevicesArr.includes(deviceDetect)) {
      // Execute code
      ut.lang = "en-US";
      let selectedVoice = "Alex";
      ut.voice = speechSynthesis.getVoices().filter(function(voice) {
        return voice.name === selectedVoice;
      })[0];
    } else if (desktopArr.includes(deviceDetect)) {
      // Execute code
      ut.lang = "en-US";
      let selectedVoice = "Microsoft Zira Desktop - English (United States)";
      ut.voice = speechSynthesis.getVoices().filter(function(voice) {
        return voice.name === selectedVoice;
      })[0];
      //console.log("In MAC ::::::::::", ut);
    } else if (sayswho.includes("Edge")) {
      ut.lang = "en-US";
      let selectedVoice = "Microsoft Zira - English (United States)";
      ut.voice = speechSynthesis.getVoices().filter(function(voice) {
        return voice.name === selectedVoice;
      })[0];
    }
    // If NOT on Apple device
    else {
      // Execute code
      ut.lang = "hi-IN";
    }
    // if (sayswho.includes("Edge")) {
    //     ut.lang = "en-IN";
    // } else if (sayswho.includes("Chrome")) {
    //     ut.lang = "hi-IN";
    // } else if (sayswho.includes("Safari")) {
    //     ut.lang = "hi-IN";
    // } else {
    //     ut.lang = "en-IN";
    // }
    /*
         speechSynthesis.getVoices().forEach(voice => {
         })
         speechSynthesis.speak(ut); */
    speechSynthesis.speak(ut);
  }
  render() {
    return <div className="header">{}</div>;
  }
}

const mapStateToProps = state => {
  return {
    isVoiceEnabledd: state.commonReducer.isVoiceEnabled,
    voiceText: state.commonReducer.voiceText
  };
};

export default connect(mapStateToProps)(TextToVoice);
