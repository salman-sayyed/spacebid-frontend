import React, { Component } from "react";
import NavBar from "../navBar/NavBar";
import "./Header.scss";

class Header extends Component {
  render() {
    return (
      <div className="topContainer">
        <NavBar />
      </div>
    );
  }
}

export default Header;
