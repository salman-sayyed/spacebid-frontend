import React, { Component } from "react";
import "./Article.scss";
import ButtonToggle from "../buttonToggle/ButtonToggle";
import Welcome from "../../landing/Welcome";
import { connect } from "react-redux";

class Article extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVoiceEnabled: JSON.parse(localStorage.getItem("sound"))
    };
  }

  checkedSoundValue = checked => {
    this.setState({
      isVoiceEnabled: checked
    });
  };

  render() {
    return (
      <div className="articleContainer">
        <Welcome isVoiceEnabled={this.state.isVoiceEnabled} />
        <div
          className={this.props.isVoiceEnabledReducer ? "soundOn" : "soundOff"}
        >
          <ButtonToggle
            checkedSoundValue={this.checkedSoundValue.bind(this)}
            type="sound"
          />
        </div>
        {/* <div className={this.state.isVoiceEnabled ? "soundOn" : "soundOff"}>
          <ButtonToggle
            checkedSoundValue={this.checkedSoundValue.bind(this)}
            type="sound"
          />
        </div> */}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isVoiceEnabledReducer: state.commonReducer.isVoiceEnabled
  };
};

export default connect(mapStateToProps)(Article);
