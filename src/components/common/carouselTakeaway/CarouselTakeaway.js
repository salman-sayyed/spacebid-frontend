import React from "react";
import Slider from "infinite-react-carousel";
import "./CarouselTakeaway.scss";

const TakeawaySlider = () => (
  <Slider className="cardItems3">
    <div className="card" id="card1">
      <div className="tooltip">
        <img
          src={require("../../../assets/images/spacebid/takeaways1.png")}
          alt="An Exciting Revenue - Sharing Opportunity"
        />
        <h4>Opportunities for Pilots & Experiments</h4>
        <div className="text">
          Through our well-established process, you can showcase your offerings
          and capture opportunities for working on pilots, POCs, and MVPs to
          create a loud buzz.{" "}
        </div>
      </div>
    </div>
    <div className="card" id="card2">
      <div className="tooltip">
        <img
          src={require("../../../assets/images/spacebid/takeaways2.png")}
          alt="An Exciting Revenue - Sharing Opportunity"
        />
        <h4>Optimization of your Pre-Sales Efforts</h4>
        <div className="text">
          Optimize your time, effort and money spent for making conversions
          using our platform as the one-stop showcase for all stakeholders.{" "}
        </div>
      </div>
    </div>
    <div className="card" id="card3">
      <div className="tooltip">
        <img
          src={require("../../../assets/images/spacebid/takeaways3.png")}
          alt="An Exciting Revenue - Sharing Opportunity"
        />
        <h4>Drive Business Growth & Development</h4>
        <div className="text">
          Sky is the limit. Showcase wide range of use cases that are relevant
          for BFL and group companies. Leave it for us to decide what is
          relevant.{" "}
        </div>
      </div>
    </div>
    <div className="card" id="card4">
      <div className="tooltip">
        <img
          src={require("../../../assets/images/spacebid/takeaways4.png")}
          alt="An Exciting Revenue - Sharing Opportunity"
        />
        <h4>Showcase Latest Work & Innovations</h4>
        <div className="text">
          Through SpaceBid, get your dedicated window to show us your offerings
          – disruptive enough? Let us see.{" "}
        </div>
      </div>
    </div>
    <div className="card" id="card5">
      <div className="tooltip">
        <img
          src={require("../../../assets/images/spacebid/takeaways5.png")}
          alt="An Exciting Revenue - Sharing Opportunity"
        />
        <h4>Collaboration Space for Partners & Employees</h4>
        <div className="text">
          Use our phygital platforms to identify next level business opportunity
          in collaboration with BFL stakeholders and business leaders.{" "}
        </div>
      </div>
    </div>
    <div className="card" id="card6">
      <div className="tooltip">
        <img
          src={require("../../../assets/images/spacebid/takeaways6.png")}
          alt="An Exciting Revenue - Sharing Opportunity"
        />
        <h4>Workshops & Sessions - Design Thinking and More</h4>
        <div className="text">
          Engage with decision makers and other stakeholders via workshops,
          tech-events & drive the new business use cases to make various
          market-ready innovative solutions.{" "}
        </div>
      </div>
    </div>
  </Slider>
);

export default TakeawaySlider;
