import React, { Component } from "react";
import "./Navbar.scss";
import LeftSideDrawer from "../leftSideDrawer/LeftSideDrawer";

class NavBar extends Component {
  render() {
    return (
      <nav>
        <LeftSideDrawer />
      </nav>
    );
  }
}

export default NavBar;
