import React, { Component } from "react";
import NumberFormat from "react-number-format";
import { FieldControl } from "react-reactive-form";
import ReactPhoneInput from "react-phone-input-2";
import "../input/Input.scss";
import {
  onTextToVoice,
  onSubmitError
} from "../../../../store/actions/commonActions";
import store from "../../../../store/store";
import Validator from "../../leftSideDrawer/Validator";

var ReactCSSTransitionGroup = require("react-addons-css-transition-group");

class NumberInput extends Component {
  constructor(props) {
    super(props);
  }
  handleOnFocus = e => {
    store.dispatch(onTextToVoice(this.props.voiceText));
    if (this.props.clippyText) {
      store.dispatch(onSubmitError("Error!", this.props.clippyText));
    }
  };
  render() {
    const { values, name, ...rest } = this.props;

    return (
      // TODO replace input with material component
      <FieldControl
        name={name}
        handleChange={this.handleChange}
        render={({ handler, hasError }) => (
          <div>
            <div className="MuiInputBase-root">
              <NumberFormat
                className="MuiInputBase-input"
                {...handler()}
                decimalScale={this.props.decimalScale}
                format={this.props.format}
                placeholder={this.props.placeholder}
                disabled={this.props.disabled}
                onFocus={this.handleOnFocus}
              />
            </div>
          </div>
        )}
      />
    );
  }
}

export default NumberInput;
