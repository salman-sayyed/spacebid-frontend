import React, { Component } from "react";
import Label from "../label/Label";
import Input from "../input/Input";
import Number from "../number/Number";
import InputCurrency from "../input/InputCurrency";
import "./ComponentGenerator.scss";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { FieldControl, Validators } from "react-reactive-form";
import DropDown from "../dropDown/DropDown";
import { MultiSelect } from "../multiSelect/MultiSelect";
import Toggle from "../toggle/Toggle";
import TextArea from "../textArea/TextArea";
import PhoneInput from "../../formDetails/phone/PhoneInput";
import TextField from "@material-ui/core/TextField";
import PdfViewer from "../../pdfViewer/PdfViewer";
import FileUploadProgress from "react-fileupload-progress";
import store from "../../../../store/store";
import {
  onVoiceEnabled,
  onSubmitError
} from "../../../../store/actions/commonActions";
import Title from "../title/Title";
import VoiceToText from "../../voiceToText/VoiceToText";
import NumberInput from "../number/Number";
import Modal from "@material-ui/core/Modal";
import { connect } from "react-redux";

const styles = {
  progressWrapper: {
    height: "50px",
    marginTop: "10px",
    width: "400px",
    float: "left",
    overflow: "hidden",
    backgroundColor: "#f5f5f5",
    borderRadius: "4px",
    WebkitBoxShadow: "inset 0 1px 2px rgba(0,0,0,.1)",
    boxShadow: "inset 0 1px 2px rgba(0,0,0,.1)"
  },
  progressBar: {
    float: "left",
    width: "0",
    height: "100%",
    fontSize: "12px",
    lineHeight: "20px",
    color: "#fff",
    textAlign: "center",
    backgroundColor: "#5cb85c",
    WebkitBoxShadow: "inset 0 -1px 0 rgba(0,0,0,.15)",
    boxShadow: "inset 0 -1px 0 rgba(0,0,0,.15)",
    WebkitTransition: "width .6s ease",
    Otransition: "width .6s ease",
    transition: "width .6s ease"
  },
  cancelButton: {
    marginTop: "5px",
    WebkitAppearance: "none",
    padding: 0,
    cursor: "pointer",
    background: "0 0",
    border: 0,
    float: "left",
    fontSize: "21px",
    fontWeight: 700,
    lineHeight: 1,
    color: "#000",
    textShadow: "0 1px 0 #fff",
    filter: "alpha(opacity=20)",
    opacity: ".2"
  },

  bslabel: {
    display: "inline-block",
    maxWidth: "100%",
    marginBottom: "5px",
    fontWeight: 700
  },

  bsButton: {
    padding: "1px 5px",
    fontSize: "12px",
    lineHeight: "1.5",
    borderRadius: "3px",
    color: "#fff",
    backgroundColor: "#337ab7",
    borderColor: "#2e6da4",
    display: "inline-block",
    padding: "6px 12px",
    marginBottom: 0,
    fontWeight: 400,
    textAlign: "center",
    whiteSpace: "nowrap",
    verticalAlign: "middle",
    touchAction: "manipulation",
    cursor: "pointer",
    WebkitUserSelect: "none",
    MozUserSelect: "none",
    msUserSelect: "none",
    userSelect: "none",
    backgroundImage: "none",
    border: "1px solid transparent"
  }
};
let comp;
class ComponentGenerator extends Component {
  // openPdf = false;
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      isProduct: false,
      isSolutionServices: false,
      fileupload: false
    };
  }

  componentDidMount() {
    if (this.props.prefill) {
      this.setState({
        fileupload: true
      });
    }
    if (this.props.clippyText && this.props.type === "agree") {
      store.dispatch(onSubmitError("Error!", this.props.clippyText));
    } else if (this.props.clippyText && this.props.type === "checkbox") {
      store.dispatch(onSubmitError("Error!", this.props.clippyText));
    }
  }

  checkedBox = name => {
    if (name == "isProduct") {
      this.setState({
        isProduct: !this.state.isProduct
      });
    }
    if (name == "isSolutionServices") {
      this.setState({
        isSolutionServices: !this.state.isSolutionServices
      });
    }
    this.props.checkedBox(name);
  };

  showPdf = () => {
    this.props.showPdf(true);
  };

  hidePdf = () => {
    this.props.showPdf(false);
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  formGetter() {
    this.setState({
      fileupload: document.getElementById("customForm")
    });
    return new FormData(document.getElementById("customForm"));
  }

  onSubmit = (onSubmit, e) => {
    e.preventDefault();
    let file = e.target.files[0];
    let size = file.size ? file.size / 1000000 : 0;
    if (size <= 3 && file.name.match("^.*.(jpg|png|pdf)$")) {
      let fileupload = this.props.onFileChange(
        this.props.values,
        this.props.name,
        this.props.mandatory,
        e
      );
      fileupload.then(function(value) {
        if (value === "success") {
          onSubmit(e);
          if (document.getElementById("percentage")) {
            document.getElementById("percentage").style.visibility = "visible";
          }
        }
        // expected output: "foo"
      });
    } else {
      store.dispatch(
        onSubmitError("Error!", "File size should not be greater than 3mb")
      );
    }
  };
  customFormRenderer = onSubmit => {
    return (
      <form id="customForm" style={{ marginBottom: "15px" }}>
        <input
          style={{ display: "block" }}
          type="file"
          name="file"
          id="exampleInputFile"
          value={this.props.value}
          onChange={this.onSubmit.bind(this, onSubmit)}
          accept={
            this.props.accepted ? this.props.accepted : ".png, .jpg, .pdf"
          }
        />
      </form>
    );
  };
  cancelProgress = (cancelHandler, e) => {
    cancelHandler();
    this.setState({
      fileupload: false
    });
    if (this.props.mandatory === false) {
      document.getElementById(this.props.uploadLink).className =
        "uploadLink none";
      let daySelect = document.getElementById("dropDownId");
      if (this.props.uploadLink === "uploadLink1") {
        daySelect.options[daySelect.options.length] = new Option(
          "Company Logo",
          "1"
        );
      }
      if (this.props.uploadLink === "uploadLink2") {
        daySelect.options[daySelect.options.length] = new Option(
          "Company Financials",
          "2"
        );
      }
      if (this.props.uploadLink === "uploadLink3") {
        daySelect.options[daySelect.options.length] = new Option(
          "Company 2",
          "3"
        );
      }
      if (this.props.uploadLink === "uploadLink4") {
        daySelect.options[daySelect.options.length] = new Option(
          "Company 3",
          "4"
        );
      }
      if (this.props.uploadLink === "uploadLink5") {
        daySelect.options[daySelect.options.length] = new Option("other", "5");
      }
      if (this.props.uploadLink === "uploadLink6") {
        daySelect.options[daySelect.options.length] = new Option(
          "Product Sheet",
          "1"
        );
      }
      if (this.props.uploadLink === "uploadLink7") {
        daySelect.options[daySelect.options.length] = new Option("Other", "2");
      }
      if (this.props.uploadLink === "uploadLink8") {
        daySelect.options[daySelect.options.length] = new Option(
          "Demo Video 3",
          "3"
        );
      }
      if (this.props.uploadLink === "uploadLink9") {
        daySelect.options[daySelect.options.length] = new Option(
          "Demo Video 4",
          "4"
        );
      }
    }
  };
  customProgressRenderer(progress, hasError, cancelHandler) {
    if (hasError || progress > -1) {
      let barStyle = Object.assign({}, styles.progressBar);
      barStyle.width = progress + "%";

      let message = <span>{barStyle.width}</span>;

      if (progress === 100) {
        message = (
          <span className="progress">
            <div>100%</div>
            <span className="icon-checkedGreenSignprogressbar"></span>
          </span>
        );
      }

      return (
        <div className="progressContainer" id="progressContainer">
          <div id="progressContainer" style={styles.progressWrapper}>
            <div style={barStyle}></div>
          </div>
          <button
            style={styles.cancelButton}
            onClick={this.cancelProgress.bind(this, cancelHandler)}
          >
            <span>&times;</span>
          </button>
          <div id="percentage" className="percentage">
            {message}
          </div>
        </div>
      );
    } else {
      return;
    }
  }
  render() {
    const {
      values,
      placeholder,
      name,
      label,
      type,
      inputType,
      maxlength,
      disabled,
      onDropDownchange,
      dropdownData,
      voiceText,
      onFocus,
      animateRequire,
      identifier
    } = this.props;
    // console.log(this.props.prefill)
    return (
      <React.Fragment>
        {
          {
            input: (
              <div className="fieldClass">
                <Label label={label} />
                <Input
                  placeholder={placeholder}
                  name={name}
                  values={values}
                  type={type}
                  inputType={inputType}
                  maxlength={maxlength}
                  disabled={disabled}
                  voiceText={voiceText}
                  onFocus={onFocus}
                  resend={this.props.resend}
                  label={label}
                  clippyText={this.props.clippyText}
                  animateRequire={animateRequire}
                />
              </div>
            ),
            inputCurrency: (
              <div className="fieldClass">
                <Label label={label} />
                <InputCurrency
                  placeholder={placeholder}
                  name={name}
                  values={values}
                  type={type}
                  inputType={inputType}
                  maxlength={maxlength}
                  disabled={disabled}
                  voiceText={voiceText}
                  onFocus={onFocus}
                  onSelectHandleChange={this.props.onSelectHandleChange}
                  onRadioHandleChange={this.props.onRadioHandleChange}
                  label={label}
                  currency={this.props.currency}
                  currencyIn={this.props.currencyIn}
                  animateRequire={animateRequire}
                  clippyText={this.props.clippyText}
                />
              </div>
            ),
            number: (
              <div className="fieldClass">
                <Label label={label} />
                <NumberInput
                  type={this.props.type}
                  name={this.props.name}
                  label={this.props.label}
                  inputType={this.props.inputType}
                  decimalScale={this.props.decimalScale}
                  value={this.props.sofarImplementation}
                  disabled={this.props.disabled}
                  placeholder={this.props.placeholder}
                  format={this.props.format}
                  voiceText={this.props.voiceText}
                  clippyText={this.props.clippyText}
                />
              </div>
            ),
            phone: (
              <div className="fieldClass">
                <Label label={label} />
                <PhoneInput
                  placeholder={placeholder}
                  name={name}
                  values={values}
                  type={type}
                  inputType={inputType}
                  maxlength={maxlength}
                  disabled={disabled}
                  onKeyPress={this.props.onKeyPress}
                  voiceText={voiceText}
                  onFocus={onFocus}
                  clippy={this.props.clippy}
                />
              </div>
            ),
            textArea: (
              <div className="fieldClass">
                <Label label={label} />
                <div className="textareaContainer">
                  <TextArea
                    placeholder={placeholder}
                    name={name}
                    maxlength={maxlength}
                    disabled={disabled}
                    values={values}
                    voiceText={voiceText}
                    onFocus={onFocus}
                    clippyText={this.props.clippyText}
                  ></TextArea>
                  <VoiceToText name={name} />
                  {/* <span onClick={this.showPdf} class="icon-microphone-black-shape"></span> */}
                </div>
              </div>
            ),
            agree: (
              <div className="agreeComponent">
                <Checkbox
                  onChange={this.props.onAcceptTnC}
                  value={values}
                  checked={values}
                />

                <h1>
                  I agree to the{" "}
                  <a onClick={this.showPdf}> Terms and Conditions</a>
                </h1>
              </div>
            ),
            checkbox: (
              <div className="fieldClass">
                <div>
                  <Checkbox
                    onChange={this.checkedBox.bind(this, name)}
                    checked={values}
                  />
                  <span>{label}</span>
                </div>
              </div>
            ),
            pdf: (
              <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={true}
                onClose={this.hidePdf.bind(this)}
                closeAfterTransition
              >
                <PdfViewer hidePdf={this.hidePdf.bind(this)} />
              </Modal>
            ),
            dropdown: (
              <div className="fieldClass">
                <Label label={label} />
                <DropDown
                  placeholder={placeholder}
                  name={name}
                  values={values}
                  type={type}
                  disabled={disabled}
                  dropdownData={dropdownData}
                  voiceText={voiceText}
                  onFocus={onFocus}
                  clippyText={this.props.clippyText}
                />
              </div>
            ),
            multiSelect: (
              <div className="fieldClass">
                <Label label={label} />
                <MultiSelect
                  placeholder={placeholder}
                  name={name}
                  values={values}
                  type={type}
                  disabled={disabled}
                  dropdownData={dropdownData}
                  identifier={identifier}
                  voiceText={voiceText}
                  onFocus={onFocus}
                  clippyText={this.props.clippyText}
                  handleCreate={this.props.handleCreate}
                  createValue={this.props.createValue}
                />
              </div>
            ),
            toggle: (
              <div className="fieldClass">
                <Label label={label} />
                <Toggle
                  placeholder={placeholder}
                  name={name}
                  values={values}
                  type={type}
                  disabled={disabled}
                  switchChange={this.props.switchChange}
                  onFocus={onFocus}
                  voiceText={voiceText}
                />
              </div>
            ),
            file: (
              <div>
                {/* {this.state.fileupload ? (<div className="created ">
                <TextField
                  type={inputType}
                  inputProps={{ maxLength: maxlength }}
                  disabled={true}
                  value={values}
                />
                <div className="progressContainer">
                  
                  {/* <button
                    style={styles.cancelButton}
                    onClick={this.cancelProgress.bind(this, cancelHandler)}
                  >
                    <span>&times;</span>
                  </button> 
                  <div id="percentage" className="percentage">
                  <span className="progress">
                    <div>100%</div>
                    <span className="icon-checkedGreenSignprogressbar"></span>
                  </span>
                  </div>
                </div>
                </div>):
                <TextField
                  type={inputType}
                  inputProps={{ maxLength: maxlength }}
                  disabled={true}
                  value={values}
                />
                } */}
                <TextField
                  type={inputType}
                  inputProps={{ maxLength: maxlength }}
                  disabled={true}
                  value={values}
                />
                <div className="uploadFIle">
                  <FileUploadProgress
                    key="ex2"
                    url=""
                    method="post"
                    onProgress={(e, request, progress) => {}}
                    onLoad={(e, request) => {}}
                    onError={(e, request) => {}}
                    onAbort={(e, request) => {}}
                    formGetter={this.formGetter.bind(this)}
                    formRenderer={this.customFormRenderer.bind(this)}
                    progressRenderer={this.customProgressRenderer.bind(this)}
                  />
                </div>
              </div>
            ),
            title: (
              <div className="fieldClass">
                <Title
                  title={label}
                  redirect={this.props.redirect}
                  state={this.state}
                  clippyText={this.props.clippyText}
                ></Title>
              </div>
            )
          }[type]
        }
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    commonReducer: state.commonReducer
  };
};

export default connect(mapStateToProps)(ComponentGenerator);
