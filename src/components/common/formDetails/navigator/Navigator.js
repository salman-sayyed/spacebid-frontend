import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import "./Navigator.scss";

class Navigator extends Component {
  onClickhandel = () => {
    this.props.nextStep();
  };

  onClickSendOTP = () => {
    this.props.sendOTP();
  };

  onClickConfirmOTP = () => {
    this.props.confirmOTP();
  };

  render() {
    const { invalid } = this.props;
    return (
      // TODO replace input with material component
      <div className="stepButtons">
        <Button
          type="button"
          onClick={this.props.prevStep}
          disabled={this.props.step === 1 ? true : false}
        >
          <span className="icon-prev"></span>
        </Button>
        <Button
          type="button"
          disabled={invalid}
          onClick={
            this.props.sendOTP
              ? this.onClickSendOTP.bind(this)
              : this.props.confirmOTP
              ? this.onClickConfirmOTP.bind(this)
              : this.onClickhandel.bind(this)
          }
          onKeyPress={
            this.props.onKeyPressEvent
              ? this.props.onKeyPressEvent.bind(this)
              : this.onClickhandel.bind(this)
          }
        >
          {this.props.sendOTP ? (
            <div className="buttonLabel">Send OTP</div>
          ) : this.props.confirmOTP ? (
            <div className="buttonLabel">Confirm</div>
          ) : (
            ""
          )}{" "}
          <span className="icon-next"></span>
        </Button>
      </div>
    );
  }
}

export default Navigator;
