import React, { Component } from "react";

class Label extends Component {
  render() {
    const { label } = this.props;
    return (
      <React.Fragment>
        <label className="ui centered">{label}</label>
      </React.Fragment>
    );
  }
}

export default Label;
