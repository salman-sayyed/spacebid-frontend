import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import "../input/Input.scss";
import "./DisplaySuccessMessage.scss";
import store from "../../../../store/store";
import { onTextToVoice } from "../../../../store/actions/commonActions";
import { updateSpocDetails } from "../../../../store/actions/updateSpocAction";

class DisplaySuccessMessage extends Component {
  saveAndContinue = e => {
    e.preventDefault();
    const {
      fullName,
      designation,
      mobile,
      alternativeMobileNumber,
      alternativeEmail,
      name,
      officialEmail,
      consent
    } = this.props.data;

    const data = {
      fullName,
      designation,
      mobile,
      alternativeMobileNumber,
      alternativeEmail,
      name,
      officialEmail,
      consent
    };
    const companyId = localStorage.getItem("companyId");
    const userId = localStorage.getItem("userId");
    store.dispatch(updateSpocDetails(data, userId, companyId));
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  componentDidMount() {
    store.dispatch(
      onTextToVoice("Wooho! You're now registered. Login anytime you wish.")
    );
  }

  render() {
    return (
      <div className="successMsgContainer">
        <React.Fragment>
          <div className="successContainer">
            <form color="blue">
              <h1 className="ui centered">
                Wooho! You're now registered. Login anytime you wish.
              </h1>
              <Button onClick={this.saveAndContinue}>Continue </Button>
            </form>
          </div>
        </React.Fragment>
      </div>
    );
  }
}

export default DisplaySuccessMessage;
