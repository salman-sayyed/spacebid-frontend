import React, { Component } from "react";
import { FieldControl } from "react-reactive-form";
import { TextareaAutosize } from "@material-ui/core";
import store from "../../../../store/store";
import {
  onTextToVoice,
  onSubmitError,
  onVoiceEnabled
} from "../../../../store/actions/commonActions";

class TextArea extends Component {
  handleOnFocus = e => {
    store.dispatch(onTextToVoice(this.props.voiceText));
    if (this.props.clippyText) {
      store.dispatch(onSubmitError("Erorr!", this.props.clippyText));
    }
  };
  componentDidMount() {
    store.dispatch(
      onTextToVoice("you can also give voice command to fill this field")
    );
    store.dispatch(onVoiceEnabled(true));
  }

  render() {
    const {
      placeholder,
      name,
      inputType,
      maxlength,
      disabled,
      onFocus,
      values
    } = this.props;

    return (
      // TODO replace input with material component
      <FieldControl
        name={name}
        handleChange={this.handleChange}
        render={({ handler, pending, touched, hasError }) => (
          <div>
            <TextareaAutosize
              {...handler()}
              placeholder={placeholder}
              type={inputType}
              inputprops={{ maxLength: maxlength }}
              disabled={disabled}
              onFocus={this.handleOnFocus}
              //defaultValue={values}
              autoFocus={onFocus}
            />
          </div>
        )}
      />
    );
  }
}

export default TextArea;
