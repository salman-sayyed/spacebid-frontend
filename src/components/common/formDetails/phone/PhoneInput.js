import React, { Component } from "react";
import { FieldControl } from "react-reactive-form";
import Clippy from "../clippy/Clippy";
import ReactPhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import "./PhoneInput.scss";
import {
  onTextToVoice,
  onSubmitError
} from "../../../../store/actions/commonActions";
import store from "../../../../store/store";
import Validator from "../../leftSideDrawer/Validator";

var ReactCSSTransitionGroup = require("react-addons-css-transition-group");

class Input extends Component {
  constructor(props) {
    super(props);

    this.state = {
      phone: ""
    };
  }
  handleOnChange = value => {
    this.setState({ phone: value }, () => {});
  };

  handleOnFocus = () => {
    if (this.props.clippy) {
      store.dispatch(
        onSubmitError("Error!", "This is optional, you can skip this")
      );
    } else if (this.props.type === "phone") {
      store.dispatch(
        onSubmitError(
          "Error!",
          "The fields marked with asterisk (*) are mandatory"
        )
      );
    }
    store.dispatch(onTextToVoice(this.props.voiceText));
  };

  render() {
    const { values, name } = this.props;

    return (
      // TODO replace input with material component
      <FieldControl
        name={name}
        handleChange={this.handleChange}
        render={({ handler, hasError }) => (
          <div>
            <div className="phoneContainer">
              <ReactCSSTransitionGroup
                transitionName="example"
                transitionAppear={true}
                transitionAppearTimeout={500}
                transitionEnter={false}
                transitionLeave={false}
              >
                <ReactPhoneInput
                  {...handler()}
                  inputProps={{
                    name: "phone",
                    required: true,
                    autoFocus: true,
                    mask: "0000000000"
                  }}
                  defaultCountry={"in"}
                  country={"in"}
                  value={values}
                  countryCodeEditable={false}
                  onKeyDown={this.props.onKeyPress}
                  autoFocus={true}
                  onFocus={this.handleOnFocus}
                />
              </ReactCSSTransitionGroup>
            </div>
          </div>
        )}
      />
    );
  }
}

export default Input;
