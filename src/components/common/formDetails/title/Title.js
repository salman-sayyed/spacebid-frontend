import React, { Component } from "react";
import "./Input.scss";
import TextField from "@material-ui/core/TextField";
import { FieldControl } from "react-reactive-form";
import Clippy from "../clippy/Clippy";
import store from "../../../../store/store";
import {
  onTextToVoice,
  onSetStep,
  onSubmitError
} from "../../../../store/actions/commonActions";
import stepReducer from "../../../../store/reducers/stepReducer";
import { solutionDetails } from "../../../../store/actions/solutionAction";

class Title extends Component {
  componentDidMount() {
    if (this.props.clippyText) {
      store.dispatch(onSubmitError("Error", this.props.clippyText));
    }
    store.dispatch(onTextToVoice(this.props.title));
  }

  render() {
    if (this.props.redirect) {
      setTimeout(() => {
        store.dispatch(onSetStep(15));
      }, 5000);
      store.dispatch(solutionDetails(this.props.state, 14));
    }

    return <h2>{this.props.title}</h2>;
  }
}

export default Title;
