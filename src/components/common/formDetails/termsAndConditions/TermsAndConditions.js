import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Background from "../../background/background";
import Header from "../../header/Header";
import Checkbox from "@material-ui/core/Checkbox";
import "../input/Input.scss";

class FullName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false
    };
  }

  saveAndContinue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  handleChange = name => event => {
    this.setState({ ...this.state, [name]: event.target.checked });
  };

  render() {
    const { values } = this.props;
    return (
      <React.Fragment>
        <Background />
        <div className="landingContainer">
          <Header />
          <div className="formContainer">
            <form color="blue">
              <h1 className="ui centered">
                I agree to the Terms and Conditions
              </h1>
              <Checkbox
                checked={this.state.checked}
                onChange={this.handleChange}
                value="checkedA"
                inputProps={{
                  "aria-label": "primary checkbox"
                }}
                label="I agree to the Terms and Conditions"
              />

              <Button onClick={this.back}>Back</Button>
              <Button onClick={this.saveAndContinue}>Save And Continue </Button>
            </form>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default FullName;
