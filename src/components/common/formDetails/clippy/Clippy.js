import React, { Component } from "react";

export class Clippy extends Component {
  render() {
    const { message } = this.props;
    return (
      <div>
        <div className="validationmsgContainer">
          <div className="validationmsg">{message}</div>
          <img
            src={require("../../../../assets/images/clippy.png")}
            alt="Validation"
          />
        </div>
      </div>
    );
  }
}

export default Clippy;
