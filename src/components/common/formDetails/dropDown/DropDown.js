import React, { Component } from "react";
import { FieldControl } from "react-reactive-form";
import Select from "@material-ui/core/Select";
import { MenuItem, MuiThemeProvider } from "@material-ui/core";
import store from "../../../../store/store";
import {
  onTextToVoice,
  onSubmitError
} from "../../../../store/actions/commonActions";

export class DropDown extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: ""
    };
  }
  change = name => ({ target: { value } }) => {
    this.props.onDropDownchange(name, value);
  };

  handleOnFocus = e => {
    store.dispatch(onTextToVoice(this.props.voiceText));
    if (this.props.clippyText) {
      store.dispatch(onSubmitError("Error!", this.props.clippyText));
    }
  };

  render() {
    const { placeholder, name, disabled, onFocus } = this.props;
    return (
      <MuiThemeProvider>
        <FieldControl
          name={name}
          render={({ handler }) => (
            <div>
              {/* <InputLabel htmlFor="name-multiple">{placeholder}</InputLabel> */}
              <Select
                {...handler()}
                style={{ width: "50%" }}
                key={name}
                inputProps={{
                  disabled: disabled
                }}
                autoFocus={onFocus}
                onFocus={this.handleOnFocus}
              >
                <MenuItem
                  htmlFor="name-multiple"
                  value={placeholder}
                  selected
                  disabled
                >
                  {placeholder}
                </MenuItem>
                {this.props.dropdownData
                  ? this.props.dropdownData.map(category => {
                      return (
                        <MenuItem key={category} value={category}>
                          {category}
                        </MenuItem>
                      );
                    })
                  : ""}
              </Select>
            </div>
          )}
        />
      </MuiThemeProvider>
    );
  }
}

export default DropDown;
