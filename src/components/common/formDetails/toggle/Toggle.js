import React, { Component } from "react";
import Switch from "react-switch";
import store from "../../../../store/store";
import { onTextToVoice } from "../../../../store/actions/commonActions";
export class Toggle extends Component {
  componentDidMount() {
    store.dispatch(onTextToVoice(this.props.voiceText));
  }
  render() {
    const { values, disabled } = this.props;
    return (
      <div>
        <Switch
          onChange={this.props.switchChange}
          checked={values}
          disabled={disabled}
        />
      </div>
    );
  }
}

export default Toggle;
