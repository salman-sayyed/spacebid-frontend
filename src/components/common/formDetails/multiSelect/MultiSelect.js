import React, { Component } from "react";
import { FieldControl } from "react-reactive-form";
import { MuiThemeProvider } from "@material-ui/core";
import CreatableSelect from "react-select/creatable";
import _ from "lodash";
import {
  onTextToVoice,
  onSubmitError,
  onVoiceEnabled
} from "../../../../store/actions/commonActions";
import store from "../../../../store/store";
import { getMasterData } from "../../../../store/actions/masterDataAction";
export class MultiSelect extends Component {
  dropdownOptions;
  constructor(props) {
    super(props);

    this.state = {
      inputValue: ""
    };
  }
  change = name => ({ target: { value } }) => {
    this.props.onDropDownchange(name, value);
  };

  componentDidMount() {
    if (
      this.props.dropdownData &&
      this.props.dropdownData.length &&
      this.props.dropdownData.length > 0
    ) {
      this.dropdownOptions = this.getOptions();
    }
  }

  // createValue = value => {
  //   store.dispatch(postMasterData(this.props.identifier, value));
  // };
  getOptions() {
    let obj;
    let options = [];
    this.props.dropdownData.map(data => {
      obj = {};
      obj.label = data;
      obj.value = data;
      options.push(obj);
    });
    return options;
  }

  handleOnFocus = e => {
    if (this.props.clippyText) {
      store.dispatch(onSubmitError("Error!", this.props.clippyText));
    }
    store.dispatch(onTextToVoice(this.props.voiceText));

    store.dispatch(onVoiceEnabled(true));
  };
  render() {
    const { values, placeholder, name, disabled } = this.props;
    if (this.props.dropdownData && this.props.dropdownData.length > 0) {
      this.dropdownOptions = this.getOptions();
    }
    return (
      <MuiThemeProvider>
        <FieldControl
          name={name}
          render={({ handler }) => (
            <div>
              <CreatableSelect
                isMulti
                {...handler()}
                key={name}
                onFocus={this.handleOnFocus}
                options={this.dropdownOptions}
                isDisabled={disabled}
                onCreateOption={this.props.createValue.bind(
                  this,
                  this.props.identifier,
                  name
                )}
                defaultValue={this.props.values}
                isClearable={false}
              />
            </div>
          )}
        />
      </MuiThemeProvider>
    );
  }
}

export default MultiSelect;
