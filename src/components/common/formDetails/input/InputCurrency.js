import React, { Component } from "react";
import "./Input.scss";
import TextField from "@material-ui/core/TextField";
import { FieldControl } from "react-reactive-form";
import Clippy from "../clippy/Clippy";
import store from "../../../../store/store";
import {
  onTextToVoice,
  onSubmitError
} from "../../../../store/actions/commonActions";
import Validator from "../../leftSideDrawer/Validator";
import ReactFlagsSelect from "react-flags-select";
import "react-flags-select/scss/react-flags-select.scss";
import NumberFormat from "react-number-format";

var ReactCSSTransitionGroup = require("react-addons-css-transition-group");

class Input extends Component {
  constructor(props) {
    super(props);

    this.state = {
      phone: "",
      currency: ""
    };
  }

  componentDidMount() {
    if (this.props.currency && this.props.currency === "Dollar") {
      this.setState({
        currency: this.props.currency
      });
    } else if (this.props.currency && this.props.currency === "INR") {
      this.setState({
        currency: this.props.currency
      });
    }
  }

  handleOnFocus = e => {
    if (this.props.clippyText) {
      store.dispatch(onSubmitError("Error!", this.props.clippyText));
    }
    store.dispatch(onTextToVoice(this.props.voiceText));
  };

  onhandelChange(e) {
    this.props.onSelectHandleChange(e);
    const currency = e === "IN" ? "INR" : "Dollar";
    this.setState({
      currency: currency
    });
  }

  render() {
    const { placeholder, name, disabled, onFocus, inputType } = this.props;
    return (
      // TODO replace input with material component
      <div className="inputCurrency">
        <div className="capitalCurrency">
          <ReactFlagsSelect
            name="capitalCurrency"
            onSelect={this.onhandelChange.bind(this)}
            defaultCountry={this.props.currency === "Dollar" ? "US" : "IN"}
            countries={["US", "IN"]}
            disabled={disabled}
            customLabels={{ US: "Dollar", IN: "INR" }}
          />

          <FieldControl
            name={name}
            handleChange={this.handleChange}
            render={({ handler, pending, touched, hasError }) => (
              <div className="MuiInputBase-root">
                {
                  <>
                    <NumberFormat
                      {...handler()}
                      className="MuiInputBase-input"
                      placeholder={placeholder}
                      decimalScale={0}
                      disabled={disabled}
                      autoFocus={onFocus}
                      onFocus={this.handleOnFocus}
                      thousandsGroupStyle="lakh"
                    />
                  </>
                }
                <div>
                  {(hasError("required") && (
                    <Validator message="The fields marked with asterisk (*) are mandatory" />
                  )) ||
                    (hasError("email") && (
                      <Validator message="Please enter a valid email" />
                    )) ||
                    (hasError("notExist") && (
                      <Validator message="The fields marked with asterisk (*) are mandatory" />
                    ))}
                </div>
              </div>
            )}
          />
        </div>

        <div className="currencyIn">
          {this.state.currency === "INR" ? (
            <div className="inputField">
              <input
                type="radio"
                name="currencyIn"
                value="Thousand"
                onChange={this.props.onRadioHandleChange}
                checked={this.props.currencyIn === "Thousand" ? true : false}
              />{" "}
              <span>Thousand</span>
              <input
                type="radio"
                name="currencyIn"
                value="Lakh"
                onChange={this.props.onRadioHandleChange}
                checked={this.props.currencyIn === "Lakh" ? true : false}
              />{" "}
              <span>Lakh</span>
              <input
                type="radio"
                name="currencyIn"
                value="Crore"
                onChange={this.props.onRadioHandleChange}
                checked={this.props.currencyIn === "Crore" ? true : false}
              />
              <span>Crore</span>
            </div>
          ) : (
            <div className="inputField">
              <input
                type="radio"
                name="currencyIn"
                value="Thousand"
                onChange={this.props.onRadioHandleChange}
                checked={this.props.currencyIn === "Thousand" ? true : false}
              />{" "}
              <span>Thousand</span>
              <input
                type="radio"
                name="currencyIn"
                value="Millions"
                onChange={this.props.onRadioHandleChange}
                checked={this.props.currencyIn === "Millions" ? true : false}
              />{" "}
              <span>Million</span>
              <input
                type="radio"
                name="currencyIn"
                value="Billion"
                onChange={this.props.onRadioHandleChange}
                checked={this.props.currencyIn === "Billion" ? true : false}
              />{" "}
              <span>Billion</span>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default Input;
