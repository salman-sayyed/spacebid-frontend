import React, { Component } from "react";
import "./Input.scss";
import TextField from "@material-ui/core/TextField";
import { FieldControl } from "react-reactive-form";
import Clippy from "../clippy/Clippy";
import store from "../../../../store/store";
import {
  onTextToVoice,
  onSubmitError,
  onVoiceEnabled
} from "../../../../store/actions/commonActions";
import Validator from "../../leftSideDrawer/Validator";
import { connect } from "react-redux";

var ReactCSSTransitionGroup = require("react-addons-css-transition-group");

class Input extends Component {
  constructor(props) {
    super(props);

    this.state = {
      phone: ""
    };
  }

  handleOnFocus = e => {
    store.dispatch(onTextToVoice(this.props.voiceText));
    if (this.props.clippyText) {
      store.dispatch(onSubmitError("Error!", this.props.clippyText));
    }
  };

  componentDidMount() {}
  render() {
    const {
      placeholder,
      name,
      inputType,
      maxlength,
      disabled,
      onFocus,
      animateRequire
    } = this.props;
    return (
      // TODO replace input with material component
      <>
        <FieldControl
          name={name}
          handleChange={this.handleChange}
          render={({ handler, pending, touched, hasError }) => (
            <div>
              {animateRequire ? (
                <ReactCSSTransitionGroup
                  transitionName="example"
                  transitionAppear={true}
                  transitionAppearTimeout={500}
                  transitionEnter={false}
                  transitionLeave={false}
                >
                  <TextField
                    {...handler()}
                    placeholder={placeholder}
                    type={inputType}
                    inputProps={{ maxLength: maxlength }}
                    disabled={disabled}
                    autoFocus={onFocus}
                    onFocus={this.handleOnFocus}
                  />
                </ReactCSSTransitionGroup>
              ) : (
                <TextField
                  {...handler()}
                  placeholder={placeholder}
                  type={inputType}
                  inputProps={{ maxLength: maxlength }}
                  disabled={disabled}
                  autoFocus={onFocus}
                  onFocus={this.handleOnFocus}
                />
              )}
            </div>
          )}
        />

        {name === "verificationCode" ||
        (name === "password" &&
          this.props.toastr &&
          this.props.toastr.type === "") ? (
          <div className="Validation resend">
            <div className="resendOTP">
              {this.props.toastr && this.props.toastr.type === "" ? (
                <div className="validationmsgContainer">
                  <div className="validationmsg">
                    Didn't receive verification Code?
                    <div
                      className="resendLink"
                      onClick={this.props.resend}
                      style={{ cursor: "pointer" }}
                    >
                      {" "}
                      Resend Now
                    </div>
                  </div>

                  <img
                    src={require("../../../../assets/images/clippy.png")}
                    alt="Validation"
                  />
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
        ) : (
          <div></div>
        )}
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
    isVoiceEnabledReducer: state.commonReducer.isVoiceEnabled,
    toastr: state.commonReducer.toastr
  };
};
export default connect(mapStateToProps)(Input);
