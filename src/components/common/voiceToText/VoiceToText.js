"use strict";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  SpeechRecognition,
  webkitSpeechRecognition
} from "react-speech-recognition";

import store from "../../../store/store";
import {
  onVoiceToText,
  onMiceEnabled
} from "../../../store/actions/commonActions";

//------------------------COMPONENT-----------------------------

class VoiceToText extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listening: false
    };
  }
  componentDidMount() {}

  toggleListen = name => {
    this.handleListen(name);
  };

  handleListen(name) {
    if (!window.webkitSpeechRecognition) {
    } else {
      var SpeechRecognition =
        window.webkitSpeechRecognition || window.SpeechRecognition;
      var recognition = new SpeechRecognition();
      recognition.continuous = true;
      recognition.interimResults = true;
      recognition.lang = "en-US";
      recognition.maxAlternatives = 1;
      this.state.listening = true;

      if (this.state.listening) {
        recognition.start();
        recognition.onend = () => recognition.start();
      } else {
        recognition.onend();
      }

      recognition.onstart = function() {
        // console.log("Recognition has started!");
      };

      var final_transcript = "";
      recognition.onresult = function(event) {
        var interim_transcript = "";
        if (typeof event.results === "undefined") {
          recognition.stop();
          return;
        }
        for (var i = event.resultIndex; i < event.results.length; ++i) {
          if (event.results[i].isFinal) {
            final_transcript += event.results[i][0].transcript;
          } else {
            interim_transcript += event.results[i][0].transcript;
          }
        }

        final_transcript = capitalize(final_transcript);
        // console.log("Your words:::::::::", capitalize(interim_transcript));
        // console.log("Your words:::::::::", capitalize(final_transcript));
        store.dispatch(onVoiceToText(linebreak(final_transcript), name));
      };

      var two_line = /\n\n/g;
      var one_line = /\n/g;
      function linebreak(s) {
        return s.replace(two_line, "<p></p>").replace(one_line, "<br>");
      }
      var first_char = /\S/;
      function capitalize(s) {
        return s.replace(first_char, function(m) {
          return m.toUpperCase();
        });
      }

      recognition.onerror = function(event) {
        // console.log("Error has occured!");
      };

      recognition.onend = function() {
        console.log("Recognition has ended!");
      };
    }
  }

  render() {
    return (
      <span
        id="microphone-btn"
        className="icon-microphone-black-shape"
        onClick={this.toggleListen.bind(this, this.props.name)}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    voiceToText: state.commonReducer.voiceToText,
    isMiceEnabled: state.commonReducer.isMiceEnabled
  };
};

export default connect(mapStateToProps)(VoiceToText);
