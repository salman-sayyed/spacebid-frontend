import React, { Component } from "react";
import "./CarouselGroupStructure.scss";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 5
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2
  }
};
class CarouselGroupStructure extends Component {
  render() {
    return (
      <Carousel responsive={responsive} className="groupStructure">
        <div className="item1">
          <div
            data-aos="flip-left"
            data-aos-delay="100"
            data-aos-duration="3000"
          >
            Bajaj Holdings & Investment Limited (BHIL) (Listed)
          </div>
        </div>
        <div className="item2">
          <div
            data-aos="flip-left"
            data-aos-delay="100"
            data-aos-duration="3000"
          >
            Bajaj Auto Limited (Listed) <br />
            [Auto Business Arm]
          </div>
          <div
            data-aos="flip-left"
            data-aos-delay="100"
            data-aos-duration="3000"
          >
            Bajaj Finserv Limited (Listed) <br />
            [Financial Services Arm]
          </div>
        </div>
        <div className="item3">
          <div
            data-aos="flip-left"
            data-aos-delay="100"
            data-aos-duration="3000"
          >
            Bajaj Finance Limited (Listed) <br />
            [Lending Business Arm]
          </div>
          <div
            data-aos="flip-left"
            data-aos-delay="100"
            data-aos-duration="3000"
          >
            Bajaj Allianz Life Insurance Company Ltd <br />
            [Protection & Retiral]
          </div>
          <div
            data-aos="flip-left"
            data-aos-delay="100"
            data-aos-duration="3000"
          >
            Bajaj Allianz General Insurance Company Ltd <br />
            [Protection]
          </div>
        </div>
        <div className="item4">
          <div
            data-aos="flip-left"
            data-aos-delay="100"
            data-aos-duration="3000"
          >
            Bajaj Housing Finance Limited <br />
            [Mortgage Lending]
          </div>
          <div
            data-aos="flip-left"
            data-aos-delay="100"
            data-aos-duration="3000"
          >
            Bajaj Financial Securities Limited <br />
            [Broking & Depositary]
          </div>
        </div>
      </Carousel>
    );
  }
}
export default CarouselGroupStructure;
