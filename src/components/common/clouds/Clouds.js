import React, { Component } from "react";
import "./Clouds.scss";

export class Clouds extends Component {
  render() {
    return (
      <div className="cloudContainer">
        <div className="cloud1"></div>
        <div className="cloud2"></div>
        <div className="cloud3"></div>
      </div>
    );
  }
}

export default Clouds;
