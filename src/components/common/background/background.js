import React, { Component } from "react";
import ButtonToogle from "../buttonToggle/ButtonToggle";
import Stars from "../stars/Stars";
import "./Background.scss";
import Clouds from "../clouds/Clouds";
import { connect } from "react-redux";

class Background extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div
        className={
          this.props.isDarkThemeEnabled === true ? "dark-theme" : "light-theme"
        }
      >
        <div className="topBgGradient"></div>
        <div className="dayNightSwitch">
          <ButtonToogle checkedValue={this.checkedValue} type="theme" />
        </div>
        {/* Night Theme */}
        <div className="starContainer">
          <Stars />
        </div>
        {/* Day Theme */}
        <Clouds />

        <div className="bottomBgGradient"></div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isDarkThemeEnabled: state.commonReducer.isDarkThemeEnabled
  };
};

export default connect(mapStateToProps)(Background);
