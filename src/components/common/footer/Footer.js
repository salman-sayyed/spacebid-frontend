import React, { Component } from "react";
import "./Footer.scss";

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <h6>
          I'd be glad to lend a hand.{" "}
          <a href="mailto:ticc@bajajfinserv.in">
            <span> Let's touchbase!</span>
          </a>
        </h6>
        <div className="curtain5" />
        <div className="girlImage" />
        <div className="curtain6" />
        <div className="socialIcon">
          <a href="https://www.facebook.com/bajajfinserv/">
            <span className="icon-icons8-facebook-f"></span>
          </a>
          <a href="https://twitter.com/bajaj_finserv">
            <span className="icon-icons8-twitter"></span>
          </a>
        </div>
      </footer>
    );
  }
}

export default Footer;
