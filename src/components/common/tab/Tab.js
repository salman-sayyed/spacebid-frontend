import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import "./Tab.scss";
import PdfViewer from "../pdfViewer/PdfViewer";

class Tabpanel extends Component {
  constructor() {
    super();
    this.state = { tabIndex: 0 };
  }
  debugBase64 = base64URL => {
    var win = window.open();
    win.document.write(
      '<iframe src="' +
        base64URL +
        '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>'
    );
  };
  render() {
    let partners = [];
    for (let i = 0; i < this.props.dashboardData.partners.length; i++) {
      partners.push(
        <div className="grid-container">
          <div className="grid-item">
            <div>Partner 1</div>
            <div>{this.props.dashboardData.partners[i].name}</div>
          </div>
          <div className="grid-item">
            <div>Location</div>
            <div>{this.props.dashboardData.partners[i].location}</div>
          </div>
          <div className="grid-item">
            <div>Website URL</div>
            <div>{this.props.dashboardData.partners[i].url}</div>
          </div>
        </div>
      );
    }

    let mandatory = [];
    let optional = [];

    if (
      this.props.dashboardData.documents &&
      this.props.dashboardData.documents.mandatory
    ) {
      this.props.dashboardData.documents.mandatory.map(value => {
        let url = value.url
          ? value.url.replace(
              "https://bflticsbprodstorage.blob.core.windows.net",
              "http://spacebid.bajajfinserv.in/files"
            )
          : "";

        return mandatory.push(
          <div>
            <div className="upload-grid-item">
              <div>{value.name}</div>
              <div>
                <a href={url}> {value.fileName}</a>
              </div>
            </div>
          </div>
        );
      });
    }

    if (
      this.props.dashboardData.documents &&
      this.props.dashboardData.documents.optional
    ) {
      this.props.dashboardData.documents.optional.map(value => {
        let url = value.url
          ? value.url.replace(
              "https://bflticsbprodstorage.blob.core.windows.net",
              "http://spacebid.bajajfinserv.in/files"
            )
          : "";

        return optional.push(
          <div>
            <div className="upload-grid-item">
              <div>{value.name}</div>
              <div>
                <a href={url}> {value.fileName}</a>
              </div>
            </div>
          </div>
        );
      });
    }

    return (
      <Tabs
        className="tabContainer"
        selectedIndex={this.state.tabIndex}
        onSelect={tabIndex => this.setState({ tabIndex })}
      >
        <TabList>
          <Tab>SPOC DETAILS</Tab>
          <Tab>COMPANY DETAILS</Tab>
          <Tab>NDA/ TERMS & CONDITIONS</Tab>
        </TabList>
        <TabPanel key="personal">
          <div className="grid-container">
            <div className="grid-item">
              <div>Full name</div>
              <div>{this.props.dashboardData.spoc[0].fullName}</div>
            </div>
            <div className="grid-item">
              <div>Current Designation</div>
              <div>{this.props.dashboardData.spoc[0].designation}</div>
            </div>
            <div className="grid-item">
              <div>Company name</div>
              <div>{this.props.dashboardData.name}</div>
            </div>
            <div className="grid-item">
              <div>Mobile no.</div>
              <div>{this.props.dashboardData.spoc[0].mobile}</div>
            </div>
            <div className="grid-item">
              <div>Alternate Mobile no.</div>
              <div>
                {this.props.dashboardData.spoc[0].alternativeMobileNumber}
              </div>
            </div>
            <div className="grid-item">
              <div>Email address</div>
              <div>{this.props.dashboardData.spoc[0].officialEmail}</div>
            </div>
          </div>
        </TabPanel>
        <TabPanel key="company">
          {this.props.dashboardData ? (
            <>
              <div className="grid-container">
                <div className="grid-item">
                  <div>Company name</div>
                  <div>{this.props.dashboardData.name}</div>
                </div>
                <div className="grid-item">
                  <div>Headquarter</div>
                  <div>{this.props.dashboardData.headquarter}</div>
                </div>
                <div className="grid-item">
                  <div>Company domain</div>
                  <div>
                    {this.props.dashboardData.domains
                      ? this.props.dashboardData.domains.join(", ")
                      : ""}
                  </div>
                </div>
                <div className="grid-item">
                  <div>Is your company operational in India?</div>
                  <div>{this.props.dashboardData.isOperationalInIndia}</div>
                </div>
                <div className="grid-item">
                  <div>Website URL*</div>
                  <div>{this.props.dashboardData.website}</div>
                </div>
                <div className="grid-item">
                  <div>Year of incorporation</div>
                  <div>{this.props.dashboardData.incorporationYear}</div>
                </div>
                <div className="grid-item">
                  <div>No. of employees</div>
                  <div>{this.props.dashboardData.employeeCount}</div>
                </div>

                <div className="grid-item">
                  <div>Company Type</div>
                  <div>
                    {this.props.dashboardData.companyType
                      ? this.props.dashboardData.companyType.join(", ")
                      : ""}
                  </div>
                </div>

                <div className="grid-item">
                  <div>Current funding stage</div>
                  <div>{this.props.dashboardData.fundingStage}</div>
                </div>
                <div className="grid-item">
                  <div>Capital raised</div>
                  <div>
                    {this.props.dashboardData.capitalCurrency}{" "}
                    {this.props.dashboardData.capital}{" "}
                    {this.props.dashboardData.currencyIn}
                  </div>
                </div>
                <div className="grid-item">
                  <div>Key investers</div>
                  <div>
                    {this.props.dashboardData.investors
                      ? this.props.dashboardData.investors.join(", ")
                      : ""}
                  </div>
                </div>
                <div className="grid-item"></div>
                <div className="grid-item">
                  <div>Key Business Area</div>
                  <div>
                    {this.props.dashboardData.keyBusinessArea
                      ? this.props.dashboardData.keyBusinessArea.join(", ")
                      : ""}
                  </div>
                </div>
                <div className="grid-item">
                  <div>Key Technology Area</div>
                  <div>
                    {this.props.dashboardData.keyTechnologyArea
                      ? this.props.dashboardData.keyTechnologyArea.join(", ")
                      : ""}
                  </div>
                </div>
              </div>
              <div className="grid-container">
                <div className="grid-item">
                  <div className="info">Tell us about your company</div>
                  <div className="infoText">
                    {this.props.dashboardData.aboutCompany}
                  </div>
                </div>
              </div>
              <div className="grid-container"></div>
              <div className="grid-container"></div>
              <div className="title "> PARTNER DETAILS </div>
              <div className="partner">{partners}</div>
              <div className="title"> SOCIAL MEDIA </div>
              <div className="grid-container social">
                <div className="grid-item">
                  <div>
                    <span className="icon-icons8-linkedin-2 icon"></span>
                    {this.props.dashboardData.socialLinks &&
                      this.props.dashboardData.socialLinks.linkdin}
                  </div>
                </div>
                <div className="grid-item">
                  <div>
                    <span className="icon-crunchbase icon"></span>
                    {this.props.dashboardData.socialLinks &&
                      this.props.dashboardData.socialLinks.crunchBase}
                  </div>
                </div>
                <div className="grid-item">
                  <div>
                    <span className="icon-icons8-facebook-f icon"></span>
                    {this.props.dashboardData.socialLinks &&
                      this.props.dashboardData.socialLinks.facebook}
                  </div>
                </div>
                <div className="grid-item">
                  <div>
                    <span className="icon-icons8-twitter icon"></span>
                    {this.props.dashboardData.socialLinks &&
                      this.props.dashboardData.socialLinks.twitter}
                  </div>
                </div>
                <div className="grid-item">
                  <div>
                    <span class="icon-othericon2 icon"></span>
                    {this.props.dashboardData.socialLinks &&
                      this.props.dashboardData.socialLinks.other}
                  </div>
                </div>
              </div>
              <div className="title"> UPLOADS </div>
              <div className="upload-grid-container">{mandatory}</div>
              <div className="upload-grid-container">{optional}</div>{" "}
            </>
          ) : (
            ""
          )}
        </TabPanel>
        <TabPanel key="terms">
          <div>NDA/ TERMS & CONDITIONS</div>
          <PdfViewer />
        </TabPanel>
      </Tabs>
    );
  }
}
export default Tabpanel;
