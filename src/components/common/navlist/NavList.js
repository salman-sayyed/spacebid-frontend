import React, { Component } from "react";
import { List, ListItem, ListItemText } from "@material-ui/core";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { logout, isUserLoggedIn } from "../../../store/actions/loginAction";
import store from "../../../store/store";
import {
  onLoginSuccess,
  onSetStep,
  onLogoutSuccess
} from "../../../store/actions/commonActions";
import { withRouter } from "react-router-dom";

export class NavList extends Component {
  constructor(props) {
    super(props);
    this.onClickLogout = this.onClickLogout.bind(this);
    this.state = {
      isLoggedIn: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isLoggedIn === false) {
      store.dispatch(onLogoutSuccess());
      this.props.history.push("/login-form");
    }
  }

  componentDidMount() {
    if (isUserLoggedIn()) {
      //store.dispatch(onLoginSuccess());
    }
  }

  solutionPage = step => {
    this.props.handleDrawerClose();
    store.dispatch(onSetStep(step));
    this.props.history.push("/solution-form");
  };
  companyPage = step => {
    this.props.handleDrawerClose();
    store.dispatch(onSetStep(step));
    this.props.history.push("/company-form");
  };
  onClickLogout(e) {
    logout();
  }

  defaultDrawerDiv() {
    return (
      <List className="defaultNav">
        <ListItem>
          <ListItemText>
            <Link
              to={{
                pathname: "/",
                state: "desiredState"
              }}
            >
              HOME
            </Link>
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            <Link to="/introduction">INTRO TO BAJAJ</Link>
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            <Link to="/learn-about-TICC">LEARN ABOUT TICC</Link>
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            <Link to="/why-space-bid">WHY SPACE BID?</Link>
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            <Link to="/personal-detail-form">APPLY NOW</Link>
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            <a href="mailto:ticc@bajajfinserv.in" target="_top">
              CONTACT US
            </a>
          </ListItemText>
        </ListItem>
        <ListItem className="defaultNav">
          <ListItemText>
            <Link to="/login-form">Login</Link>
          </ListItemText>
        </ListItem>
      </List>
    );
  }

  loginDrawerDiv() {
    let index = 0;
    if (
      this.props.dashboardData.spoc[0].solutions &&
      this.props.dashboardData.spoc[0].solutions.length > 0
    ) {
      index = this.props.dashboardData.spoc[0].solutions
        ? this.props.dashboardData.spoc[0].solutions.length - 1
        : "";
    }
    let isComplete = this.props.dashboardData.spoc[0].solutions[index]
      ? this.props.dashboardData.spoc[0].solutions[index].isCompleted
      : "";
    if (isComplete === false) {
      return (
        <List className="logoutList">
          <ListItem>
            <ListItemText>
              <Link
                to={{
                  pathname: "/",
                  state: "desiredState"
                }}
              >
                HOME
              </Link>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <Link to="/introduction">INTRO TO BAJAJ</Link>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <Link to="/learn-about-TICC">LEARN ABOUT TICC</Link>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <Link to="/why-space-bid">WHY SPACE BID?</Link>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <Link to="#" onClick={this.solutionPage.bind(this, 1)}>
                SOLUTION DETAILS
              </Link>
            </ListItemText>
            <ListItemText>
              <Link to="#" onClick={this.solutionPage.bind(this, 4)}>
                About Solution
              </Link>
            </ListItemText>
            <ListItemText>
              <Link to="#" onClick={this.solutionPage.bind(this, 8)}>
                Implementaion Details
              </Link>
            </ListItemText>
            <ListItemText>
              <Link to="#" onClick={this.solutionPage.bind(this, 7)}>
                Clients
              </Link>
            </ListItemText>
            <ListItemText>
              <Link to="#" onClick={this.solutionPage.bind(this, 11)}>
                Cloud Compability
              </Link>
            </ListItemText>
          </ListItem>

          <ListItem>
            <ListItemText>
              <Link to="#" onClick={this.solutionPage.bind(this, 12)}>
                UPLOADS
              </Link>
            </ListItemText>
          </ListItem>

          <ListItem>
            <ListItemText>
              <Link to="/" onClick={this.onClickLogout.bind(this)}>
                Logout
              </Link>
            </ListItemText>
          </ListItem>
        </List>
      );
    } else {
      return (
        <List className="logoutList">
          <ListItem>
            <ListItemText>
              <Link
                to={{
                  pathname: "/",
                  state: "desiredState"
                }}
              >
                HOME
              </Link>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <Link to="/introduction">INTRO TO BAJAJ</Link>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <Link to="/learn-about-TICC">LEARN ABOUT TICC</Link>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <Link to="/why-space-bid">WHY SPACE BID?</Link>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <Link to="/dashboard">SPOC DETAILS</Link>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <Link to="/company-form">COMPANY DETAILS</Link>
            </ListItemText>
            <ListItemText>
              <Link to="#" onClick={this.companyPage.bind(this, 6)}>
                About Company
              </Link>
            </ListItemText>
            <ListItemText>
              <Link to="#" onClick={this.companyPage.bind(this, 7)}>
                Funding Details
              </Link>
            </ListItemText>
            <ListItemText>
              <Link to="#" onClick={this.companyPage.bind(this, 8)}>
                Partner Details
              </Link>
            </ListItemText>
            <ListItemText>
              <Link to="#" onClick={this.companyPage.bind(this, 10)}>
                Social Media
              </Link>
            </ListItemText>
          </ListItem>

          <ListItem>
            <ListItemText>
              <Link to="#" onClick={this.companyPage.bind(this, 11)}>
                UPLOADS
              </Link>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <Link
                to={{
                  pathname: "/dashboard"
                }}
              >
                Dashboard
              </Link>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <Link to="#query">CONTACT US</Link>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>
              <Link to="/" onClick={this.onClickLogout.bind(this)}>
                Logout
              </Link>
            </ListItemText>
          </ListItem>
        </List>
      );
    }
  }

  render() {
    let view = this.defaultDrawerDiv();
    if (isUserLoggedIn()) {
      view = this.loginDrawerDiv();
    }
    return view;
  }
}

const mapStateToProps = state => {
  return {
    dashboardData: state.dashboardDataReducer.data,
    isLoggedIn: state.commonReducer.isLoggedIn
  };
};

export default withRouter(connect(mapStateToProps)(NavList));
