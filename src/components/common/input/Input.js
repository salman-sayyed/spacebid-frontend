import React, { Component } from "react";
import "./Input.scss";
import TextField from "@material-ui/core/TextField";
import { FieldControl, Validators } from "react-reactive-form";

class Input extends Component {
  render() {
    const { values, placeholder, name, type, inputType } = this.props;

    return (
      // TODO replace input with material component
      <FieldControl
        name={name}
        render={({ handler, pending, touched, hasError }) => (
          <div>
            <TextField
              {...handler()}
              placeholder={placeholder}
              type={inputType}
              maxLength="2"
            />
            <div className="Validation">
              {(hasError("required") && (
                <Validator message="The fields marked with asterisk (*) are mandatory" />
              )) ||
                (hasError("email") && (
                  <Validator message="Please enter a valid email" />
                )) ||
                (hasError("notExist") && (
                  <Validator message="The fields marked with asterisk (*) are mandatory" />
                ))}
            </div>
          </div>
        )}
      />
    );
  }
}

export default Input;
