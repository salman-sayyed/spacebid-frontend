import React, { Component } from "react";
import "./Stars.scss";

export class Stars extends Component {
  render() {
    return (
      <div>
        <div className="starContainer1">
          <div className="star1 star"></div>
          <div className="star2 star"></div>
          <div className="star3 star"></div>
          <div className="star4 star"></div>
          <div className="star5 star"></div>
        </div>
        <div className="starContainer2">
          <div className="star1 star"></div>
          <div className="star2 star"></div>
          <div className="star3 star"></div>
          <div className="star4 star"></div>
          <div className="star5 star"></div>
        </div>
        <div className="starContainer2">
          <div className="star1 star"></div>
          <div className="star2 star"></div>
          <div className="star3 star"></div>
          <div className="star4 star"></div>
          <div className="star5 star"></div>
        </div>
      </div>
    );
  }
}

export default Stars;
