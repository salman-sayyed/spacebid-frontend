import React from "react";
import Slider from "infinite-react-carousel";
import "./CarouselStrategic.scss";

const settings = {
  slidesToShow: 1
};
const SimpleSlider = () => (
  <Slider className="cardContainer desktop" {...settings}>
    <div className="cardItems">
      <div className="card">
        <div className="image image1" />
        <div className="text">
          <h3>Point of Sale (POS)/Point of Purchase (POP) Innovations</h3>
        </div>
      </div>
      <div className="card">
        <div className="image image2" />
        <div className="text">
          <h3>
            Frictionless Search <br />&<br /> Purchase Enablers
          </h3>
        </div>
      </div>
      <div className="card">
        <div className="image image3" />
        <div className="text">
          <h3>Applied Artificial Intelligence (AI) & General AI Experiments</h3>
        </div>
      </div>
      <div className="card">
        <div className="image image4" />
        <div className="text">
          <h3>
            Hardware, Kiosks, Appliances <br />&<br /> Gadgets
          </h3>
        </div>
      </div>
    </div>
    <div className="cardItems">
      <div className="card">
        <div className="image image5" />
        <div className="text">
          <h3>
            Virtual Reality <br />&<br /> Augmented Reality
          </h3>
        </div>
      </div>
      <div className="card">
        <div className="image image6" />
        <div className="text">
          <h3>
            Projections <br />&<br /> Camera Tech
          </h3>
        </div>
      </div>
      <div className="card">
        <div className="image image7" />
        <div className="text">
          <h3>
            Mobility, Bots <br />&<br /> Apps
          </h3>
        </div>
      </div>
      <div className="card">
        <div className="image image8" />
        <div className="text">
          <h3>Internet of Things (IoT), Sensors & Devices</h3>
        </div>
      </div>
    </div>
    <div className="cardItems">
      <div className="card">
        <div className="image image9" />
        <div className="text">
          <h3>
            Design Thinking <br />
            (UX/UI)
          </h3>
        </div>
      </div>
      <div className="card">
        <div className="image image10" />
        <div className="text">
          <h3>Database & Cloud Infra Technologies</h3>
        </div>
      </div>
      <div className="card">
        <div className="image image11" />
        <div className="text">
          <h3>Application Programming Interface (API) & Programming</h3>
        </div>
      </div>
      <div className="card">
        <div className="image image12" />
        <div className="text">
          <h3>
            Conversational
            <br /> Search
          </h3>
        </div>
      </div>
    </div>
    <div className="cardItems">
      <div className="card">
        <div className="image image13" />
        <div className="text">
          <h3>
            Conversational <br /> Commerce
          </h3>
        </div>
      </div>
      <div className="card">
        <div className="image image14" />
        <div className="text">
          <h3>
            Conversational <br /> Analytics
          </h3>
        </div>
      </div>
      <div className="card">
        <div className="image image15" />
        <div className="text">
          <h3>
            Robotics <br />&<br /> Drones
          </h3>
        </div>
      </div>
      <div className="card">
        <div className="image image16" />
        <div className="text">
          <h3>
            Device <br /> Fingerprinting
          </h3>
        </div>
      </div>
    </div>
  </Slider>
);

export default SimpleSlider;
