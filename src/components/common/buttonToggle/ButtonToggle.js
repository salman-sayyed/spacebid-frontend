import React, { Component } from "react";
import Switch from "react-switch";
import "./ButtonToggle.scss";
import store from "../../../store/store";
import {
  onVoiceEnabled,
  onDarkThemeEnabled
} from "../../../store/actions/commonActions";
import { connect } from "react-redux";

class ButtonToggle extends Component {
  constructor() {
    super();
    this.state = {
      //checked: JSON.parse(localStorage.getItem("dark-theme"))
      //soundChecked: JSON.parse(localStorage.getItem("sound"))
    };
    //this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    if (this.props.isAutoTimeBasedThemeSetting) {
      var date = new Date();
      var hour = date.getHours();
      if (hour > 6 || hour < 18) {
        //localStorage.setItem("dark-theme", false);

        store.dispatch(onDarkThemeEnabled(false, true));
      }
      if (hour <= 6 || hour >= 18) {
        //localStorage.setItem("dark-theme", true);
        //store.dispatch(onDarkThemeEnabled(true))
        store.dispatch(onDarkThemeEnabled(true, true));
      }
    }
  }

  handleSoundChange = checked => {
    if (this.props.type === "sound" && this.props.isVoiceEnabled !== checked) {
      store.dispatch(onVoiceEnabled(checked));
      //this.setState({ soundChecked: checked });
      //const { checkedSoundValue } = this.props;
      //checkedSoundValue(this.props.isVoiceEnabled);
    }
  };

  handleThemeChange = checked => {
    if (
      this.props.type === "theme" &&
      this.props.isDarkThemeEnabled !== checked
    ) {
      store.dispatch(onDarkThemeEnabled(checked, false));
      // this.setState({ checked });
      // const { checkedValue } = this.props;
      // checkedValue(checked);
    }
  };

  render() {
    if (this.props.type === "theme") {
      // localStorage.setItem("dark-theme", this.state.checked);
    }
    // if (this.props.type == "sound") {
    //   localStorage.setItem("sound", this.state.soundChecked);
    // }
    if (this.props.type === "theme") {
      return (
        <div className="themeSwitch">
          <Switch
            className="react-switch"
            onChange={this.handleThemeChange.bind(this)}
            checked={this.props.isDarkThemeEnabled}
            // onColor="#fff"
            onHandleColor="#ff6666"
            // offColor="#fff"
            offHandleColor="#99ccff"
            checkedIcon=""
            checkedIcon={<span className="dayToggle"></span>}
            uncheckedIcon={
              <span className="icon-Night-Toggle">
                <span className="path1"></span>
                <span className="path2"></span>
                <span className="path3"></span>
                <span className="path4"></span>
                <span className="path5"></span>
                <span className="path6"></span>
                <span className="path7"></span>
                <span className="path8"></span>
                <span className="path9"></span>
                <span className="path10"></span>
                <span className="path11"></span>
                <span className="path12"></span>
                <span className="path13"></span>
                <span className="path14"></span>
                <span className="path15"></span>
                <span className="path16"></span>
                <span className="path17"></span>
                <span className="path18"></span>
                <span className="path19"></span>
                <span className="path20"></span>
                <span className="path21"></span>
                <span className="path22"></span>
                <span className="path23"></span>
                <span className="path24"></span>
                <span className="path25"></span>
                <span className="path26"></span>
                <span className="path27"></span>
                <span className="path28"></span>
                <span className="path29"></span>
                <span className="path30"></span>
                <span className="path31"></span>
                <span className="path32"></span>
                <span className="path33"></span>
              </span>
            }
          />
        </div>
      );
    }
    if (this.props.type === "sound") {
      return (
        <div className="soundSwitch">
          <Switch
            className="react-switch"
            onChange={this.handleSoundChange.bind(this)}
            checked={this.props.isVoiceEnabled}
            onColor="#fff"
            onHandleColor="#191a69"
            offColor="#fff"
            offHandleColor="#191a69"
            checkedIcon={<span className="icon-volume-mute"></span>}
            uncheckedIcon={<span className="icon-volume-high"></span>}
          />
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    isVoiceEnabled: state.commonReducer.isVoiceEnabled,
    isDarkThemeEnabled: state.commonReducer.isDarkThemeEnabled,
    isAutoTimeBasedThemeSetting: state.commonReducer.isAutoTimeBasedThemeSetting
  };
};

export default connect(mapStateToProps)(ButtonToggle);
