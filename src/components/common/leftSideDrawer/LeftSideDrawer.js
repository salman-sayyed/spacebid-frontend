import React, { Component } from "react";
import Drawer from "@material-ui/core/Drawer";
import IconButton from "@material-ui/core/IconButton";
import "./LeftSideDrawer.scss";
import NavList from "../navlist/NavList";
import store from "../../../store/store";
import { onSetStep } from "../../../store/actions/commonActions";

const drawerWidth = 240;

export default class SideDrawer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };
  }
  setOpen = value => {
    this.setState({
      open: value
    });
  };

  handleDrawerOpen = () => {
    this.setOpen(true);
  };

  handleDrawerClose = () => {
    store.dispatch(onSetStep(0));
    this.setOpen(false);
  };

  render() {
    return (
      <div className="">
        <IconButton onClick={this.handleDrawerOpen}>
          <span className="icon-nav"></span>
        </IconButton>

        <Drawer
          className=""
          variant="persistent"
          anchor="left"
          open={this.state.open}
        >
          <div className="">
            <IconButton onClick={this.handleDrawerClose}>
              <span className="icon-close"></span>
            </IconButton>
          </div>
          <NavList handleDrawerClose={this.handleDrawerClose} />
        </Drawer>
      </div>
    );
  }
}
