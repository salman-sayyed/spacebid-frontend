import React, { Component } from "react";
import { connect } from "react-redux";
import store from "../../../store/store";
import { onSubmitReset } from "../../../store/actions/commonActions";
import "./Validator.scss";
import Clippy from "../../common/formDetails/clippy/Clippy";

class Validator extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    if (this.props.toastr.message !== "") {
      setTimeout(function() {
        store.dispatch(onSubmitReset());
      }, 5000);
    }
    return (
      <div>
        {this.props.message ? (
          <div className="Validation">
            <Clippy message={this.props.message} />
          </div>
        ) : (
          this.props.toastr.message !== "" && (
            <div className="Validation">
              <Clippy message={this.props.toastr.message} />
            </div>
          )
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    toastr: state.commonReducer.toastr
  };
};

export default connect(mapStateToProps)(Validator);
