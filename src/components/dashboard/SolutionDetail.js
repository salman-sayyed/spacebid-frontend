import React, { Component } from "react";
import "./Dashboard.scss";
import "./SolutionDetail.scss";
import LeftSideDrawer from "../common/leftSideDrawer/LeftSideDrawer";
import ButtonToggle from "../common/buttonToggle/ButtonToggle";
import Stars from "../common/stars/Stars";
import "../common/stars/Stars.scss";
// import Tabpanel from "../common/tab/Tab";
import Clouds from "../common/clouds/Clouds";
import "../common/clouds/Clouds.scss";
// import { Carousel } from "react-responsive-carousel";
// import "react-responsive-carousel/lib/styles/carousel.min.css";
import AOS from "aos";
import "aos/dist/aos.css";
import { connect } from "react-redux";
import { getDashboardData } from "../../store/actions/dashboardDataAction";
import { getNotificationData } from "../../store/actions/notificationAction";
import store from "../../store/store";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";
import NotifyModal from "./NotifyModal";

AOS.init({
  delay: 100,
  mirror: true
});
class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scrollValue: 0
    };
  }

  componentDidMount() {
    store.dispatch(getDashboardData());
    store.dispatch(getNotificationData());
    localStorage.removeItem("solutionId");
  }

  onClick() {
    let showItems = this.state.showItems;
    showItems = !showItems;
    this.setState({ showItems });
  }

  createNotificationView() {
    let notificationView = [];
    for (var j = 0; j < this.props.notificationData.length; j++) {
      notificationView.push(
        <li className="unread">{this.props.notificationData[j].text}</li>
      );
    }
    return notificationView;
  }

  render() {
    let index = this.props.location.state.index
      ? this.props.location.state.index
      : 0;
    let solutionView = [];
    let notificationView = this.createNotificationView();

    for (
      let i = 0;
      i < this.props.dashboardData.spoc[0].solutions.length;
      i++
    ) {
      solutionView.push();
    }
    if (
      this.props.dashboardData &&
      this.props.dashboardData.spoc[0].solutions[index]
    ) {
      let imageurl =
        this.props.dashboardData.spoc[0].solutions[index].documents.mandatory &&
        this.props.dashboardData.spoc[0].solutions[index].documents
          .mandatory[0] &&
        this.props.dashboardData.spoc[0].solutions[index].documents.mandatory[0]
          .url
          ? this.props.dashboardData.spoc[0].solutions[index].documents
              .mandatory[0].url
          : "";

      let mandurl = imageurl.replace(
        "https://bflticsbprodstorage.blob.core.windows.net",
        "http://spacebid.bajajfinserv.in/files"
      );
      //console.log(mandurl);
      return (
        <div className="dashboard">
          <div
            className={
              this.props.isDarkThemeEnabled ? "dark-theme" : "light-theme"
            }
          >
            <nav>
              {/* <LeftSideDrawer /> */}
              <div className="back">
                <Link to="/dashboard">
                  <span className="icon-prev"></span>
                </Link>
              </div>
              <ButtonToggle checkedValue={this.checkedValue} type="theme" />
            </nav>
            <Stars />
            <Clouds />
            <div className="dashboardContainer solutiondetails">
              <header>
                <h1>Solution Name</h1>
              </header>
              <div className="solutionCarouselContainer">
                <div className="carousel carousel-slider">
                  <div className="slider">
                    <div className="slide">
                      <div className="Card">
                        <div className="solutionTitile">
                          <img
                            src={require("../../../src/assets/images/dashboard/solution-1.png")}
                            alt="Solution"
                          />
                          <h4>
                            {this.props.dashboardData.spoc[0].solutions[index]
                              .name
                              ? this.props.dashboardData.spoc[0].solutions[
                                  index
                                ].name
                              : ""}
                          </h4>
                        </div>
                        <div className="solutionDetail">
                          <h4>Your solution is under evaluation</h4>
                          <p>
                            You can keep an eye here or at your inbox for your
                            application status. Click on the banner to check
                            solution details.
                          </p>
                        </div>
                        <img
                          src="../../../src/assets/images/dashboard/solution-1.png"
                          className="imgNone"
                        ></img>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="tabContainer tabP1">
                <div className="react-tabs__tab-panel react-tabs__tab-panel--selected">
                  <div className="grid-container">
                    <div className="grid-item">
                      <div>Solution Details</div>
                      <div>
                        {this.props.dashboardData.spoc[0].solutions[index].name
                          ? this.props.dashboardData.spoc[0].solutions[index]
                              .name
                          : ""}
                      </div>
                    </div>
                    <div className="grid-item">
                      <div>Type</div>
                      <div>
                        {this.props.dashboardData.spoc[0].solutions[index].type
                          ? this.props.dashboardData.spoc[0].solutions[
                              index
                            ].type.join(", ")
                          : ""}
                      </div>
                    </div>
                    <div className="grid-item">
                      <div>Domains Served</div>
                      <div>
                        {this.props.dashboardData.spoc[0].solutions[index]
                          .domain
                          ? this.props.dashboardData.spoc[0].solutions[
                              index
                            ].domain.join(", ")
                          : ""}
                      </div>
                    </div>
                    <div className="grid-item">
                      <div>Solution Relevant For</div>
                      <div>
                        {this.props.dashboardData.spoc[0].solutions[index]
                          .relevance
                          ? this.props.dashboardData.spoc[0].solutions[
                              index
                            ].relevance.join(", ")
                          : ""}
                      </div>
                    </div>
                    <div className="grid-item">
                      <div>Technology Stack</div>
                      <div>
                        {this.props.dashboardData.spoc[0].solutions[index].stack
                          ? this.props.dashboardData.spoc[0].solutions[
                              index
                            ].stack.join(", ")
                          : ""}
                      </div>
                    </div>
                  </div>
                  <div className="info">
                    Share a brief discription of your solution{" "}
                  </div>
                  <div className="infoText infosolution">
                    {this.props.dashboardData.spoc[0].solutions[index]
                      .description
                      ? this.props.dashboardData.spoc[0].solutions[index]
                          .description
                      : ""}
                  </div>
                  <div className="info">
                    Current Challenges you are solving{" "}
                  </div>
                  <div className="infoText infosolution">
                    {this.props.dashboardData.spoc[0].solutions[index]
                      .challenges
                      ? this.props.dashboardData.spoc[0].solutions[index]
                          .challenges
                      : ""}
                  </div>
                  <div className="info">
                    What are your USP's of your solution?{" "}
                  </div>
                  <div className="infoText infosolution">
                    {this.props.dashboardData.spoc[0].solutions[index].usp
                      ? this.props.dashboardData.spoc[0].solutions[index].usp
                      : ""}
                  </div>
                  <div className="info">
                    How will it impact business? What are the benefit of BFL?{" "}
                  </div>
                  <div className="infoText infosolution">
                    {this.props.dashboardData.spoc[0].solutions[index].impact
                      ? this.props.dashboardData.spoc[0].solutions[index].impact
                      : ""}
                  </div>
                  {this.props.dashboardData.spoc[0].solutions[index].clients
                    ? this.props.dashboardData.spoc[0].solutions[
                        index
                      ].clients.map(value => {
                        return (
                          <div className="grid-container">
                            <div className="grid-item">
                              <div>Client name</div>
                              <div>{value.name}</div>
                            </div>
                            <div className="grid-item">
                              <div>Country</div>
                              <div>{value.country}</div>
                            </div>
                            <div className="grid-item">
                              <div>Website URL</div>
                              <div>{value.url}</div>
                            </div>
                          </div>
                        );
                      })
                    : ""}
                  <div className="grid-container">
                    <div className="grid-item">
                      <div>No. of Implementations so far</div>
                      <div>
                        {this.props.dashboardData.spoc[0].solutions[index]
                          .totalImplementations
                          ? this.props.dashboardData.spoc[0].solutions[index]
                              .totalImplementations
                          : ""}
                      </div>
                    </div>
                    <div className="grid-item">
                      <div>No. of Implementations in India</div>
                      <div>
                        {this.props.dashboardData.spoc[0].solutions[index]
                          .implementations
                          ? this.props.dashboardData.spoc[0].solutions[index]
                              .implementations
                          : ""}
                      </div>
                    </div>
                    <div className="grid-item">
                      <div>Type implementation timeline</div>
                      <div>
                        {this.props.dashboardData.spoc[0].solutions[index]
                          .implementationsTimeline
                          ? this.props.dashboardData.spoc[0].solutions[index]
                              .implementationsTimeline
                          : ""}
                      </div>
                    </div>
                  </div>
                  <div className="info">Commercial Model </div>
                  <div className="infoText infosolution">
                    {this.props.dashboardData.spoc[0].solutions[index]
                      .commercialModels
                      ? this.props.dashboardData.spoc[0].solutions[index]
                          .commercialModels
                      : ""}
                  </div>
                  <div className="info">
                    What is the future roadmap of your solution{" "}
                  </div>
                  <div className="infoText infosolution">
                    {this.props.dashboardData.spoc[0].solutions[index]
                      .futureRoadmap
                      ? this.props.dashboardData.spoc[0].solutions[index]
                          .futureRoadmap
                      : ""}
                  </div>
                  <div className="grid-container">
                    <div className="grid-item">
                      <div>Provider</div>
                      <div>
                        {this.props.dashboardData.spoc[0].solutions[index]
                          .provider
                          ? this.props.dashboardData.spoc[0].solutions[
                              index
                            ].provider.name.join(", ")
                          : ""}
                      </div>
                    </div>
                    <div className="grid-item">
                      <div>References</div>
                      <div>
                        {this.props.dashboardData.spoc[0].solutions[index]
                          .provider
                          ? this.props.dashboardData.spoc[0].solutions[index]
                              .provider.reference
                          : ""}
                      </div>
                    </div>
                    <div className="grid-item">
                      <div>Count</div>
                      <div>
                        {this.props.dashboardData.spoc[0].solutions[index]
                          .provider
                          ? this.props.dashboardData.spoc[0].solutions[index]
                              .provider.count
                          : ""}
                      </div>
                    </div>
                  </div>
                  <div className="title"> UPLOADS </div>
                  <div className="upload-grid-container">
                    <div className="upload-grid-item">
                      <div>
                        {this.props.dashboardData.spoc[0].solutions[index]
                          .documents.mandatory[0] &&
                        this.props.dashboardData.spoc[0].solutions[index]
                          .documents.mandatory[0].name
                          ? this.props.dashboardData.spoc[0].solutions[index]
                              .documents.mandatory[0].name
                          : ""}
                      </div>
                      <div>
                        <a href={mandurl}>
                          {" "}
                          {this.props.dashboardData.spoc[0].solutions[index]
                            .documents.mandatory[0] &&
                          this.props.dashboardData.spoc[0].solutions[index]
                            .documents.mandatory[0].fileName
                            ? this.props.dashboardData.spoc[0].solutions[index]
                                .documents.mandatory[0].fileName
                            : ""}
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="upload-grid-container">
                    {this.props.dashboardData.spoc[0].solutions[index].documents
                      .optional
                      ? this.props.dashboardData.spoc[0].solutions[
                          index
                        ].documents.optional.map(file => {
                          let url = file.url.replace(
                            "https://bflticsbprodstorage.blob.core.windows.net",
                            "http://spacebid.bajajfinserv.in/files"
                          );
                          return (
                            <div className="upload-grid-item">
                              <div>{file.name}</div>
                              <div>
                                <a href={url}> {file.fileName}</a>
                              </div>
                            </div>
                          );
                        })
                      : ""}
                  </div>
                </div>
              </div>
              <div className="footer">
                {/* <div>
                  Edit details? <Link to="/query">Click here</Link> to raise
                  request
                </div> */}
                <div>
                  Have Query? <Link to="/query">Ask here</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return "";
  }
}

const mapStateToProps = state => {
  return {
    dashboardData: state.dashboardDataReducer.data,
    notificationData: state.notificationDataReducer.data,
    isDarkThemeEnabled: state.commonReducer.isDarkThemeEnabled
  };
};

export default connect(mapStateToProps)(Dashboard);
