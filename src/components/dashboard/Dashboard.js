import React, { Component } from "react";
import "./Dashboard.scss";
import LeftSideDrawer from "../common/leftSideDrawer/LeftSideDrawer";
import ButtonToggle from "../common/buttonToggle/ButtonToggle";
import Stars from "../common/stars/Stars";
import "../common/stars/Stars.scss";
import Tabpanel from "../common/tab/Tab";
// import { element } from "prop-types";
import Clouds from "../common/clouds/Clouds";
import "../common/clouds/Clouds.scss";
// import Carousel from "../common/carousel";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import AOS from "aos";
import "aos/dist/aos.css";
import { connect } from "react-redux";
import { getDashboardData } from "../../store/actions/dashboardDataAction";
import {
  getNotificationData,
  updateNotificationData
} from "../../store/actions/notificationAction";
import store from "../../store/store";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";
import SolutionDetail from "./SolutionDetail";
import { Query } from "../query/Query";

AOS.init({
  delay: 100,
  mirror: true
});
class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scrollValue: 0,
      solutionPage: 0,
      showSolution: false,
      queryPopup: false,
      editPopup: false
    };
  }

  componentDidMount() {
    store.dispatch(getDashboardData());
    store.dispatch(getNotificationData());
    localStorage.removeItem("solutionId");
  }

  onClick() {
    let showItems = this.state.showItems;

    showItems = !showItems;
    this.setState({ showItems });
    if (this.state.showItems === true) {
      store.dispatch(getNotificationData());
    }
    setTimeout(() => {
      store.dispatch(updateNotificationData());
    }, 3000);
  }

  createNotificationView(notificationData) {
    let notificationView = [];
    if (notificationData.length === 0) {
      notificationView.push(
        <li className="unread">Notifications are not available</li>
      );
    }
    for (var j = 0; j < notificationData.length; j++) {
      notificationView.push(
        <li className="unread">{notificationData[j].text}</li>
      );
    }
    return notificationView;
  }
  onSolutionPage = () => {
    let index = 0;
    if (
      this.props.dashboardData.spoc[0].solutions &&
      this.props.dashboardData.spoc[0].solutions.length > 0
    ) {
      index = this.props.dashboardData.spoc[0].solutions.length - 1;
    }
    if (
      this.props.dashboardData.spoc[0].formStatus === "submitted" &&
      this.props.dashboardData.spoc[0].solutions[index].isCompleted === false
    ) {
      if (this.props.dashboardData.spoc[0].solutions[index].solutionPage > 0) {
        if (index >= 0) {
          localStorage.setItem(
            "solutionId",
            this.props.dashboardData.spoc[0].solutions[index]._id
          );
        }
        alert("You already have incomplete solution first fill that form");
        this.props.history.push("/solution-form");
      }
    } else if (
      this.props.dashboardData.spoc[0].formStatus === "drafted" &&
      this.props.dashboardData.spoc[0].solutions[index].isCompleted === false
    ) {
      if (this.props.dashboardData.spoc[0].solutions[index].solutionPage > 0) {
        if (index >= 0) {
          localStorage.setItem(
            "solutionId",
            this.props.dashboardData.spoc[0].solutions[index]._id
          );
        }
        this.props.history.push("/solution-form");
      }
    } else {
      this.props.history.push("/solution-form");
    }
  };
  showSolution = i => {
    this.props.history.push("/solution-detail");
    this.props.history.push({
      pathname: "/solution-detail",
      state: { index: i }
    });
  };

  queryhidePopup = () => {
    this.setState({ queryPopup: false, editPopup: false });
  };
  editQueryPopup = () => {
    this.setState({ editPopup: true, queryPopup: false });
  };
  render() {
    let { notificationData } = this.props;

    notificationData = notificationData.filter(m => m.isView != true);

    let solutionView = [];
    let notificationView = this.createNotificationView(notificationData);
    for (
      let i = 0;
      i < this.props.dashboardData.spoc[0].solutions.length;
      i++
    ) {
      solutionView.push(
        <div className="Card">
          <div className="solutionTitile">
            <img
              src={require("../../../src/assets/images/dashboard/solution-1.png")}
              alt="Solution"
            />
            <h4>{this.props.dashboardData.spoc[0].solutions[i].name}</h4>
          </div>
          <div
            className="solutionDetail"
            onClick={this.showSolution.bind(this, i)}
          >
            <h4>Your solution is under evaluation</h4>
            <p>
              You can keep an eye here or at your inbox for your application
              status. Click on the banner to check solution details.
            </p>
          </div>
          <img
            src="../../../src/assets/images/dashboard/solution-1.png"
            className="imgNone"
          ></img>
        </div>
      );
    }
    return (
      <div className="dashboard">
        <div
          className={
            this.props.isDarkThemeEnabled ? "dark-theme" : "light-theme"
          }
        >
          <div className="topBgGradient"></div>
          <nav>
            <LeftSideDrawer />
            <ButtonToggle checkedValue={this.checkedValue} type="theme" />
          </nav>
          <Stars />
          <Clouds />
          <div className="dashboardContainer">
            <header>
              <h1>Your Dashboard</h1>
              <div className="notify">
                <span
                  className="icon-notification"
                  onClick={this.onClick.bind(this)}
                >
                  <div className="detail">{notificationData.length}</div>
                </span>
                {this.state.showItems ? (
                  <ul className="notify-list">{notificationView}</ul>
                ) : null}
              </div>
            </header>

            <div className="solutionCarouselContainer">
              {/* <div className="solutionTag">Solution 1</div> */}
              <Carousel>{solutionView}</Carousel>
            </div>
            <div className="solutionAction">
              <Button onClick={this.onSolutionPage}>ADD MORE SOLUTIONS</Button>
            </div>
            {/* {this.state.showSolution === true ? <SolutionDetail index={this.state.solutionPage}/> :<Tabpanel dashboardData={this.props.dashboardData} /> } */}
            <Tabpanel dashboardData={this.props.dashboardData} />
            <div className="footer">
              {/* <div>
                Edit details?{" "}
                <Link
                  onClick={() => {
                    this.setState({ queryPopup: true });
                  }}
                >
                  Click here
                </Link>{" "}
                to raise request
              </div> */}
              <div id="query">
                Have Query?{" "}
                <Link
                  onClick={() => {
                    this.setState({ queryPopup: true });
                  }}
                >
                  Ask here
                </Link>
              </div>
              <Query
                popUp={this.state.queryPopup}
                editPopup={this.state.editPopup}
                queryhidePopup={this.queryhidePopup}
                editQueryPopup={this.editQueryPopup}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    dashboardData: state.dashboardDataReducer.data,
    notificationData: state.notificationDataReducer.data,
    isDarkThemeEnabled: state.commonReducer.isDarkThemeEnabled
  };
};

export default connect(mapStateToProps)(Dashboard);
