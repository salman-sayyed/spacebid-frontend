import React, { Component } from "react";
import "./Landing.scss";
import Header from "../common/header/Header";
import Article from "../common/article/Article";
// import Footer from '../common/footer/Footer';
import Background from "../common/background/background";
import store from "../../store/store";
import { getData } from "../../store/actions/getDataActions";
import {
  onVoiceEnabled,
  stopTextToVoice
} from "../../store/actions/commonActions";
import { isUserLoggedIn } from "../../store/actions/loginAction";
import { closeBrowser } from "../../store/actions/closeBrowserAction";

if (isUserLoggedIn()) {
  let data = "";
  window.onbeforeunload = function(e) {
    e.returnValue = "Are you sure you want to leave::::?";

    const name = localStorage.getItem("userName");
    const url = e.path[0].location.href;
    const email = localStorage.getItem("usermail");
    data = { name, email, url };
    if (
      window.location.pathname === "/company-form" ||
      window.location.pathname === "/solution-form"
    ) {
      store.dispatch(closeBrowser(data));
    }
  };
}

class Landing extends Component {
  componentDidMount() {
    // store.dispatch(getData());
  }
  componentWillUnmount() {
    store.dispatch(stopTextToVoice(true));
    store.dispatch(onVoiceEnabled(false));
    var synth = window.speechSynthesis;
    synth.pause();
    synth.cancel();
  }

  render() {
    return (
      <div>
        <Background />
        <div id="landingContainer" className="landingContainer">
          <Header />
          <Article />
          {/* <Footer />  */}
        </div>
      </div>
    );
  }
}

export default Landing;
