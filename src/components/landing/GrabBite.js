import React, { Component } from "react";
import "./GrabBite.scss";
import TypeTextToVoice from "../common/textToVoice/TypeTextToVoice";
import { Link } from "react-router-dom";
import Validator from "../common/leftSideDrawer/Validator";
import store from "../../store/store";
import { onTextToVoice } from "../../store/actions/commonActions";

class GrabBite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: 1,
      isClippy: false
    };
  }

  onSpeechEnd() {}

  componentWillMount() {
    this.onMessageshow();
  }
  onMessageshow = () => {
    let date = new Date();
    let t1 = date.getHours();

    // if (time[1] == "PM") t1 += 12;

    if (t1 >= 6 && t1 < 12) {
      this.setState({ message: "morning" });
    } else if (t1 >= 12 && t1 < 17) {
      this.setState({ message: "afternoon" });
    } else {
      this.setState({ message: "evening" });
    }
  };

  getMessage() {
    let { message } = this.state;

    let msg1;
    if (message == "morning")
      msg1 =
        '[ "It\'s beautiful morning! Let\'s grab a bite","Time for some conversations!"," Let\'s move ahead, select from below options" ]';
    else if (message == "afternoon")
      msg1 =
        '[ "Hope you are having a great afternoon! How about a spot of lunch?","Time for some conversations!","Let\'s move ahead, select from below options" ]';
    else
      msg1 =
        '[ "It\'s a wonderful evening. Let\'s get rolling!","Time for some conversations!","Let\'s move ahead, select from below options" ]';

    return msg1;
  }

  render() {
    let msg1 = this.getMessage();
    setTimeout(() => {
      this.setState({
        isClippy: true
      });
      store.dispatch(onTextToVoice("Already registered! Please login."));
    }, 17000);
    return (
      <div className="learningStep">
        <TypeTextToVoice
          name="Welcome 3"
          text={msg1}
          isVoiceEnabled={this.props.isVoiceEnabled}
        />
        {this.state.isClippy ? (
          <div className="Validation resend">
            <div className="resendOTP">
              <div className="validationmsgContainer">
                <div className="validationmsg">
                  Already registered!
                  <div
                    style={{
                      cursor: "pointer",
                      display: "flex",
                      justifyContent: " center",
                      marginTop: "0.3rem"
                    }}
                  >
                    <label style={{ marginRight: "0.3rem" }}>Please</label>
                    <div
                      className="resendLink"
                      onClick={() => {
                        this.props.historyObj.push("/login-form");
                      }}
                    >
                      login
                    </div>
                    .
                  </div>
                </div>

                <img
                  src={require("../../assets/images/clippy.png")}
                  alt="Validation"
                />
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
        <div id="heading">
          <h1 className="animation-text">
            <div className="typewrite" data-type={msg1}>
              <span className="wrap"></span>
            </div>
          </h1>
        </div>
        {/* <h2>It's beautiful evening, Let's grab a bite! Meanwhile, tell me how do you wanna start? </h2> */}
        <ul>
          <li>
            <Link to="/introduction">Intro to Bajaj</Link>
          </li>
          <li>
            <Link to="/learn-about-TICC">Learn about TICC</Link>
          </li>
          <li>
            <Link to="/why-space-bid">Why SpaceBid?</Link>
          </li>
          <li>
            <Link to="/personal-detail-form">Apply Now!</Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default GrabBite;
