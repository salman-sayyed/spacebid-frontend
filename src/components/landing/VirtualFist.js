import React, { Component } from "react";
import GrabBite from "./GrabBite";
import "./VirtualFist.scss";
import TypeTextToVoice from "../common/textToVoice/TypeTextToVoice";

class VirtualFist extends Component {
  VirtualFistText = '[ "Let\'s get started... Gimme a fist bump!" ]';

  constructor(props) {
    super(props);

    this.state = {
      showVirtualFist: false
    };
  }

  onSpeechEnd() {
    this.setState({ showVirtualFist: true });
  }

  render() {
    return (
      <React.Fragment>
        {!this.state.showVirtualFist ? (
          <div className="fistAnimationContainer">
            <div className="fistUp">
              <span className="icon-sparkup"></span>
            </div>
            <div className="fistAnimation">
              <div className="left">
                <img
                  src={require("../../assets/images/left-fist.png")}
                  alt="left-fist"
                />
              </div>
              <div className="right">
                <img
                  src={require("../../assets/images/right-fist.png")}
                  alt="right-fist"
                />
              </div>
            </div>
            <div className="fistDown">
              <span className="icon-sparkdown"></span>
            </div>
            <div id="heading">
              <h1 className="animation-text">
                <div className="typewrite" data-type={this.VirtualFistText}>
                  <span className="wrap"></span>
                </div>
              </h1>
            </div>
            <TypeTextToVoice
              name={"Welcome 1"}
              text={this.VirtualFistText}
              isVoiceEnabled={this.props.isVoiceEnabled}
              onSpeechEnd={this.onSpeechEnd.bind(this)}
            />
            {/* <h2>Glad you are here! Consider this a Virtual Fist bump!</h2> */}
          </div>
        ) : (
          <GrabBite
            isVoiceEnabled={this.props.isVoiceEnabled}
            historyObj={this.props.historyObj}
          />
        )}
      </React.Fragment>
    );
  }
}

export default VirtualFist;
