import React, { Component } from "react";
import "./Welcome.scss";
import VirtualFist from "./VirtualFist";
import TypeTextToVoice from "../common/textToVoice/TypeTextToVoice";
import { isUserLoggedIn } from "../../store/actions/loginAction";
import { withRouter } from "react-router-dom";
import store from "../../store/store";
import {
  onVoiceEnabled,
  stopTextToVoice
} from "../../store/actions/commonActions";

class Welcome extends Component {
  welcomeText =
    '[ "Hey, Welcome to SpaceBid","I am Blu, Your Virtual Assistant!"," I don\'t wear a cape but I can walk you through SpaceBid... hold on tightly to your jet-ski!" ]';

  constructor(props) {
    super(props);

    this.state = {
      showWelcome: false
    };
  }

  onSpeechEnd() {
    this.setState({ showWelcome: true });
  }

  myFunction() {
    document.getElementById("hiddenButton").click();
  }

  componentDidMount() {
    if (isUserLoggedIn()) {
      this.props.history.push("/");
    }
  }
  componentWillUnmount() {
    this.onSpeechEnd();
  }
  render() {
    return (
      <React.Fragment>
        {!this.state.showWelcome ? (
          <div id="heading" className="welcome">
            <h1 className="animation-text">
              <div className="typewrite" data-type={this.welcomeText}>
                <span className="wrap"></span>
              </div>
            </h1>
            <TypeTextToVoice
              name={"Welcome 1"}
              text={this.welcomeText}
              onSpeechEnd={this.onSpeechEnd.bind(this)}
            />
          </div>
        ) : (
          <VirtualFist historyObj={this.props.history} />
        )}
      </React.Fragment>
    );
  }
}

export default withRouter(Welcome);
