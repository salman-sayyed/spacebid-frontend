import React, { Component } from "react";
import "./Query.scss";
import { connect } from "react-redux";
import store from "../../store/store";
import ReactTable from "react-table";
import "react-table/react-table.css";
import {
  getQueryData,
  getSelectedQueryData
} from "../../store/actions/QueryAction";
import ModalPopup from "./ModalPopup";
import QueryPopup from "./QueryPopup";
import LeftSideDrawer from "../common/leftSideDrawer/LeftSideDrawer";
import ButtonToggle from "../common/buttonToggle/ButtonToggle";
import Stars from "../common/stars/Stars";
import "../common/stars/Stars.scss";
import Clouds from "../common/clouds/Clouds";
import "../common/clouds/Clouds.scss";
import { getDashboardData } from "../../store/actions/dashboardDataAction";
import moment from "moment";
import { Link } from "@reach/router";

export class Query extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPopup: false,
      showQueryPopup: false,
      slectedRowIndex: 0,
      scrollValue: 0,
      queryData: []
    };
  }
  componentDidMount() {
    if (
      !this.props.dashboardData ||
      !this.props.dashboardData.spoc ||
      this.props.dashboardData.spoc.length === 0 ||
      !this.props.dashboardData.spoc[0].officialEmail
    ) {
      store.dispatch(getDashboardData());
    }
    store.dispatch(getQueryData());
  }
  componentWillReceiveProps() {
    //store.dispatch(getQueryData());
  }
  handleOpen() {
    this.setState({ open: true });
  }
  queryhandleOpen() {
    this.setState({ showQueryPopup: true });
  }
  backToDashboard = () => {
    this.props.history.push("/dashboard");
  };
  hidePopup() {
    this.setState({ showPopup: false });
  }
  queryhidePopup = () => {
    this.setState({ showQueryPopup: false });
  };

  checkedValue = checked => {
    this.setState({
      themeValue: checked
    });
  };
  editQuery = (querydata, index) => {
    this.props.editQueryPopup();
    store.dispatch(getSelectedQueryData(querydata));
    this.setState({
      showPopup: true,
      slectedRowIndex: index,
      querydata: querydata
    });
  };
  render() {
    const columns = [
      {
        id: "Name", // Required because our accessor is not a string
        Header: "Name",
        accessor: d => d.messages[0].name // Custom value accessors!
      },
      {
        Header: "Subject",
        accessor: "subject"
        //Cell: props => <span className='number'>{props.value}</span> // Custom cell components!
      },
      {
        id: "Time",
        Header: "Time",
        //accessor: "lastUpdatedTime",
        accessor: d => {
          return moment(d.lastUpdatedTime).format("lll");
        }
      }
    ];

    let popupView;
    if (this.props.editPopup) {
      // console.log(this.state.querydata);
      popupView = (
        <ModalPopup
          query={
            this.state.querydata
              ? this.state.querydata[this.state.slectedRowIndex]
              : ""
          }
          selectedRowIndex={this.state.slectedRowIndex}
          onPopupClose={this.hidePopup.bind(this)}
        ></ModalPopup>
      );
    }

    if (this.props.popUp) {
      popupView = (
        <QueryPopup
          open={this.props.popUp} //this.state.showQueryPopup
          onPopupClose={this.props.queryhidePopup}
          queryhidePopup={this.props.queryhidePopup}
          editQuery={this.editQuery.bind(this)}
        ></QueryPopup>
      );
    }

    return (
      <div className="query">
        {popupView}

        {/* </div> */}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    querydata: state.queryDataReducer.data,
    dashboardData: state.dashboardDataReducer.data,
    isDarkThemeEnabled: state.commonReducer.isDarkThemeEnabled
  };
};

export default connect(mapStateToProps)(Query);
