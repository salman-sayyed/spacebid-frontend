import React, { Component } from "react";
import { connect } from "react-redux";
import store from "../../store/store";
import Modal from "@material-ui/core/Modal";
import "./ModalPopup.scss";
import Backdrop from "@material-ui/core/Backdrop";
import {
  replyQueryDetailData,
  uploadDocument
} from "../../store/actions/QueryAction";
import "filepond/dist/filepond.min.css";
import PropTypes from "prop-types";
import { Button } from "@material-ui/core";
import "./QueryPopup.scss";

var dateFormat = require("dateformat");
class ModalPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      replyText: "",
      files: []
    };
  }

  componentDidMount() {
    //store.dispatch(getQueryDetailData(this.props.query._id));
  }

  handleOpen() {
    this.setState({ open: true });
  }

  handleClose() {
    this.setState({ open: false });
    this.props.onPopupClose();
  }

  handleUpload() {
    store.dispatch(
      uploadDocument(
        this.state.files,
        this.props.selectedQuery.userId,
        this.props.selectedQuery._id
      )
    );
  }

  handleInit() {}

  handleChange(event) {
    this.setState({
      replyText: event.target.value
    });
  }

  handleChangeFile = e => {
    e.preventDefault();
    let file = e.target.files;

    this.setState({
      files: file
    });
  };

  renderMessages() {
    let documents = [];
    for (
      var i = 0;
      i < this.props.selectedQuery
        ? this.props.selectedQuery.attachments.length
        : 0;
      i++
    ) {
      documents.push(
        <div className="upload-grid-item">
          {/* <div>{this.props.query.attachments[i].fileName}</div> */}
          <div>
            <a href={this.props.selectedQuery.attachments[i].url}>
              {" "}
              {this.props.selectedQuery.attachments[i].fileName}
            </a>
          </div>
        </div>
      );
    }

    return (
      <div
        className={
          this.props.isDarkThemeEnabled === true
            ? "dark-theme modelContainer"
            : "light-theme modelContainer"
        }
      >
        <div className="model">
          <h1>
            Subject -{" "}
            {this.props.selectedQuery ? this.props.selectedQuery.subject : ""}
          </h1>
          <button className="close" onClick={this.handleClose.bind(this)}>
            <span className="icon-close"></span>
          </button>
          <div className="table-container">
            <table>
              <thead>
                <tr>
                  <th>Sender</th>
                  <th>Message</th>
                  <th>Date/Time</th>
                </tr>
              </thead>
              <tbody>{this.renderRows()}</tbody>
            </table>
            <div className="reply">
              <textarea
                ref="replyTextAreRef"
                placeholder="Type here..."
                onChange={event => this.handleChange(event)}
              ></textarea>
              <Button onClick={this.postReply.bind(this)}>Reply</Button>
              <div className="upload-container"></div>
              <div className="title"> Attachements </div>

              {/* <div className="title"> Upload </div> */}
              <input
                type="file"
                name="pic"
                onChange={this.handleChangeFile}
              ></input>
              <input
                type="submit"
                value="Upload File"
                name="submit"
                onClick={this.handleUpload.bind(this)}
              />
              <div className="upload-grid-container">
                <div className="upload-grid-item">{documents}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
    let messageRows = [];
    for (var i = 0; i < this.props.selectedQuery.messages.length; i++) {
      const message = this.props.selectedQuery.messages[i];
      var parseDate = dateFormat(message.dateTime, " mmm d, yyyy h:MM: TT");
      messageRows.push(
        <tr>
          <td>{message.name}</td>
          <td>{message.text}</td>
          <td>{parseDate}</td>
        </tr>
      );
    }
  }

  renderRows() {
    //let query = this.props.data[this.props.selectedRowIndex];
    let messageRows = [];
    for (var i = 0; i < this.props.selectedQuery.messages.length; i++) {
      const message = this.props.selectedQuery.messages[i];
      var parseDate = dateFormat(message.dateTime, " mmm d, yyyy h:MM: TT");
      messageRows.push(
        <tr>
          <td>{message.name}</td>
          <td>{message.text}</td>
          <td>{parseDate}</td>
        </tr>
      );
    }

    return messageRows;
  }

  postReply() {
    store.dispatch(
      replyQueryDetailData(
        this.props.selectedQuery,
        this.state.replyText,
        this.props.dashboardData.spoc[0]
      )
    );
    var replyTextAreRef = this.refs.replyTextAreRef;
    replyTextAreRef.value = "";
  }

  render() {
    // console.log(this.props.selectedQuery);
    //let query = this.props.data[this.props.selectedRowIndex];
    return (
      <div>
        {/* <button type="button" onClick={this.handleOpen.bind(this)}>
        react-transition-group
      </button> */}
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          //className={classes.modal}
          open={this.state.open}
          onClose={this.handleClose.bind(this)}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500
          }}
        >
          {/* <Fade in={this.state.open}>
          <div >
            <h2 id="transition-modal-title">Transition modal</h2>
            <p id="transition-modal-description">react-transition-group animates me.</p>
          </div>
        </Fade> */}
          {this.renderMessages()}
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedQuery: state.queryDataReducer.selectedQuery,
    dashboardData: state.dashboardDataReducer.data,
    isDarkThemeEnabled: state.commonReducer.isDarkThemeEnabled
  };
};

ModalPopup.propTypes = {
  messages: PropTypes.array
};

export default connect(mapStateToProps)(ModalPopup);
