import React, { Component } from "react";
import { connect } from "react-redux";
import store from "../../store/store";
import Modal from "@material-ui/core/Modal";
import "./ModalPopup.scss";
import Backdrop from "@material-ui/core/Backdrop";
import {
  getQueryDetailData,
  replyQueryDetailData,
  createQuerty,
  getQueryData
} from "../../store/actions/QueryAction";
import { FilePond } from "react-filepond";
import "filepond/dist/filepond.min.css";
import PropTypes from "prop-types";
import "./QueryPopup.scss";

var dateFormat = require("dateformat");
class QueryPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      text: "",
      subject: "",
      queryField: true,
      isCreateQuerty: false
    };
  }

  queryhidePopup() {
    this.setState({ open: false });
    this.props.onPopupClose();
  }

  OnClickSubmit() {
    store.dispatch(
      createQuerty(
        this.state.text,
        this.state.subject,
        this.props.dashboardData.spoc[0]
      )
    );
    this.setState({
      isCreateQuerty: false
    });
    // this.handleClose();
    // var replyTextAreRef = this.refs.replyTextAreRef;
    // replyTextAreRef.value = "";
    store.dispatch(getQueryData());
  }

  handleTextChange(event) {
    this.setState({
      text: event.target.value
    });
  }

  handleSubjectChange(event) {
    this.setState({
      subject: event.target.value
    });
  }

  handleClose = () => {
    this.setState({ open: false });
    this.props.onPopupClose();
  };

  renderMessages = () => {
    return (
      <div
        className={
          this.props.isDarkThemeEnabled === true
            ? "dark-theme modelContainer"
            : "light-theme modelContainer"
        }
      >
        <div className="model">
          <div className="modelClass">
            <h1>Ask Query</h1>
            <button
              onClick={() => {
                this.setState({ isCreateQuerty: true });
              }}
              class={
                this.state.isCreateQuerty ? "create-btn-enable" : "create-btn"
              }
            >
              CREATE QUERY
            </button>
            <button
              type="button"
              onClick={this.handleClose.bind(this)}
              className="close"
            >
              <span className="icon-close"></span>
            </button>
            {this.state.isCreateQuerty ? (
              <>
                <h2 id="simple-modal-title">
                  <input
                    type="text"
                    placeholder="Subject"
                    onChange={this.handleSubjectChange.bind(this)}
                  />
                </h2>
                <p id="simple-modal-description" class="descri-box">
                  <textarea
                    placeholder="Type query.."
                    onChange={this.handleTextChange.bind(this)}
                  ></textarea>
                  <div className="submit">
                    <button onClick={this.OnClickSubmit.bind(this)}>
                      Submit
                    </button>
                  </div>
                </p>
              </>
            ) : (
              ""
            )}
            <div class="query-list">
              <div class="title">Previous Queries</div>
              <table>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Subject</th>

                    <th>Date</th>
                    <th>Time</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.querydata
                    ? this.props.querydata.map((querydata, index) => {
                        var date = dateFormat(
                          querydata.dateTime,
                          " mmm d, yyyy "
                        );
                        var time = dateFormat(querydata.dateTime, "h:MM: TT");
                        return (
                          <tr>
                            <td>{localStorage.getItem("userName")}</td>
                            <td>{querydata.subject}</td>
                            <td>{date}</td>
                            <td>{time}</td>
                            <td>
                              <span
                                onClick={this.props.editQuery.bind(
                                  this,
                                  querydata,
                                  index
                                )}
                              >
                                <span class="icon-edit-2"></span>
                              </span>
                            </td>
                          </tr>
                        );
                      })
                    : ""}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  };

  componentDidMount() {
    this.setState({ open: this.props.open });
    // console.log(this.props.querydata)
  }

  onClose() {
    this.props.onPopupClose();
  }

  render() {
    return (
      <div>
        <div>
          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={this.props.open}
            onClose={this.onClose.bind(this)}
            closeAfterTransition
          >
            {this.renderMessages()}
          </Modal>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    querydata: state.queryDataReducer.data,
    query: state.queryDataReducer.selectedQuery,
    dashboardData: state.dashboardDataReducer.data,
    isDarkThemeEnabled: state.commonReducer.isDarkThemeEnabled
  };
};

QueryPopup.propTypes = {
  messages: PropTypes.array
};

export default connect(mapStateToProps)(QueryPopup);
