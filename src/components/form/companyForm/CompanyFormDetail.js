import React, { Component } from "react";
import "./CompanyFormDetail.scss";
import Background from "../../common/background/background";
import Header from "../../common/header/Header";
import { FormBuilder, FieldGroup, Validators } from "react-reactive-form";
import Navigator from "../../common/formDetails/navigator/Navigator";
import ComponentGenerator from "../../common/formDetails/componentGenerator/ComponentGenerator";
import { Redirect, Prompt } from "react-router-dom";
import store from "../../../store/store";
import {
  getMasterData,
  postMasterData
} from "../../../store/actions/masterDataAction";
import { updateCompanyDetails } from "../../../store/actions/updateCompanyAction";
import { uploadCompanyDocument } from "../../../store/actions/uploadDocumentAction";
import { connect } from "react-redux";
import { Button } from "@material-ui/core";
import LinearDeterminate from "../../common/progressbar/Progressbar";
import { deleteCompanyDocument } from "../../../store/actions/deleteFileAction";
import {
  onTextToVoice,
  onSetStep,
  isProduct,
  isSolutionService
} from "../../../store/actions/commonActions";
import TextToVoice from "../../common/textToVoice/TextToVoice";
import ButtonToggle from "../../common/buttonToggle/ButtonToggle";
import { getDashboardData } from "../../../store/actions/dashboardDataAction";
import Validator from "../../common/leftSideDrawer/Validator";
import Label from "../../common/formDetails/label/Label";
import { closeBrowser } from "../../../store/actions/closeBrowserAction";
import { isUserLoggedIn } from "../../../store/actions/loginAction";

var ReactCSSTransitionGroup = require("react-addons-css-transition-group");

const keyBusinessArea = [
  "Enterprise search",
  "Frictionless logins",
  "Anonymous user / visitor tracking and fingerprinting",
  "Marketing & engagements",
  "Frictionless purchase & payments",
  "Digital branch",
  "Retail store",
  "Digital contact centre",
  "Digital backend operations",
  "Risk & fraud management",
  "Workforce management",
  "IT support & services",
  "Monitoring & tracking",
  "Data analytics"
];
const keyTechnologyArea = [
  "POS / POP Innovations",
  "Frictionless Search & Purchase Enablers",
  "Applied AI & General AI Experiments",
  "Hardware, Kiosks, Appliances & Gadgets",
  "Virtual & Augmented Reality",
  "Projections & Camera Tech",
  "Mobility, Bots and Apps",
  "IoT, sensors & devices",
  "Device Fingerprinting",
  "Robotics & Drones",
  "Design Thinking",
  "DB & Cloud Infra technologies",
  "API & Programming",
  "Conversational Search",
  "Conversational Commerce",
  "Conversational Analytics"
];
const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));
const isEmpty = (control: AbstractControl) => {
  return sleep(1000).then(() => {
    if (control.value === "") {
      throw { notExist: true };
    } else {
      return null;
    }
  });
};
const isRequired = (control: AbstractControl) => {
  return sleep(1000).then(() => {
    if (control.value === "" || control.value === "Select") {
      throw { notExist: true };
    } else {
      return null;
    }
  });
};

export class CompanyFormDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 1,
      companyName: "",
      headquarter: "",
      companyDomain: "",
      websiteURL: "",
      incorporation: "",
      employees: "",
      operational: false,
      aboutCompany: "",
      stageFunding: "",
      capitalRaised: 0,
      keyInvestors: "",
      partnerName: "",
      location: "",
      websiteURL2: "",
      partners: [],
      linkedin: "",
      facebook: "",
      twitter: "",
      crunchBase: "",
      otherLink: "",
      deckfile: "",
      capitalCurrency: "INR",
      currencyIn: "",
      capabilityDeck: "Capability Deck",
      partnerfill: false,
      keyBusinessArea: "",
      companyType: "",
      isProduct: false,
      isSolutionServices: false,
      isBlocking: false
    };
    this.fillForm = this.fillForm.bind(this);
  }

  componentDidMount() {
    this.setState({
      isBlocking: true
    });
    store.dispatch(getDashboardData());
    if (
      this.props.dashboardData &&
      this.props.dashboardData.spoc[0].companyPage
    ) {
      this.setState({
        step: parseInt(this.props.dashboardData.spoc[0].companyPage) + 1
      });
    }
    store.dispatch(
      getMasterData(
        "Headquarter,Domain,FundingStage,KeyInvestor,KeyBusinessArea,KeyTechnologyArea"
      )
    );

    this.Form5.reset();
    if (this.props.dashboardData.partners.length > 0) {
      let data = {};
      let partners = [];
      this.props.dashboardData.partners.map(content => {
        data = {
          partnerName: content.name,
          location: content.location,
          websiteURL2: content.url
        };
        partners.push(data);
      });
      this.setState({ partners: partners });
    }
    let x = this.props.dashboardData ? this.setDefaultValue() : "";
    window.addEventListener("beforeunload", this.onUnload);
  }
  onUnload = () => {};
  subscribeFormChangeEvent(step) {
    switch (step) {
      case 3:
        this.Form1.get("headquarter").valueChanges.subscribe(value => {
          this.setState({
            headquarter: value
          });
        });
        this.Form1.get("companyDomain").valueChanges.subscribe(value => {
          this.setState({
            companyDomain: value ? value : ""
          });
        });
        this.Form1.get("companyName").valueChanges.subscribe(value => {
          this.setState({
            companyName: value
          });
        });
        break;
      case 4:
        this.Form2.get("operational").valueChanges.subscribe(value => {
          this.setState({
            operational: value
          });
        });
        this.Form2.get("websiteURL").valueChanges.subscribe(value => {
          this.setState({
            websiteURL: value
          });
        });
        this.Form2.get("incorporation").valueChanges.subscribe(value => {
          this.setState({
            incorporation: value
          });
        });
        this.Form2.get("employees").valueChanges.subscribe(value => {
          this.setState({
            employees: value
          });
        });
        break;
      case 5:
        this.Form7.get("companyType").valueChanges.subscribe(value => {
          this.setState({
            companyType: value
          });
        });
        this.Form7.get("keyBusinessArea").valueChanges.subscribe(value => {
          this.setState({
            keyBusinessArea: value ? value : ""
          });
        });
        this.Form7.get("keyTechnologyArea").valueChanges.subscribe(value => {
          this.setState({
            keyTechnologyArea: value ? value : ""
          });
        });
      case 6:
        this.Form3.get("aboutCompany").valueChanges.subscribe(value => {
          this.setState({
            aboutCompany: value
          });
        });
        break;
      case 7:
        this.Form4.get("stageFunding").valueChanges.subscribe(value => {
          this.setState({
            stageFunding: value
          });
        });
        this.Form4.get("capitalRaised").valueChanges.subscribe(value => {
          this.setState({
            capitalRaised: parseInt(value)
          });
        });
        this.Form4.get("keyInvestors").valueChanges.subscribe(value => {
          this.setState({
            keyInvestors: value
          });
          if (value) {
            //this.postTOMasterDropDown(value, "KeyInvestor");
          }
        });
        break;
      case 8:
        this.Form5.get("partnerName").valueChanges.subscribe(value => {
          this.setState({
            partnerName: value
          });
        });
        this.Form5.get("location").valueChanges.subscribe(value => {
          this.setState({
            location: value
          });
        });
        this.Form5.get("websiteURL2").valueChanges.subscribe(value => {
          this.setState({
            websiteURL2: value
          });
        });
        break;
      case 10:
        this.Form6.get("linkedIn").valueChanges.subscribe(value => {
          this.setState({
            linkedIn: value
          });
        });
        this.Form6.get("facebook").valueChanges.subscribe(value => {
          this.setState({
            facebook: value
          });
        });
        this.Form6.get("otherLink").valueChanges.subscribe(value => {
          this.setState({
            otherLink: value
          });
        });
        this.Form6.get("twitter").valueChanges.subscribe(value => {
          this.setState({
            twitter: value
          });
        });
        this.Form6.get("crunchBase").valueChanges.subscribe(value => {
          this.setState({
            crunchBase: value
          });
        });
        break;
      default:
    }
  }

  componentWillReceiveProps() {
    if (this.props.commonReducer.step && this.props.commonReducer.step > 0) {
      this.setState({
        step: this.props.commonReducer.step
      });

      store.dispatch(onSetStep(0));
    }
    if (
      this.props.commonReducer.voiceToText &&
      this.props.commonReducer.name === "aboutCompany"
    ) {
      this.Form3.get("aboutCompany").patchValue(
        this.props.commonReducer.voiceToText
      );
    }
  }

  createValue = (identifier, name, value) => {
    let obj;
    obj = {};
    obj.label = value;
    obj.value = value;
    let arr = [];
    if (name === "companyDomain") {
      arr = this.state.companyDomain ? this.state.companyDomain : [];
    } else if (name === "keyInvestors") {
      arr = this.state.keyInvestors ? this.state.keyInvestors : [];
    } else if (name === "keyTechnologyArea") {
      arr = this.state.keyTechnologyArea ? this.state.keyTechnologyArea : [];
    } else if (name === "keyBusinessArea") {
      arr = this.state.keyBusinessArea;
    }
    if (arr.length > 0) {
      arr.push(obj);
    } else {
      arr = [obj];
      if (name === "companyDomain") {
        this.Form1.get(name).setValue(obj);
      } else if (name === "keyInvestors") {
        this.Form4.get(name).setValue(obj);
      } else if (name === "keyTechnologyArea") {
        this.Form7.get(name).setValue(obj);
      } else if (name === "keyBusinessArea") {
        this.Form7.get(name).setValue(obj);
      }
    }
    this.setState({
      [name]: arr
    });
    store.dispatch(postMasterData(identifier, value));
  };
  handleCreate = (name, newValue) => {
    this.setState({
      [name]: newValue
    });
  };

  componentWillUnmount() {
    const { step } = this.state;
    switch (step) {
      case 3:
        this.Form1.get("headquarter").valueChanges.unsubscribe();
        this.Form1.get("companyDomain").valueChanges.unsubscribe();
        this.Form1.get("companyName").valueChanges.unsubscribe();
        break;
      case 4:
        this.Form2.get("operational").valueChanges.unsubscribe();
        this.Form2.get("websiteURL").valueChanges.unsubscribe();
        this.Form2.get("incorporation").valueChanges.unsubscribe();
        this.Form2.get("employees").valueChanges.unsubscribe();
        break;
      case 5:
        this.Form7.get("companyType").valueChanges.unsubscribe();
        this.Form7.get("keyBusinessArea").valueChanges.unsubscribe();
        this.Form7.get("keyTechnologyArea").valueChanges.unsubscribe();
      case 6:
        this.Form3.get("aboutCompany").valueChanges.unsubscribe();
        break;
      case 7:
        this.Form4.get("stageFunding").valueChanges.unsubscribe();
        this.Form4.get("capitalRaised").valueChanges.unsubscribe();
        this.Form4.get("keyInvestors").valueChanges.unsubscribe();
        break;
      case 8:
        this.Form5.get("partnerName").valueChanges.unsubscribe();
        this.Form5.get("location").valueChanges.unsubscribe();
        this.Form5.get("websiteURL2").valueChanges.unsubscribe();
        break;
      case 10:
        this.Form6.get("linkedIn").valueChanges.unsubscribe();
        this.Form6.get("facebook").valueChanges.unsubscribe();
        this.Form6.get("twitter").valueChanges.unsubscribe();
        this.Form6.get("crunchBase").valueChanges.unsubscribe();
        this.Form6.get("otherLink").valueChanges.unsubscribe();
        break;
      default:
    }
    // if (isUserLoggedIn()) {
    //   let data = "";

    //   if(typeof window['onbeforeunload'] === 'function'){
    //     window.onbeforeunload = function(e) {
    //       e.returnValue = "Are you sure you want to leave::::?";
    //       console.log("e",e,'window',window)
    //       const name = localStorage.getItem("userName");
    //       const url = e.path[0].location.href;
    //       const email = localStorage.getItem("usermail");
    //       data = { name, email, url };
    //       store.dispatch(closeBrowser(data));
    //     };
    //   }

    //   // window.removeEventListener("beforeunload", this.onUnload);
    // }
  }

  setDefaultValue = () => {
    this.Form1.get("companyName").setValue(
      this.props.dashboardData.name ? this.props.dashboardData.name : ""
    );
    this.Form1.get("headquarter").setValue(
      this.props.dashboardData.headquarter
        ? this.props.dashboardData.headquarter
        : "Select"
    );

    if (
      this.props.dashboardData.companyType &&
      this.props.dashboardData.companyType.length > 0
    ) {
      this.props.dashboardData.companyType.map(value => {
        if (value === "Product") {
          this.Form7.get("isProduct").setValue(true);
        }
        if (value === "Solution Service") {
          this.Form7.get("isSolutionService").setValue(true);
        }
      });
    }
    this.Form7.get("keyBusinessArea").setValue(
      this.props.dashboardData.keyBusinessArea ? this.getKeyBusiness() : ""
    );
    this.Form7.get("keyTechnologyArea").setValue(
      this.props.dashboardData.keyTechnologyArea ? this.getKeyTechnology() : ""
    );
    this.Form1.get("companyDomain").setValue(
      this.props.dashboardData.domains ? this.getOptions() : ""
    );
    this.Form2.get("operational").setValue(
      this.props.dashboardData.isOperationalInIndia &&
        this.props.dashboardData.isOperationalInIndia === "yes"
        ? true
        : false
    );
    if (this.props.dashboardData.isOperationalInIndia === "Yes") {
      this.setState({
        operational: true
      });
    } else {
      this.setState({
        operational: false
      });
    }
    this.Form2.get("websiteURL").setValue(
      this.props.dashboardData.website ? this.props.dashboardData.website : ""
    );
    this.Form2.get("incorporation").setValue(
      this.props.dashboardData.incorporationYear
        ? this.props.dashboardData.incorporationYear
        : ""
    );
    this.Form2.get("employees").setValue(
      this.props.dashboardData.employeeCount
        ? this.props.dashboardData.employeeCount
        : ""
    );
    this.Form3.get("aboutCompany").setValue(
      this.props.dashboardData.aboutCompany
        ? this.props.dashboardData.aboutCompany
        : ""
    );
    this.Form4.get("stageFunding").setValue(
      this.props.dashboardData.fundingStage
        ? this.props.dashboardData.fundingStage
        : "Select"
    );
    this.Form4.get("capitalCurrency").setValue(
      this.props.dashboardData.capitalCurrency
        ? this.props.dashboardData.capitalCurrency
        : "INR"
    );
    this.Form4.get("currencyIn").setValue(
      this.props.dashboardData.currencyIn
        ? this.props.dashboardData.currencyIn
        : "Thousand"
    );
    this.Form4.get("capitalRaised").setValue(
      this.props.dashboardData.capital ? this.props.dashboardData.capital : ""
    );
    this.Form4.get("keyInvestors").setValue(
      this.props.dashboardData.investors ? this.getInvestorOptions() : ""
    );
    if (
      this.props.dashboardData.partners &&
      this.props.dashboardData.partners > 0
    ) {
      let partners = this.state.partners;
      this.props.dashboardData.partners.map(content => {
        let data = {
          partnerName: content.name,
          location: content.location,
          websiteURL2: content.url
        };
        partners.push(data);
      });
      this.setState({
        partnerName: "",
        location: "",
        websiteURL2: "",
        partners
      });
      this.Form5.reset();
    }
    if (this.props.dashboardData.socialLinks) {
      this.Form6.get("facebook").setValue(
        this.props.dashboardData.socialLinks &&
          this.props.dashboardData.socialLinks.facebook
          ? this.props.dashboardData.socialLinks.facebook
          : ""
      );
      this.Form6.get("linkedIn").setValue(
        this.props.dashboardData.socialLinks &&
          this.props.dashboardData.socialLinks.linkdin
          ? this.props.dashboardData.socialLinks.linkdin
          : ""
      );
      this.Form6.get("otherLink").setValue(
        this.props.dashboardData.socialLinks &&
          this.props.dashboardData.socialLinks.other
          ? this.props.dashboardData.socialLinks.other
          : ""
      );
      this.props.dashboardData.socialLinks &&
        this.Form6.get("twitter").setValue(
          this.props.dashboardData.socialLinks.twitter
            ? this.props.dashboardData.socialLinks.twitter
            : ""
        );
      this.Form6.get("crunchBase").setValue(
        this.props.dashboardData.socialLinks &&
          this.props.dashboardData.socialLinks.crunchBase
          ? this.props.dashboardData.socialLinks.crunchBase
          : ""
      );
    }
  };

  Form1 = FormBuilder.group({
    companyName: ["", [Validators.required], isEmpty],
    headquarter: ["", [Validators.maxLength(50)]],
    companyDomain: [
      this.props.dropdownData["Domain"],
      [Validators.required, Validators.maxLength(50)],
      isEmpty
    ]
  });

  Form2 = FormBuilder.group({
    operational: [false, Validators.required],
    websiteURL: [
      "",
      [
        Validators.required,
        Validators.pattern(
          "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?"
        )
      ],
      isEmpty
    ],
    incorporation: ["", Validators.required, isEmpty],
    employees: ["", Validators.required, isEmpty]
  });
  Form3 = FormBuilder.group({
    aboutCompany: [
      "",
      [
        Validators.required,
        Validators.maxLength(1000),
        Validators.pattern("^[A-za-z0-9 ][^!@#$%^&*()_+={}?/<>~]+$")
      ],
      isEmpty
    ]
  });

  Form4 = FormBuilder.group({
    stageFunding: [
      "",
      [Validators.required, Validators.maxLength(50)],
      isRequired
    ],
    capitalCurrency: ["INR"],
    currencyIn: [""],
    capitalRaised: [0],
    keyInvestors: [[], [Validators.maxLength(50)]]
  });

  Form5 = FormBuilder.group({
    partnerName: [
      "",
      [
        Validators.required,
        Validators.maxLength(50),
        Validators.pattern("^[A-Za-z0-9 _-]+$")
      ],
      isEmpty
    ],
    location: [
      "",
      [
        Validators.required,
        Validators.maxLength(50),
        Validators.pattern("^[A-Za-z0-9 ,_-]+$")
      ],
      isEmpty
    ],
    websiteURL2: [
      "",
      [
        Validators.required,
        Validators.pattern(
          "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?"
        )
      ],
      isEmpty
    ]
  });

  Form6 = FormBuilder.group({
    linkedIn: [
      "",
      [
        Validators.pattern(
          "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?"
        )
      ]
    ],
    facebook: [
      "",
      [
        Validators.pattern(
          "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?"
        )
      ]
    ],
    twitter: [
      "",
      [
        Validators.pattern(
          "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?"
        )
      ]
    ],
    crunchBase: [
      "",
      [
        Validators.pattern(
          "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?"
        )
      ]
    ],
    otherLink: [
      "",
      [
        Validators.pattern(
          "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?"
        )
      ]
    ]
  });
  Form7 = FormBuilder.group({
    companyType: [""],
    isProduct: [false],
    isSolutionService: [false],
    keyBusinessArea: ["", Validators.required, isRequired],
    keyTechnologyArea: ["", Validators.required, isRequired]
  });

  handleSubmit = () => {};
  switchChange = checked => {
    this.setState({ operational: checked });
    this.Form2.get("operational").setValue(checked);
  };

  nextStep = () => {
    const { step } = this.state;

    this.setState({
      step: step + 1
    });

    store.dispatch(updateCompanyDetails(this.state));
  };
  addClientOnNext = () => {
    this.addPatner();
    this.nextStep();
  };
  prevStep = () => {
    const { step } = this.state;
    if (step > 1) {
      this.setState({
        step: step - 1
      });
    } else {
      this.setState({
        step: 1
      });
    }
  };

  addPatner = () => {
    const partner = {
      partnerName: this.state.partnerName,
      location: this.state.location,
      websiteURL2: this.state.websiteURL2
    };
    let partners = this.state.partners;
    partners.push(partner);
    this.Form5.reset();
    this.setState({
      partnerName: "",
      location: "",
      websiteURL2: "",
      partners
    });
  };

  clearForm = () => {
    this.setState({
      partnerfill: false
    });
    this.Form5.reset();
  };

  fillForm = selectedPartner => {
    this.setState({
      partnerfill: true
    });
    store.dispatch(updateCompanyDetails(this.state));
    if (this.props.dropdownData.partners) {
      store.dispatch(updateCompanyDetails(this.props.dropdownData));
    }
    this.Form5.get("partnerName").setValue(
      this.props.dropdownData.name
        ? this.props.dropdownData.name
        : selectedPartner.partnerName
    );
    this.Form5.get("location").setValue(
      this.props.dropdownData.location
        ? this.props.dropdownData.location
        : selectedPartner.location
    );
    this.Form5.get("websiteURL2").setValue(
      this.props.dropdownData.name
        ? this.props.dropdownData.url
        : selectedPartner.websiteURL2
    );
  };

  onFileChange = (title, uploadingContentKey, mandatory, e) => {
    e.preventDefault();
    let file = e.target.files[0];

    this.setState({
      [uploadingContentKey]: file
    });

    let storeValue = store.dispatch(
      uploadCompanyDocument(file, title, mandatory, uploadingContentKey)
    );

    return storeValue;
  };
  displayUploadTag() {
    if (parseInt(document.getElementById("dropDownId").value) === 1) {
      document.getElementById("uploadLink1").className = "uploadLink";
      let uploadLink1 = document.getElementById("dropDownId");
      uploadLink1.remove(uploadLink1.selectedIndex);
    }
    if (parseInt(document.getElementById("dropDownId").value) === 2) {
      document.getElementById("uploadLink2").className = "uploadLink";
      let uploadLink2 = document.getElementById("dropDownId");
      uploadLink2.remove(uploadLink2.selectedIndex);
    }
    if (parseInt(document.getElementById("dropDownId").value) === 3) {
      document.getElementById("uploadLink3").className = "uploadLink";
      let uploadLink3 = document.getElementById("dropDownId");
      uploadLink3.remove(uploadLink3.selectedIndex);
    }
    if (parseInt(document.getElementById("dropDownId").value) === 4) {
      document.getElementById("uploadLink4").className = "uploadLink";
      let uploadLink4 = document.getElementById("dropDownId");
      uploadLink4.remove(uploadLink4.selectedIndex);
    }
    if (parseInt(document.getElementById("dropDownId").value) === 5) {
      document.getElementById("uploadLink5").className = "uploadLink";
      let uploadLink5 = document.getElementById("dropDownId");
      uploadLink5.remove(uploadLink5.selectedIndex);
    }
    //alert(document.getElementById('dropDownId').value);
  }

  deleteUploadFile = mandatoryValue => {
    const doc_id = mandatoryValue
      ? this.props.companyData.documents
        ? this.props.companyData.documents.mandatory[0]._id
        : ""
      : this.props.companyData.documents
      ? this.props.companyData.documents.optional[0]._id
      : "";
    const mandatory = mandatoryValue;
    const company_id = this.props.companyData._id;
    store.dispatch(deleteCompanyDocument(doc_id, mandatory, company_id));
  };

  isUploading(uploadingContentKey) {
    let isContentExists = this.props.uploadingContents
      ? this.props.uploadingContents.find(content => {
          return content === uploadingContentKey;
        })
      : false;
    return (
      <div>
        {isContentExists ? <LinearDeterminate></LinearDeterminate> : ""}
      </div>
    );

    // ' {this.state.isSubmitting ?
    //   <LinearDeterminate  />
    // : 'Hello'}'
  }

  getOptions = () => {
    let obj;
    let options = [];
    if (this.props.dashboardData.domains) {
      this.props.dashboardData.domains.map(data => {
        obj = {};
        obj.value = data;
        obj.label = data;
        options.push(obj);
      });
    }
    return options;
  };
  getInvestorOptions = () => {
    let obj;
    let options = [];
    if (this.props.dashboardData.investors) {
      this.props.dashboardData.investors.map(data => {
        obj = {};
        obj.value = data;
        obj.label = data;
        options.push(obj);
      });
    }
    return options;
  };
  getKeyBusiness = () => {
    let obj;
    let options = [];
    if (this.props.dashboardData.keyBusinessArea) {
      this.props.dashboardData.keyBusinessArea.map(data => {
        obj = {};
        obj.value = data;
        obj.label = data;
        options.push(obj);
      });
    }
    return options;
  };
  getKeyTechnology = () => {
    let obj;
    let options = [];
    if (this.props.dashboardData.keyTechnologyArea) {
      this.props.dashboardData.keyTechnologyArea.map(data => {
        obj = {};
        obj.value = data;
        obj.label = data;
        options.push(obj);
      });
    }
    return options;
  };

  checkedSoundValue = checked => {
    this.setState({
      isVoiceEnabled: checked
    });
  };
  onSelectHandleChange = e => {
    const currency = e === "IN" ? "INR" : "Dollar";
    this.setState({
      capitalCurrency: currency
    });
    this.Form4.get("capitalCurrency").setValue(currency);
  };
  onRadioHandleChange = e => {
    e.preventDefault();
    this.setState({
      currencyIn: e.target.value
    });
    this.Form4.get("currencyIn").setValue(e.target.value);
  };
  checkedBox = name => {
    if (name == "isProduct") {
      store.dispatch(isProduct(!this.state.isProduct));
      this.setState({
        isProduct: !this.state.isProduct
      });
      this.Form7.get("isProduct").setValue(!this.state.isProduct);
    }
    if (name == "isSolutionServices") {
      this.setState({
        isSolutionServices: !this.state.isSolutionServices
      });
      this.Form7.get("isSolutionService").setValue(
        !this.state.isSolutionServices
      );
      store.dispatch(isSolutionService(!this.state.isSolutionServices));
    }
  };

  render() {
    const { incorporation, employees } = this.state;
    const { step } = this.state;

    this.subscribeFormChangeEvent(step);
    let name = this.props.dashboardData.spoc
      ? this.props.dashboardData.spoc[0].fullName
      : "";
    return (
      <React.Fragment>
        <Background />

        <div className="landingContainer">
          <Header />
          <TextToVoice />
          <Validator />

          <div className="articleContainer">
            <div
              className={
                this.props.isVoiceEnabledReducer ? "soundOn" : "soundOff"
              }
            >
              <ButtonToggle
                checkedSoundValue={this.checkedSoundValue.bind(this)}
                type="sound"
              />
            </div>
          </div>
          {this.props.dropdownData &&
          this.props.dropdownData.Headquarter &&
          this.props.dropdownData.Headquarter.length > 0 ? (
            <div className="companyformContainer scrollsolution">
              {
                {
                  1: (
                    <React.Fragment>
                      <ReactCSSTransitionGroup
                        transitionName="page"
                        transitionAppear={true}
                        transitionAppearTimeout={5000}
                        transitionEnter={true}
                        transitionLeave={true}
                      >
                        <div className="fieldClassCenter">
                          <ComponentGenerator
                            label={
                              "We love conversations - Let's kickoff one right away " +
                              name
                            }
                            type="title"
                          />
                        </div>
                      </ReactCSSTransitionGroup>
                      <Navigator
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleSubmit={this.handleSubmit}
                        // invalid={invalid}
                      />
                    </React.Fragment>
                  ),
                  2: (
                    <React.Fragment>
                      <ReactCSSTransitionGroup
                        transitionName="page"
                        transitionAppear={true}
                        transitionAppearTimeout={5000}
                        transitionEnter={true}
                        transitionLeave={true}
                      >
                        <div className="fieldClassCenter">
                          <ComponentGenerator
                            label="How's your workplace like? Drop details here"
                            type="title"
                          />
                        </div>
                      </ReactCSSTransitionGroup>
                      <Navigator
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleSubmit={this.handleSubmit}
                        // invalid={invalid}
                      />
                    </React.Fragment>
                  ),
                  3: (
                    <React.Fragment>
                      <FieldGroup
                        key="Form1"
                        control={this.Form1}
                        render={({ invalid, get }) => (
                          <form
                            onSubmit={this.handleSubmit}
                            className="compny1"
                          >
                            <ReactCSSTransitionGroup
                              transitionName="page"
                              transitionAppear={true}
                              transitionAppearTimeout={5000}
                              transitionEnter={true}
                              transitionLeave={true}
                            >
                              <ComponentGenerator
                                className="formTitle"
                                label="Give us a virtual feel of your workplace"
                                type="title"
                              />
                              <ComponentGenerator
                                type="input"
                                placeholder="Company Name"
                                name="companyName"
                                label="Name of the organization you work for*"
                                inputType="text"
                                disabled={true}
                                values={this.Form1.meta}
                                maxlength={100}
                                voiceText="Name of the organization you work for"
                              />
                              <ComponentGenerator
                                type="dropdown"
                                placeholder={
                                  this.props.dashboardData.headquarter
                                    ? this.props.dashboardData.headquarter
                                    : "Select"
                                }
                                name="headquarter"
                                label="Your organization is headquartered at"
                                inputType="dropdown"
                                disabled={
                                  this.props.dashboardData.headquarter
                                    ? true
                                    : false
                                }
                                values={this.Form1.value.headquarter}
                                dropdownData={
                                  this.props.dropdownData.Headquarter
                                }
                                voiceText="Your organization is headquartered at"
                                clippyText={"Your organization's headquarter"}
                              />
                              <ComponentGenerator
                                type="multiSelect"
                                placeholder="Company domain"
                                name="companyDomain"
                                label="Your business domain is into*"
                                inputType="dropdown"
                                identifier="Domain"
                                createValue={this.createValue.bind(this)}
                                disabled={
                                  this.props.dashboardData.domains &&
                                  this.props.dashboardData.domains.length > 0
                                    ? true
                                    : false
                                }
                                clippyText={"Select your buisness domain"}
                                handleCreate={this.handleCreate}
                                values={this.Form1.value.companyDomain}
                                dropdownData={this.props.dropdownData.Domain}
                                voiceText="Your business domain is into"
                              />
                            </ReactCSSTransitionGroup>

                            <Navigator
                              nextStep={this.nextStep}
                              prevStep={this.prevStep}
                              handleSubmit={this.handleSubmit}
                              invalid={invalid}
                              step={this.state.step}
                            />
                          </form>
                        )}
                      />
                    </React.Fragment>
                  ),
                  4: (
                    <React.Fragment>
                      <FieldGroup
                        key="Form2"
                        control={this.Form2}
                        render={({ invalid, get }) => (
                          <form onSubmit={this.handleSubmit}>
                            <FieldGroup
                              key="Form1"
                              control={this.Form1}
                              render={({ invalid, get }) => (
                                <form
                                  onSubmit={this.handleSubmit}
                                  className="compny1 blur"
                                >
                                  <ReactCSSTransitionGroup
                                    transitionName="page"
                                    transitionAppear={true}
                                    transitionAppearTimeout={5000}
                                    transitionEnter={true}
                                    transitionLeave={true}
                                  >
                                    <ComponentGenerator
                                      type="multiSelect"
                                      placeholder="Company domain"
                                      name="companyDomain"
                                      label="Your business domain is into*"
                                      inputType="dropdown"
                                      identifier="Domain"
                                      createValue={this.createValue.bind(this)}
                                      disabled={
                                        this.props.dashboardData.domains &&
                                        this.props.dashboardData.domains
                                          .length > 0
                                          ? true
                                          : false
                                      }
                                      handleCreate={this.handleCreate}
                                      values={this.Form1.value.companyDomain}
                                      dropdownData={
                                        this.props.dropdownData.Domain
                                      }
                                      voiceText="Your business domain is into"
                                    />
                                  </ReactCSSTransitionGroup>
                                </form>
                              )}
                            />
                          </form>
                        )}
                      />
                      <div className="compny2">
                        <ReactCSSTransitionGroup
                          transitionName="page"
                          transitionAppear={true}
                          transitionAppearTimeout={5000}
                          transitionEnter={true}
                          transitionLeave={true}
                        >
                          <ComponentGenerator
                            type="toggle"
                            name="operational"
                            label="Do you have operations in India?*"
                            values={this.state.operational}
                            maxlength={100}
                            switchChange={this.switchChange}
                            disabled={
                              this.props.dashboardData.isOperationalInIndia !==
                              "No"
                                ? true
                                : false
                            }
                            voiceText="Do you have operations in India?"
                            onFocus={true}
                          />
                        </ReactCSSTransitionGroup>
                        <FieldGroup
                          key="Form2"
                          control={this.Form2}
                          render={({ invalid, get }) => (
                            <form onSubmit={this.handleSubmit}>
                              <ReactCSSTransitionGroup
                                transitionName="page"
                                transitionAppear={true}
                                transitionAppearTimeout={5000}
                                transitionEnter={true}
                                transitionLeave={true}
                              >
                                <ComponentGenerator
                                  type="input"
                                  placeholder="Type website URL"
                                  name="websiteURL"
                                  label="Can you provide the website URL of your organization?*"
                                  inputType="text"
                                  disabled={false}
                                  maxlength={100}
                                  disabled={
                                    this.props.dashboardData.website
                                      ? true
                                      : false
                                  }
                                  voiceText="Can you provide the website URL of your organization?"
                                  clippyText={"Please enter valid URL"}
                                />
                                <ComponentGenerator
                                  type="number"
                                  name="incorporation"
                                  label="Your organization was incorporated in the year*"
                                  placeholder="0000"
                                  inputType="number"
                                  disabled={false}
                                  decimalScale={0}
                                  format={"####"}
                                  values={incorporation}
                                  disabled={
                                    this.props.dashboardData.incorporationYear
                                      ? true
                                      : false
                                  }
                                  clippyText={"Only numbers (0-9) are allowed"}
                                  voiceText="Your organization was incorporated in the year"
                                />
                                <ComponentGenerator
                                  type="number"
                                  placeholder="0000"
                                  name="employees"
                                  label="Number of employees working in your organization*"
                                  inputType="number"
                                  decimalScale={0}
                                  disabled={false}
                                  values={employees}
                                  disabled={
                                    this.props.dashboardData.employeeCount
                                      ? true
                                      : false
                                  }
                                  clippyText={"Only numbers (0-9) are allowed"}
                                  voiceText="Number of employees working in your organization"
                                />
                              </ReactCSSTransitionGroup>
                              <Navigator
                                nextStep={this.nextStep}
                                prevStep={this.prevStep}
                                invalid={invalid}
                                handleSubmit={this.handleSubmit}
                              />
                            </form>
                          )}
                        />
                      </div>
                    </React.Fragment>
                  ),
                  5: (
                    <>
                      <FieldGroup
                        key="Form7"
                        control={this.Form7}
                        meta={{
                          isProduct: this.state.isProduct,
                          isSolutionServices: this.state.isProduct
                        }}
                        render={({ invalid, get }) => (
                          <form
                            onSubmit={this.handleSubmit}
                            className="marginTop25"
                          >
                            <ReactCSSTransitionGroup
                              transitionName="page"
                              transitionAppear={true}
                              transitionAppearTimeout={5000}
                              transitionEnter={true}
                              transitionLeave={true}
                            >
                              <FieldGroup
                                key="Form2"
                                control={this.Form2}
                                render={({ invalid, get }) => (
                                  <form
                                    onSubmit={this.handleSubmit}
                                    className="blur margin-top-bottom numberInput "
                                  >
                                    <ComponentGenerator
                                      type="number"
                                      placeholder="0000"
                                      name="employees"
                                      label="Number of employees working in your organization*"
                                      inputType="number"
                                      decimalScale={0}
                                      disabled={false}
                                      values={employees}
                                      disabled={
                                        this.props.dashboardData.employeeCount
                                          ? true
                                          : false
                                      }
                                      voiceText="Number of employees working in your organization"
                                    />
                                  </form>
                                )}
                              />
                              <div className="fieldClass">
                                <Label label={"Company Type*"} />
                              </div>
                              <div
                                className="checkbox-filed"
                                style={{
                                  pointerEvents:
                                    this.props.dashboardData.companyType &&
                                    this.props.dashboardData.companyType
                                      .length > 0
                                      ? "none"
                                      : "pointer"
                                }}
                              >
                                <ComponentGenerator
                                  type="checkbox"
                                  name="isProduct"
                                  label="Product"
                                  values={
                                    this.Form7.value.isProduct ? true : false
                                  }
                                  checkedBox={this.checkedBox}
                                  disabled={
                                    this.Form7.value.isProduct ? true : false
                                  }
                                  clippyText={"Select company Type"}
                                />
                                <ComponentGenerator
                                  type="checkbox"
                                  name="isSolutionServices"
                                  label="Solution Services"
                                  values={
                                    this.Form7.value.isSolutionService
                                      ? true
                                      : false
                                  }
                                  checkedBox={this.checkedBox}
                                  disabled={
                                    this.Form7.value.isSolutionService
                                      ? true
                                      : false
                                  }
                                />
                              </div>

                              <ComponentGenerator
                                type="multiSelect"
                                placeholder="Key Business Area"
                                name="keyBusinessArea"
                                label="Key Business Area*"
                                inputType="dropdown"
                                identifier="KeyBusinessArea"
                                values={this.state.keyBusinessArea}
                                dropdownData={
                                  this.props.dropdownData.KeyBusinessArea
                                }
                                handleCreate={this.handleCreate}
                                createValue={this.createValue.bind(this)}
                                disabled={
                                  this.props.dashboardData.keyBusinessArea &&
                                  this.props.dashboardData.keyBusinessArea
                                    .length > 0
                                    ? true
                                    : false
                                }
                                voiceText="Key Business Area"
                                clippyText={"Select Key Business Area"}
                              />
                              <ComponentGenerator
                                type="multiSelect"
                                placeholder="Key Technology Area"
                                name="keyTechnologyArea"
                                label="Key Technology Area*"
                                inputType="dropdown"
                                identifier="KeyTechnologyArea"
                                values={this.state.keyTechnologyArea}
                                dropdownData={
                                  this.props.dropdownData.KeyTechnologyArea
                                }
                                handleCreate={this.handleCreate}
                                createValue={this.createValue.bind(this)}
                                disabled={
                                  this.props.dashboardData.keyTechnologyArea &&
                                  this.props.dashboardData.keyTechnologyArea
                                    .length > 0
                                    ? true
                                    : false
                                }
                                voiceText="Key Technology Area"
                                clippyText={"Select Key Technology Area"}
                              />
                            </ReactCSSTransitionGroup>
                            <Navigator
                              nextStep={this.nextStep}
                              prevStep={this.prevStep}
                              invalid={
                                invalid
                                  ? true
                                  : this.Form7.value.isProduct ||
                                    this.Form7.value.isSolutionService
                                  ? false
                                  : true
                              }
                              handleSubmit={this.handleSubmit}
                            />
                          </form>
                        )}
                      />
                    </>
                  ),
                  6: (
                    <FieldGroup
                      key="Form3"
                      control={this.Form3}
                      render={({ invalid, get }) => (
                        <form
                          onSubmit={this.handleSubmit}
                          className="marginTop25"
                        >
                          <FieldGroup
                            key="Form7"
                            control={this.Form7}
                            render={({ invalid, get }) => (
                              <form
                                onSubmit={this.handleSubmit}
                                className="blur margin-top-bottom numberInput "
                              >
                                <ReactCSSTransitionGroup
                                  transitionName="page"
                                  transitionAppear={true}
                                  transitionAppearTimeout={5000}
                                  transitionEnter={true}
                                  transitionLeave={true}
                                >
                                  <ComponentGenerator
                                    type="multiSelect"
                                    placeholder="Key Technology Area"
                                    name="keyTechnologyArea"
                                    label="Key Business Area*"
                                    inputType="dropdown"
                                    identifier="KeyTechnologyArea"
                                    values={this.state.keyTechnologyArea}
                                    dropdownData={
                                      this.props.dropdownData.KeyTechnologyArea
                                    }
                                    handleCreate={this.handleCreate}
                                    createValue={this.createValue.bind(this)}
                                    disabled={
                                      this.props.dashboardData
                                        .keyTechnologyArea &&
                                      this.props.dashboardData.keyTechnologyArea
                                        .length > 0
                                        ? true
                                        : false
                                    }
                                    voiceText="Key Technology Area"
                                  />
                                </ReactCSSTransitionGroup>
                              </form>
                            )}
                          />
                          <ReactCSSTransitionGroup
                            transitionName="page"
                            transitionAppear={true}
                            transitionAppearTimeout={5000}
                            transitionEnter={true}
                            transitionLeave={true}
                          >
                            <ComponentGenerator
                              type="textArea"
                              name="aboutCompany"
                              label="Can you tell me something about your organization?*"
                              placeholder="About your company"
                              inputType="text"
                              disabled={
                                this.props.dashboardData.aboutCompany
                                  ? true
                                  : false
                              }
                              values={
                                this.props.commonReducer.voiceToText &&
                                this.props.commonReducer.name === "aboutCompany"
                                  ? this.props.commonReducer.voiceToText
                                  : ""
                              }
                              voiceText="Can you tell me something about your organization?"
                              onFocus={true}
                              clippyText={
                                "You can also give voice command to fill this field"
                              }
                            />
                          </ReactCSSTransitionGroup>

                          <Navigator
                            nextStep={this.nextStep}
                            prevStep={this.prevStep}
                            invalid={
                              invalid ||
                              this.Form3.value.aboutCompany.match(
                                "[^!@#$%^&*()_+={}?/<>~]+$"
                              ) === null
                                ? true
                                : false
                            }
                            handleSubmit={this.handleSubmit}
                          />
                        </form>
                      )}
                    />
                  ),
                  7: (
                    <React.Fragment>
                      <FieldGroup
                        key="Form4"
                        control={this.Form4}
                        render={({ invalid, get }) => (
                          <form
                            onSubmit={this.handleSubmit}
                            className="compny1"
                          >
                            <FieldGroup
                              key="Form3"
                              control={this.Form3}
                              render={({ invalid, get }) => (
                                <form
                                  onSubmit={this.handleSubmit}
                                  className="blur margin-top"
                                >
                                  <ReactCSSTransitionGroup
                                    transitionName="page"
                                    transitionAppear={true}
                                    transitionAppearTimeout={5000}
                                    transitionEnter={true}
                                    transitionLeave={true}
                                  >
                                    <ComponentGenerator
                                      type="textArea"
                                      name="aboutCompany"
                                      label="Can you tell me something about your organization?*"
                                      placeholder="About your company"
                                      inputType="text"
                                      disabled={
                                        this.props.dashboardData.aboutCompany
                                          ? true
                                          : false
                                      }
                                      values={
                                        this.props.commonReducer.voiceToText &&
                                        this.props.commonReducer.name ===
                                          "aboutCompany"
                                          ? this.props.commonReducer.voiceToText
                                          : ""
                                      }
                                      voiceText="Can you tell me something about your organization?"
                                    />
                                  </ReactCSSTransitionGroup>
                                </form>
                              )}
                            />
                            <ReactCSSTransitionGroup
                              transitionName="page"
                              transitionAppear={true}
                              transitionAppearTimeout={5000}
                              transitionEnter={true}
                              transitionLeave={true}
                            >
                              <ComponentGenerator
                                className="formTitle"
                                label="'Show me the money' is phrase the world believes in"
                                type="title"
                              />
                              <ComponentGenerator
                                type="dropdown"
                                name="stageFunding"
                                label="What is the current funding stage your organization is into?*"
                                inputType="dropdown"
                                disabled={
                                  this.props.dashboardData.fundingStage
                                    ? true
                                    : false
                                }
                                placeholder={
                                  this.props.dashboardData.fundingStage
                                    ? this.props.dashboardData.FundingStage
                                    : "Select"
                                }
                                values={this.Form4.value.stageFunding}
                                dropdownData={
                                  this.props.dropdownData.FundingStage
                                }
                                clippyText={"Select current funding stage"}
                                voiceText="What is the current funding stage your organization is into?"
                              />
                              <ComponentGenerator
                                type="inputCurrency"
                                placeholder="000"
                                name="capitalRaised"
                                label="Amount of capital raised"
                                inputType="number"
                                maxlength={4}
                                currency={this.Form4.value.capitalCurrency}
                                onSelectHandleChange={this.onSelectHandleChange}
                                onRadioHandleChange={this.onRadioHandleChange}
                                currencyIn={this.Form4.value.currencyIn}
                                disabled={
                                  this.props.dashboardData.capital
                                    ? true
                                    : false
                                }
                                clippyText={"Only numbers (0-9) are allowed"}
                                voiceText="Amount of capital raised"
                              />
                              <ComponentGenerator
                                type="multiSelect"
                                placeholder="Key Investors"
                                name="keyInvestors"
                                label="Who are your key investors?"
                                inputType="dropdown"
                                identifier="KeyInvestor"
                                values={this.state.keyInvestors}
                                dropdownData={
                                  this.props.dropdownData.KeyInvestor
                                }
                                handleCreate={this.handleCreate}
                                createValue={this.createValue.bind(this)}
                                disabled={
                                  this.props.dashboardData.investors &&
                                  this.props.dashboardData.investors.length > 0
                                    ? true
                                    : false
                                }
                                clippyText={"Key investors in your business"}
                                voiceText="Who are your key investors?"
                              />
                            </ReactCSSTransitionGroup>
                            <Navigator
                              nextStep={this.nextStep}
                              prevStep={this.prevStep}
                              handleSubmit={this.handleSubmit}
                              invalid={invalid}
                            />
                          </form>
                        )}
                      />
                    </React.Fragment>
                  ),

                  8: (
                    <React.Fragment>
                      <FieldGroup
                        key="Form5"
                        control={this.Form5}
                        render={({ invalid, get }) => (
                          <form
                            onSubmit={this.handleSubmit}
                            className="mobileWidthfield textarewrapper"
                          >
                            <FieldGroup
                              key="Form4"
                              control={this.Form4}
                              render={({ invalid, get }) => (
                                <form
                                  onSubmit={this.handleSubmit}
                                  className=" blur "
                                >
                                  <ReactCSSTransitionGroup
                                    transitionName="page"
                                    transitionAppear={true}
                                    transitionAppearTimeout={5000}
                                    transitionEnter={true}
                                    transitionLeave={true}
                                  >
                                    <ComponentGenerator
                                      type="multiSelect"
                                      placeholder="Key Investors"
                                      name="keyInvestors"
                                      label="Who are your key investors?"
                                      inputType="dropdown"
                                      identifier="KeyInvestor"
                                      values={this.state.keyInvestors}
                                      dropdownData={
                                        this.props.dropdownData.KeyInvestor
                                      }
                                      handleCreate={this.handleCreate}
                                      createValue={this.createValue.bind(this)}
                                      disabled={
                                        this.props.dashboardData.investors &&
                                        this.props.dashboardData.investors
                                          .length > 0
                                          ? true
                                          : false
                                      }
                                      voiceText="Who are your key investors?"
                                    />
                                  </ReactCSSTransitionGroup>
                                </form>
                              )}
                            />
                            <ReactCSSTransitionGroup
                              transitionName="page"
                              transitionAppear={true}
                              transitionAppearTimeout={5000}
                              transitionEnter={true}
                              transitionLeave={true}
                            >
                              <ComponentGenerator
                                className="formTitle"
                                label=" I would like to know few  details about the best partnership that you have built"
                                type="title"
                              />
                              <div className="fieldClass partnerTab">
                                {this.state.partners.map(value => {
                                  if (
                                    this.state.partnerName !== value.partnerName
                                  ) {
                                    return (
                                      <Button
                                        key={value.partnerName}
                                        onClick={this.fillForm.bind(
                                          this,
                                          value
                                        )}
                                      >
                                        {value.partnerName}
                                        <span className="icon-arrowhead-pointing-to-the-right icon"></span>
                                      </Button>
                                    );
                                  } else {
                                    return (
                                      <Button
                                        key={value.partnerName}
                                        onClick={this.clearForm.bind(this)}
                                      >
                                        {value.partnerName}
                                        <span className="icon-sort-down icon"></span>
                                      </Button>
                                    );
                                  }
                                })}
                              </div>
                              <ComponentGenerator
                                type="input"
                                name="partnerName"
                                label="Name of the organization your partner works for*"
                                inputType="text"
                                disabled={false}
                                placeholder="Partner Name"
                                voiceText="Name of the organization your partner works for"
                                clippyText={
                                  "Only alphanumeric values (a-z, A-Z, 0-9) are allowed"
                                }
                              />
                              <ComponentGenerator
                                type="input"
                                name="location"
                                label="Your partner's organization is headquartered at*"
                                inputType="text"
                                disabled={false}
                                placeholder="Location"
                                voiceText="Your partner's organization is headquartered at"
                                clippyText={"Select your partner's headquarter"}
                              />
                              <ComponentGenerator
                                type="input"
                                placeholder="Website URL"
                                name="websiteURL2"
                                label="Can you provide the website URL of your partner's organization?*"
                                inputType="text"
                                disabled={false}
                                maxlength={100}
                                voiceText="Can you provide the website URL of your partner's organization?"
                                clippyText={"Please enter valid URL"}
                              />
                              <div className="morebtn">
                                <Button
                                  onClick={this.addPatner}
                                  disabled={
                                    this.state.partnerfill === true
                                      ? true
                                      : invalid
                                  }
                                >
                                  <span className="icon-addition-sign icon"></span>{" "}
                                  {this.state.partners.length > 0 ||
                                  (this.props.dashboardData.partners &&
                                    this.props.dashboardData.partners.length >
                                      0)
                                    ? "Add more partner"
                                    : "Add partner"}
                                </Button>
                              </div>
                            </ReactCSSTransitionGroup>
                            <Navigator
                              nextStep={
                                (this.state.partnerName &&
                                  this.state.location &&
                                  this.state.websiteURL2) !== ""
                                  ? this.addClientOnNext
                                  : this.nextStep
                              }
                              prevStep={this.prevStep}
                              handleSubmit={this.handleSubmit}
                              invalid={
                                this.state.partners.length > 0 ||
                                !invalid ||
                                (this.props.dashboardData.partners &&
                                  this.props.dashboardData.partners.length > 0)
                                  ? false
                                  : true
                              }
                            />
                          </form>
                        )}
                      />
                    </React.Fragment>
                  ),
                  9: (
                    <React.Fragment>
                      <ReactCSSTransitionGroup
                        transitionName="page"
                        transitionAppear={true}
                        transitionAppearTimeout={5000}
                        transitionEnter={true}
                        transitionLeave={true}
                      >
                        <div className="fieldClassCenter">
                          <ComponentGenerator
                            label="Who doesn't like choices- You've a choice of skipping this section"
                            type="title"
                          />
                        </div>
                      </ReactCSSTransitionGroup>
                      <Navigator
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleSubmit={this.handleSubmit}
                        // invalid={invalid}
                      />
                    </React.Fragment>
                  ),
                  10: (
                    <React.Fragment>
                      <FieldGroup
                        key="Form6"
                        control={this.Form6}
                        render={({ invalid, get }) => (
                          <form
                            onSubmit={this.handleSubmit}
                            className="textarewrapper"
                          >
                            <FieldGroup
                              key="Form5"
                              control={this.Form5}
                              render={({ invalid, get }) => (
                                <form
                                  onSubmit={this.handleSubmit}
                                  className="mobileWidthfield blur"
                                >
                                  <ReactCSSTransitionGroup
                                    transitionName="page"
                                    transitionAppear={true}
                                    transitionAppearTimeout={5000}
                                    transitionEnter={true}
                                    transitionLeave={true}
                                  >
                                    <ComponentGenerator
                                      type="input"
                                      placeholder="Type website URL"
                                      name="websiteURL2"
                                      label="Can you provide the website URL of your partner's organization?*"
                                      inputType="text"
                                      disabled={false}
                                      maxlength={100}
                                      voiceText="Can you provide the website URL of your partner's organization?"
                                    />
                                    <div className="morebtn">
                                      <Button
                                        onClick={this.addPatner}
                                        disabled={
                                          this.state.partnerfill === true
                                            ? true
                                            : invalid
                                        }
                                      >
                                        <span className="icon-addition-sign icon"></span>{" "}
                                        {this.state.partners.length > 0 ||
                                        (this.props.dashboardData.partners &&
                                          this.props.dashboardData.partners
                                            .length > 0)
                                          ? "Add more partner"
                                          : "Add partner"}
                                      </Button>
                                    </div>
                                  </ReactCSSTransitionGroup>
                                </form>
                              )}
                            />
                            <ReactCSSTransitionGroup
                              transitionName="page"
                              transitionAppear={true}
                              transitionAppearTimeout={5000}
                              transitionEnter={true}
                              transitionLeave={true}
                            >
                              <ComponentGenerator
                                className="formTitle"
                                label="We won't judge your company by the photos, likes or even posts - We don't promise though! ;)"
                                type="title"
                                clippyText={
                                  "This is optional, you can skip this"
                                }
                              />
                              <div className="socialLink">
                                <span className="icon-icons8-linkedin-2 icon"></span>
                                <ComponentGenerator
                                  type="input"
                                  name="linkedIn"
                                  inputType="text"
                                  disabled={
                                    this.props.dashboardData.socialLinks
                                      ? this.props.dashboardData.socialLinks
                                          .linkdin
                                        ? true
                                        : false
                                      : ""
                                  }
                                  placeholder="LinkedIn/Link"
                                  voiceText="LinkedIn Link"
                                />
                              </div>
                              <div className="socialLink">
                                <span className="icon-icons8-facebook-f icon"></span>
                                <ComponentGenerator
                                  type="input"
                                  placeholder="Facebook/Link"
                                  name="facebook"
                                  inputType="text"
                                  disabled={
                                    this.props.dashboardData.socialLinks
                                      ? this.props.dashboardData.socialLinks
                                          .facebook
                                        ? true
                                        : false
                                      : ""
                                  }
                                  maxlength={100}
                                  voiceText="Facebook Link"
                                />
                              </div>
                              <div className="socialLink">
                                <span className="icon-icons8-twitter icon"></span>
                                <ComponentGenerator
                                  type="input"
                                  placeholder="Twitter/Link"
                                  name="twitter"
                                  inputType="text"
                                  disabled={
                                    this.props.dashboardData.socialLinks
                                      ? this.props.dashboardData.socialLinks
                                          .twitter
                                        ? true
                                        : false
                                      : ""
                                  }
                                  maxlength={100}
                                  voiceText="Twitter Link"
                                />
                              </div>
                              <div className="socialLink">
                                <span className="icon-crunchbase icon"></span>
                                <ComponentGenerator
                                  type="input"
                                  name="crunchBase"
                                  inputType="text"
                                  disabled={
                                    this.props.dashboardData.socialLinks
                                      ? this.props.dashboardData.socialLinks
                                          .crunchBase
                                        ? true
                                        : false
                                      : ""
                                  }
                                  placeholder="CrunchBase/Link "
                                  voiceText="CrunchBase Link"
                                />
                              </div>
                              <div className="socialLink">
                                <span class="icon-othericon2 icon"></span>
                                <ComponentGenerator
                                  type="input"
                                  name="otherLink"
                                  inputType="text"
                                  disabled={
                                    this.props.dashboardData.other
                                      ? this.props.dashboardData.other
                                          .crunchBase
                                        ? true
                                        : false
                                      : ""
                                  }
                                  placeholder="Others "
                                  voiceText="Others"
                                />
                              </div>
                              <div className="skip">
                                <Button
                                  label="Skip"
                                  type="title"
                                  onClick={this.nextStep.bind(this)}
                                >
                                  Skip
                                </Button>
                              </div>
                            </ReactCSSTransitionGroup>
                            <Navigator
                              nextStep={this.nextStep}
                              prevStep={this.prevStep}
                              handleSubmit={this.handleSubmit}
                              invalid={invalid}
                            />
                          </form>
                        )}
                      />
                    </React.Fragment>
                  ),
                  11: (
                    <React.Fragment>
                      <ReactCSSTransitionGroup
                        transitionName="page"
                        transitionAppear={true}
                        transitionAppearTimeout={5000}
                        transitionEnter={true}
                        transitionLeave={true}
                      >
                        <ComponentGenerator
                          className="formTitle"
                          label={
                            "I am so intrigued " +
                            name +
                            ". Can you please tell me something more about your company?"
                          }
                          type="title"
                          clippyText={
                            "company logo only jpg,png allowed. other only jpg,png,and pdf allowed."
                          }
                        />
                        <div className="uploadContainer">
                          <div className="uploadLink">
                            <div className="uploadLocation">
                              <ComponentGenerator
                                type="file"
                                placeholder="Upload"
                                name="companyLogo"
                                values={"Company logo"}
                                prefill={
                                  this.state.companyLogo
                                    ? this.state.companyLogo
                                    : ""
                                }
                                inputType="text"
                                disabled={false}
                                maxlength={100}
                                mandatory={true}
                                deleteUploadFile={this.deleteUploadFile}
                                accepted={".png,.jpg"}
                                onFileChange={this.onFileChange.bind(this)}
                              />

                              {this.isUploading("company-logo")}
                            </div>
                            <span className="icon-upload"></span>
                          </div>
                          <div className="uploadTitle">
                            <div className="fieldClass">
                              <h2>Select and Upload</h2>
                            </div>
                          </div>
                          <div id="uploadLink1" className="uploadLink none">
                            <div className="uploadLocation">
                              <ComponentGenerator
                                type="file"
                                placeholder="Upload"
                                name="deckfile"
                                values={"Capability Deck"}
                                file={this.state.deckfile}
                                prefill={
                                  this.state.deckfile ? this.state.deckfile : ""
                                }
                                inputType="text"
                                mandatory={false}
                                disabled={false}
                                maxlength={100}
                                uploadLink="uploadLink1"
                                deleteUploadFile={this.deleteUploadFile}
                                onFileChange={this.onFileChange.bind(this)}
                                voiceText="Capability Deck"
                                onFocus={true}
                              ></ComponentGenerator>

                              {this.isUploading("capability-deck")}
                            </div>
                            <span className="icon-upload"></span>
                          </div>
                          <div id="uploadLink2" className="uploadLink none">
                            <div className="uploadLocation">
                              <ComponentGenerator
                                type="file"
                                placeholder="Upload"
                                name="company1"
                                values="Company Financials"
                                inputType="text"
                                disabled={false}
                                mandatory={false}
                                uploadLink="uploadLink2"
                                deleteUploadFile={this.deleteUploadFile}
                                maxlength={100}
                                onFileChange={this.onFileChange.bind(this)}
                              />

                              {this.isUploading("company-logo")}
                            </div>
                            <span className="icon-upload"></span>
                          </div>
                          {/* <div id="uploadLink3" className="uploadLink none">
                            <div className="uploadLocation">
                              <ComponentGenerator
                                type="file"
                                placeholder="Upload"
                                name="company2"
                                values="Company 2"
                                inputType="text"
                                disabled={false}
                                mandatory={false}
                                maxlength={100}
                                uploadLink="uploadLink3"
                                deleteUploadFile={this.deleteUploadFile}
                                onFileChange={this.onFileChange.bind(this)}
                              />

                              {this.isUploading("company-logo")}
                            </div>
                            <span className="icon-upload"></span>
                          </div>
                          <div id="uploadLink4" className="uploadLink none">
                            <div className="uploadLocation">
                              <ComponentGenerator
                                type="file"
                                placeholder="Upload"
                                name="Company 3"
                                values="Company 3"
                                inputType="text"
                                disabled={false}
                                maxlength={100}
                                uploadLink="uploadLink4"
                                mandatory={false}
                                deleteUploadFile={this.deleteUploadFile}
                                onFileChange={this.onFileChange.bind(this)}
                              />

                              {this.isUploading("company-logo")}
                            </div>
                            <span className="icon-upload"></span>
                          </div> */}
                          <div id="uploadLink5" className="uploadLink none">
                            <div className="uploadLocation">
                              <ComponentGenerator
                                type="file"
                                placeholder="Upload"
                                name="otherUpload"
                                values="Other"
                                inputType="text"
                                disabled={false}
                                maxlength={100}
                                uploadLink="uploadLink5"
                                mandatory={false}
                                deleteUploadFile={this.deleteUploadFile}
                                onFileChange={this.onFileChange.bind(this)}
                              />
                              <div id="fileName"></div>
                              {this.isUploading("company-logo")}
                            </div>
                            <span className="icon-upload"></span>
                          </div>
                          <div className="uploadLinkdropdown">
                            <div className="uploadLocation">
                              <select
                                id="dropDownId"
                                onChange={this.displayUploadTag}
                                defaultValue={this.value}
                              >
                                <option selected="selected">Select</option>
                                <option value="1">Capability Deck</option>
                                <option value="2">Company Financials</option>
                                {/* <option value="3">Company 2</option>
                                <option value="4">Company 3</option> */}
                                <option value="5">Other</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </ReactCSSTransitionGroup>
                      <Navigator
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        invalid={
                          this.props.dashboardData.documents &&
                          this.props.dashboardData.documents.mandatory &&
                          (this.props.dashboardData.documents.mandatory.length >
                            0 ||
                            this.props.dashboardData.documents.mandatory[0])
                            ? false
                            : true
                        }
                        handleSubmit={this.handleSubmit}
                      />
                    </React.Fragment>
                  ),
                  12: (
                    <Redirect
                      to={{
                        pathname: "/solution-form"
                        // state: this.state
                      }}
                    />
                  )
                }[step]
              }
            </div>
          ) : (
            ""
          )}
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    company: state.registerSpocReducer.data.result,
    companyData: state.registerSpocReducer.data,
    dropdownData: state.masterDataReducer.data,
    isSubmitting: state.commonReducer.spinner.isSubmitting,
    uploadingContents: state.commonReducer.uploadingContents,
    dashboardData: state.dashboardDataReducer.data,
    isVoiceEnabledReducer: state.commonReducer.isVoiceEnabled,
    commonReducer: state.commonReducer
  };
};

export default connect(mapStateToProps)(CompanyFormDetail);
