import React, { Component } from "react";
import ComponentGenerator from "../common/formDetails/componentGenerator/ComponentGenerator";
import Navigator from "../common/formDetails/navigator/Navigator";
import Background from "../common/background/background";
import Header from "../common/header/Header";
import { Redirect } from "react-router-dom";
import { FormBuilder, FieldGroup, Validators } from "react-reactive-form";
import { connect } from "react-redux";
import store from "../../store/store";
import "./PersonalDetailForm.scss";
import DisplaySuccessMessage from "../common/formDetails/displaySuccessMessage/DisplaySuccessMessage";
import {
  registerStartupData,
  verifyOtp
} from "../../store/actions/registerSpocAction";
import Validator from "../common/leftSideDrawer/Validator";
import {
  onTextToVoice,
  onSubmitError,
  onVoiceEnabled
} from "../../store/actions/commonActions";
import {
  onPersonalFormPrevStepChange,
  onPersonalFormNextStepChange
} from "../../store/actions/stepAction";
import TextToVoice from "../common/textToVoice/TextToVoice";
import ButtonToggle from "../common/buttonToggle/ButtonToggle";
import { resendOtp } from "../../store/actions/loginAction";
import { Button } from "@material-ui/core";
import stepReducer from "../../store/reducers/stepReducer";

const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));
const isEmpty = (control: AbstractControl) => {
  return sleep(1000).then(() => {
    if (control.value === "") {
      throw { notExist: true };
    } else {
      return null;
    }
  });
};

class PersonalDetailForm extends Component {
  state = {
    step: 1,
    fullName: "",
    designation: "",
    name: "",
    mobile: "",
    alternativeMobileNumber: "",
    alternativeEmail: "",
    officialEmail: "",
    verificationCode: "",
    isOfficialEmailFormValid: false,
    consent: false,
    isopen: false,
    isTnCAccepted: false,
    isVoiceEnabled: JSON.parse(localStorage.getItem("sound"))
  };

  voiceText = [
    "Hey! Can you introduce yourself?",
    "What is your role in the organization?",
    "What is the name of the organization you are working for?",
    "I'd love to get connected. Could you please provide your phone number?",
    "Do you have any  alternative mobile number?",
    "Do you have any alternative email ID?",
    "I need to drop you a electronic message. Could you provide your official email ID?",
    "A verification code is probably sitting in your inbox now - please enter the code",
    "Terms & Condition",
    "Wooho! You're now registered. Login anytime you wish.",
    ""
  ];

  componentDidMount() {
    var x = document.getElementsByClassName("MuiInputBase-input");
    //store.dispatch(onTextToVoice(this.formText[0]));
    store.dispatch(
      onSubmitError(
        "Error!",
        "The fields marked with asterisk (*) are mandatory"
      )
    );
    store.dispatch(onVoiceEnabled(true));
  }

  showPdf = openPdf => {
    this.setState({ isopen: openPdf });
  };

  fullNameForm = FormBuilder.group({
    fullName: [
      "",
      [
        Validators.required,
        Validators.maxLength(50),
        Validators.minLength(2),
        Validators.pattern("^[a-zA-Z0-9][a-zA-Z0-9 ]+$")
      ],
      isEmpty
    ]
  });

  designationForm = FormBuilder.group({
    designation: [
      "",
      [
        Validators.required,
        Validators.maxLength(50),
        Validators.minLength(2),
        Validators.pattern("^[a-zA-Z0-9][a-zA-Z0-9 ]+$")
      ],
      isEmpty
    ]
  });

  companyNameForm = FormBuilder.group({
    name: [
      "",
      [
        Validators.required,
        Validators.maxLength(100),
        Validators.minLength(2),
        Validators.pattern("^[a-zA-Z0-9][a-zA-Z0-9 ]+$")
      ],
      isEmpty
    ]
  });

  mobileNumberForm = FormBuilder.group({
    mobile: ["", [Validators.required], isEmpty]
  });

  alternativeMobileNumberForm = FormBuilder.group({
    alternativeMobileNumber: ["", Validators.required]
  });

  officialEmailForm = FormBuilder.group({
    officialEmail: [
      "",
      [
        Validators.required,
        Validators.email,
        Validators.pattern(
          "^[a-z0-9]+(.[_a-z0-9]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,15})$"
        )
      ],
      isEmpty
    ]
  });

  alternativeEmailForm = FormBuilder.group({
    alternativeEmail: [
      "",
      [
        Validators.email,
        Validators.pattern(
          "^[a-z0-9]+(.[_a-z0-9]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,15})$"
        )
      ]
    ]
  });

  verificationCodeForm = FormBuilder.group({
    verificationCode: ["", [Validators.required], isEmpty]
  });

  consentForm = FormBuilder.group({
    consent: [false, Validators.requiredTrue]
  });

  subscribeFormChangeEvent(step) {
    switch (step) {
      case 1:
        this.fullNameForm.get("fullName").valueChanges.subscribe(value => {
          if (value.match("^[a-zA-Z0-9 ]+$")) {
            this.setState({
              fullName: value
            });
          } else if (value === "") {
            store.dispatch(
              onSubmitError(
                "Error!",
                "The fields marked with asterisk (*) are mandatory"
              )
            );
          } else {
            store.dispatch(
              onSubmitError("Error!", "Special Characters not Allowed")
            );
          }
        });
        break;
      case 2:
        this.designationForm
          .get("designation")
          .valueChanges.subscribe(value => {
            if (value.match("^[a-zA-Z0-9 ]+$")) {
              this.setState({
                designation: value
              });
            } else if (value === "") {
              store.dispatch(
                onSubmitError(
                  "Error!",
                  "The fields marked with asterisk (*) are mandatory"
                )
              );
            } else {
              store.dispatch(
                onSubmitError("Error!", "Special Characters not Allowed")
              );
            }
          });
        break;
      case 3:
        this.companyNameForm.get("name").valueChanges.subscribe(value => {
          if (value.match("^[a-zA-Z0-9 ]+$")) {
            this.setState({
              name: value
            });
          } else if (value === "") {
            store.dispatch(
              onSubmitError(
                "Error!",
                "The fields marked with asterisk (*) are mandatory"
              )
            );
          } else {
            store.dispatch(
              onSubmitError("Error!", "Special Characters not Allowed")
            );
          }
        });
        break;
      case 4:
        this.mobileNumberForm.get("mobile").valueChanges.subscribe(value => {
          this.setState({
            mobile: value
          });
        });
        break;
      case 5:
        this.alternativeMobileNumberForm
          .get("alternativeMobileNumber")
          .valueChanges.subscribe(value => {
            this.setState({
              alternativeMobileNumber: value
            });
          });
        break;
      case 7:
        this.alternativeEmailForm
          .get("alternativeEmail")
          .valueChanges.subscribe(value => {
            if (
              value.match(
                "^[^s][a-z0-9]+(.[_a-z0-9]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,15})$"
              )
            ) {
              this.setState({
                alternativeEmail: value
              });
            } else if (value === "") {
              store.dispatch(
                onSubmitError(
                  "Error!",
                  "The fields marked with asterisk (*) are mandatory"
                )
              );
            } else {
              store.dispatch(
                onSubmitError("Error!", "This is optional and can be skipped")
              );
            }
          });
        break;
      case 6:
        this.officialEmailForm
          .get("officialEmail")
          .valueChanges.subscribe(value => {
            this.setState({
              officialEmail: value,
              isOfficialEmailFormValid:
                this.officialEmailForm.get("officialEmail").status !== "INVALID"
            });
          });
        break;
      case 8:
        this.verificationCodeForm
          .get("verificationCode")
          .valueChanges.subscribe(value => {
            this.setState({
              verificationCode: value
            });
          });
        break;
      default:
    }
  }

  componentWillUnmount() {
    switch (this.props.currentStep) {
      case 1:
        return this.fullNameForm.get("fullName").valueChanges.unsubscribe();
      case 2:
        return this.designationForm
          .get("designation")
          .valueChanges.unsubscribe();
      case 3:
        return this.companyNameForm.get("name").valueChanges.unsubscribe();
      case 4:
        return this.mobileNumberForm.get("mobile").valueChanges.unsubscribe();
      case 5:
        return this.alternativeMobileNumberForm
          .get("alternativeMobileNumber")
          .valueChanges.unsubscribe();
      case 7:
        return this.alternativeEmailForm
          .get("alternativeEmail")
          .valueChanges.unsubscribe();
      case 6:
        return this.officialEmailForm
          .get("officialEmail")
          .valueChanges.unsubscribe();
      case 8:
        return this.verificationCodeForm
          .get("verificationCode")
          .valueChanges.unsubscribe();
      default:
    }
  }

  handleSubmit = e => {
    this.nextStep();
  };

  sendOTP = event => {
    const {
      fullName,
      designation,
      mobile,
      alternativeMobileNumber,
      alternativeEmail,
      name,
      officialEmail,
      consent
    } = this.state;
    const data = {
      fullName,
      designation,
      mobile,
      alternativeMobileNumber,
      alternativeEmail,
      name,
      officialEmail,
      consent
    };
    store.dispatch(registerStartupData(data));
  };

  confirmOTP = event => {
    const { verificationCode } = this.state;

    store.dispatch(verifyOtp(verificationCode, false));
  };

  nextStep = () => {
    // const { step } = this.state;
    // this.setState({
    //   step: step + 1
    // });
    //store.dispatch(onTextToVoice(this.formText[this.props.currentStep]));
    store.dispatch(onPersonalFormNextStepChange());
  };

  prevStep = () => {
    // const { step } = this.state;
    // if (step > 1) {
    //   this.setState({
    //     step: step - 1
    //   });
    // } else {
    //   this.setState({
    //     step: 1
    //   });
    // }
    store.dispatch(onPersonalFormPrevStepChange());
  };

  onAcceptTnC = () => {
    this.setState({
      consent: !this.state.consent
    });
  };

  checkedSoundValue = checked => {
    this.setState({
      isVoiceEnabled: checked
    });
  };
  onKeyPress = event => {
    if (event.keyCode === 13) {
      this.nextStep();
    }
  };
  onKeyPressOTP = event => {
    if (event.keyCode === 13) {
      this.sendOTP();
    }
  };

  handleResendOtpClick = () => {
    // store.dispatch(onSubmitError("Error !", "OTP is sent again"));
    store.dispatch(resendOtp(this.state.officialEmail, "isRegister"));
  };

  render() {
    const step = this.props.currentStep;
    this.subscribeFormChangeEvent(step);
    const {
      fullName,
      designation,
      name,
      mobile,
      alternativeMobileNumber,
      verificationCode,
      alternativeEmail,
      officialEmail
    } = this.state;
    const values = {
      fullName,
      designation,
      name,
      mobile,
      alternativeMobileNumber,
      verificationCode,
      alternativeEmail,
      officialEmail
    };
    return (
      <React.Fragment>
        <Background />
        <div className="landingContainer">
          <Header />
          <Validator />
          <TextToVoice />

          <div className="articleContainer">
            <div
              className={
                this.props.isVoiceEnabledReducer ? "soundOn" : "soundOff"
              }
            >
              <ButtonToggle
                checkedSoundValue={this.checkedSoundValue.bind(this)}
                type="sound"
              />
            </div>
          </div>
          <div className="formContainer">
            {
              {
                1: (
                  <FieldGroup
                    key="fullNameForm"
                    control={this.fullNameForm}
                    render={({ get, invalid }) => (
                      <form onSubmit={this.handleSubmit.bind(this)}>
                        <ComponentGenerator
                          type="input"
                          placeholder="Full Name"
                          name="fullName"
                          label="Hey! Can you introduce yourself?*"
                          inputType="text"
                          maxlength={100}
                          voiceText={
                            invalid
                              ? this.voiceText[0]
                              : "Do you want to review the information. on the screen in case we captured it wrong?"
                          }
                          onFocus={true}
                          animateRequire={true}
                        />
                        <Navigator
                          nextStep={this.nextStep}
                          prevStep={this.prevStep}
                          invalid={invalid}
                          step={this.state.step}
                          handleSubmit={this.handleSubmit}
                          // onKeyPressEvent={this.onKeyPress}
                        />
                      </form>
                    )}
                  />
                ),
                2: (
                  <FieldGroup
                    key="designationForm"
                    control={this.designationForm}
                    render={({ get, invalid }) => (
                      <form
                        onSubmit={this.handleSubmit.bind(this)}
                        className="mobileWidthfield"
                      >
                        <ComponentGenerator
                          type="input"
                          placeholder="Current Designation"
                          name="designation"
                          label="What is your role in the organization?*"
                          values={values.designation}
                          inputType="text"
                          maxlength={100}
                          voiceText={
                            invalid
                              ? this.voiceText[1]
                              : "Seems like you are not sure. Do you want to make any corrections here? Go ahead and update."
                          }
                          clippyText={
                            "Only alphanumeric values (a-z, A-Z, 0-9) are allowed"
                          }
                          onFocus={true}
                          animateRequire={true}
                        />
                        <Navigator
                          nextStep={this.nextStep}
                          prevStep={this.prevStep}
                          invalid={invalid}
                          handleSubmit={this.handleSubmit}
                          onKeyPressEvent={this.onKeyPress}
                        />
                      </form>
                    )}
                  />
                ),
                3: (
                  <FieldGroup
                    key="companyNameForm"
                    control={this.companyNameForm}
                    render={({ get, invalid }) => (
                      <form onSubmit={this.handleSubmit}>
                        <ComponentGenerator
                          type="input"
                          placeholder="Company Name"
                          name="name"
                          label="What is the name of the organization you are working for?*"
                          values={values.name}
                          inputType="text"
                          maxlength={100}
                          voiceText={
                            invalid
                              ? this.voiceText[2]
                              : "Do you want to review the information. on the screen in case we captured it wrong?"
                          }
                          clippyText={"Give organization you working for"}
                          onFocus={true}
                          animateRequire={true}
                        />
                        <Navigator
                          nextStep={this.nextStep}
                          prevStep={this.prevStep}
                          invalid={invalid}
                          handleSubmit={this.handleSubmit}
                          onKeyPressEvent={this.onKeyPress}
                        />
                      </form>
                    )}
                  />
                ),
                4: (
                  <FieldGroup
                    key="mobileNumberForm"
                    control={this.mobileNumberForm}
                    render={({ get, invalid }) => (
                      <form onSubmit={this.handleSubmit}>
                        <ComponentGenerator
                          type="phone"
                          placeholder="00000 00000"
                          name="mobile"
                          label="I'd love to get connected. Could you please provide your phone number?*"
                          values={values.mobile}
                          inputType="number"
                          maxlength={10}
                          voiceText={
                            invalid
                              ? this.voiceText[3]
                              : "Seems like you are not sure. Do you want to make any corrections here? Go ahead and update."
                          }
                          onFocus={true}
                        />
                        <Navigator
                          nextStep={this.nextStep}
                          prevStep={this.prevStep}
                          invalid={invalid}
                          handleSubmit={this.handleSubmit}
                          animateRequire={true}
                          onKeyPressEvent={this.onKeyPress}
                        />
                      </form>
                    )}
                  />
                ),
                5: (
                  <FieldGroup
                    key="alternativeMobileNumberForm"
                    control={this.alternativeMobileNumberForm}
                    render={({ get, invalid }) => (
                      <form
                        onSubmit={this.handleSubmit}
                        className="mobileWidthfield"
                      >
                        <ComponentGenerator
                          type="phone"
                          placeholder="00000 00000"
                          name="alternativeMobileNumber"
                          label="Do you have any alternative mobile number?"
                          values={values.alternativeMobileNumber}
                          inputType="number"
                          maxlength={10}
                          voiceText={
                            invalid
                              ? "This is optional, you can skip this"
                              : "Do you want to review the information. on the screen in case we captured it wrong?"
                          }
                          clippy={true}
                          onFocus={true}
                          animateRequire={true}
                        />
                        <Navigator
                          nextStep={this.nextStep}
                          prevStep={this.prevStep}
                          handleSubmit={this.handleSubmit}
                          onKeyPressEvent={this.onKeyPress}
                        />
                      </form>
                    )}
                  />
                ),

                6: (
                  <div>
                    <FieldGroup
                      key="officialEmailForm"
                      control={this.officialEmailForm}
                      render={({ get, invalid }) => (
                        <form>
                          <div className="officailEmail">
                            <ComponentGenerator
                              type="input"
                              placeholder="johndoe@abccompany.com"
                              name="officialEmail"
                              label="I need to drop you an electronic message. Could you provide your official email ID?*"
                              values={values.officialEmail}
                              ref={"verificationCode"}
                              inputType="email"
                              maxlength={50}
                              voiceText={
                                invalid
                                  ? this.voiceText[6]
                                  : "Seems like you are not sure . Do you want to make any corrections here? Go ahead and update."
                              }
                              onFocus={true}
                              clippyText={"Please enter valid email"}
                              animateRequire={true}
                            />
                          </div>
                        </form>
                      )}
                    />
                    <Navigator
                      nextStep={this.nextStep}
                      disabled={this.state.isOfficialEmailFormValid}
                      prevStep={this.prevStep}
                      invalid={!this.state.isOfficialEmailFormValid}
                      name="send"
                      onKeyPressEvent={this.onKeyPressOTP}
                    />
                  </div>
                ),
                7: (
                  <FieldGroup
                    key="alternativeEmailForm"
                    control={this.alternativeEmailForm}
                    render={({ get, invalid }) => (
                      <form
                        onSubmit={this.handleSubmit}
                        className="mobileWidthfield"
                      >
                        <ComponentGenerator
                          type="input"
                          placeholder="Enter Email ID"
                          name="alternativeEmail"
                          label="Do you have any alternative email ID?"
                          values={values.alternativeEmail}
                          inputType="email"
                          maxlength={50}
                          voiceText={this.voiceText[5]}
                          onFocus={true}
                          animateRequire={true}
                          clippyText={"This is optional and can be skipped"}
                        />
                        <Navigator
                          nextStep={this.nextStep}
                          prevStep={this.prevStep}
                          invalid={invalid}
                          sendOTP={this.sendOTP}
                          handleSubmit={this.handleSubmit}
                          onKeyPressEvent={this.onKeyPress}
                        />
                      </form>
                    )}
                  />
                ),
                8: (
                  <div>
                    <FieldGroup
                      key="verificationCodeForm"
                      control={this.verificationCodeForm}
                      render={({ get, invalid }) => (
                        <form>
                          <ComponentGenerator
                            type="input"
                            placeholder="******"
                            name="verificationCode"
                            label="A verification code is probably sitting in your inbox now - please enter the code*"
                            values={values.verificationCode}
                            inputType="password"
                            maxlength={6}
                            voiceText={
                              invalid
                                ? this.voiceText[7]
                                : "Seems like you are not sure. Do you want to make any corrections here? Go ahead and update."
                            }
                            resend={this.handleResendOtpClick}
                            onFocus={true}
                            animateRequire={true}
                          />
                        </form>
                      )}
                    />
                    <Navigator
                      nextStep={this.nextStep}
                      prevStep={this.prevStep}
                      confirmOTP={this.confirmOTP}
                      invalid={false}
                      handleSubmit={this.handleSubmit}
                      name="verify"
                      onKeyPressEvent={this.onKeyPressOTP}
                    />
                  </div>
                ),

                9: (
                  <div>
                    <ComponentGenerator
                      type={this.state.isopen ? "pdf" : "agree"}
                      name="consent"
                      label="I agree to the Terms and Conditions"
                      values={this.state.consent}
                      showPdf={this.showPdf}
                      onAcceptTnC={this.onAcceptTnC.bind(this)}
                      onFocus={true}
                      clippyText={"Please agree to terms and condition"}
                      voiceText="Terms and Conditions"
                    />

                    <Navigator
                      nextStep={this.nextStep}
                      prevStep={this.prevStep}
                      handleSubmit={this.handleSubmit}
                      invalid={this.state.consent === false ? true : false}
                      onKeyPressEvent={this.onKeyPress}
                    />
                  </div>
                ),
                10: (
                  <React.Fragment>
                    <DisplaySuccessMessage
                      label="Wooho! You're now registered. Login anytime you wish."
                      data={this.state}
                      nextStep={this.nextStep}
                      prevStep={this.prevStep}
                    />
                    <div className="stepButtons">
                      <Button type="button" onClick={this.prevStep}>
                        <span className="icon-prev"></span>
                      </Button>
                    </div>
                  </React.Fragment>
                ),
                11: (
                  <Redirect
                    to={{
                      pathname: "/company-form"
                    }}
                  />
                )
              }[step]
            }
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    spocData: state.registerSpocReducer.data,
    currentStep: state.stepReducer.personalFormCurrentStep,
    isVoiceEnabledReducer: state.commonReducer.isVoiceEnabled
  };
};

export default connect(mapStateToProps)(PersonalDetailForm);
