import React, { Component } from "react";
import Background from "../../common/background/background";
import Header from "../../common/header/Header";
import { FormBuilder, FieldGroup, Validators } from "react-reactive-form";
import Navigator from "../../common/formDetails/navigator/Navigator";
import ComponentGenerator from "../../common/formDetails/componentGenerator/ComponentGenerator";
import { Redirect } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import { solutionDetails } from "../../../store/actions/solutionAction";
import { deleteSolutionDocument } from "../../../store/actions/deleteFileAction";
import { submitForm } from "../../../store/actions/submitFormAction";
import store from "../../../store/store";
import {
  getMasterData,
  postMasterData
} from "../../../store/actions/masterDataAction";
import { uploadDocument } from "../../../store/actions/uploadDocumentAction";
import { getDashboardData } from "../../../store/actions/dashboardDataAction";
import LinearDeterminate from "../../common/progressbar/Progressbar";
import ButtonToggle from "../../common/buttonToggle/ButtonToggle";
import TextToVoice from "../../common/textToVoice/TextToVoice";
import { onSetStep } from "../../../store/actions/commonActions";
import { parse } from "react-filepond";
import { logout } from "../../../store/actions/loginAction";
import "./SolutionForm.scss";
import Validator from "../../common/leftSideDrawer/Validator";

const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));
const isEmpty = (control: AbstractControl) => {
  return sleep(1000).then(() => {
    if (control.value === "") {
      throw { notExist: true };
    } else {
      return null;
    }
  });
};

const isRequired = (control: AbstractControl) => {
  return sleep(1000).then(() => {
    if (control.value === "" || control.value === "Select") {
      throw { notExist: true };
    } else {
      return null;
    }
  });
};

var ReactCSSTransitionGroup = require("react-addons-css-transition-group");
export class SolutionForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 1,
      solutionName: "",
      type: "",
      domainserved: "",
      solutionRelavent: "",
      technologyStack: "",
      briefDecription: "",
      challengeSolving: "",
      uspsSolution: "",
      benefite: "",
      clientName: "",
      country: "",
      websiteURL: "",
      sofarImplementation: "",
      indiaImplementation: "",
      timelineImplementation: "",
      commercialModel: "",
      futureRoadmap: "",
      providerName: "",
      references: "",
      count: "",
      architecturefile: "",
      soutionArchitecture: "Soution Architecture",
      clients: [],
      providers: [],
      clientfill: false,
      isSubmitted: true,
      solutionsdata: []
    };
  }

  componentDidMount() {
    let index = 0;
    if (
      this.props.dashboardData.spoc[0].solutions &&
      this.props.dashboardData.spoc[0].solutions.length > 0
    ) {
      index = this.props.dashboardData.spoc[0].solutions.length - 1;
    }

    if (
      this.props.dashboardData &&
      this.props.dashboardData.spoc[0].solutions[index] &&
      this.props.dashboardData.spoc[0].solutions[index].solutionPage &&
      this.props.dashboardData.spoc[0].solutions[index].isCompleted === false
    ) {
      this.setState({
        step:
          parseInt(
            this.props.dashboardData.spoc[0].solutions[index].solutionPage
          ) + 1
      });
      localStorage.setItem(
        "solutionId",
        this.props.dashboardData.spoc[0].solutions[index]._id
      );
    }

    store.dispatch(
      getMasterData(
        "SolutionType,SolutionDomain,SolutionCloudProvider,SolutionRelevance,SolutionTechnologyStack,Country"
      )
    );

    if (this.props.dashboardData.spoc[0].solutions && index >= 0) {
      let data = {};
      let clients = [];
      if (
        this.props.dashboardData.spoc[0].solutions[index] &&
        this.props.dashboardData.spoc[0].solutions[index].isCompleted ===
          false &&
        (this.props.dashboardData.spoc[0].solutions[index].solutionPage !== 0 ||
          this.props.dashboardData.spoc[0].solutions[index].solutionPage === 14)
      ) {
        this.props.dashboardData.spoc[0].solutions[index].clients.map(
          content => {
            data = {
              clientName: content.name,
              country: content.country,
              websiteURL: content.url
            };
            clients.push(data);
          }
        );
        this.setState({ clients: clients });
      }
    }

    let x = this.props.dashboardData ? this.setDefaultValue() : "";
  }
  componentWillReceiveProps() {
    if (this.props.commonReducer.step && this.props.commonReducer.step > 0) {
      this.setState({
        step: this.props.commonReducer.step
      });
      let index = 0;
      if (
        this.props.dashboardData.spoc[0].solutions &&
        this.props.dashboardData.spoc[0].solutions.length > 0
      ) {
        index = this.props.dashboardData.spoc[0].solutions.length - 1;
      }
      localStorage.setItem(
        "solutionId",
        this.props.dashboardData.spoc[0].solutions[index]._id
      );
      store.dispatch(onSetStep(0));
    }
    if (
      this.props.commonReducer.voiceToText &&
      this.props.commonReducer.name === "briefDecription"
    ) {
      this.Form3.get("briefDecription").patchValue(
        this.props.commonReducer.voiceToText
      );
    }
    if (
      this.props.commonReducer.voiceToText &&
      this.props.commonReducer.name === "challengeSolving"
    ) {
      this.Form3.get("challengeSolving").patchValue(
        this.props.commonReducer.voiceToText
      );
    }
    if (
      this.props.commonReducer.voiceToText &&
      this.props.commonReducer.name === "uspsSolution"
    ) {
      this.Form4.get("uspsSolution").patchValue(
        this.props.commonReducer.voiceToText
      );
    }
    if (
      this.props.commonReducer.voiceToText &&
      this.props.commonReducer.name === "benefite"
    ) {
      this.Form4.get("benefite").patchValue(
        this.props.commonReducer.voiceToText
      );
    }
    if (
      this.props.commonReducer.voiceToText &&
      this.props.commonReducer.name === "commercialModel"
    ) {
      this.Form7.get("commercialModel").patchValue(
        this.props.commonReducer.voiceToText
      );
    }
    if (
      this.props.commonReducer.voiceToText &&
      this.props.commonReducer.name === "futureRoadmap"
    ) {
      this.Form7.get("futureRoadmap").patchValue(
        this.props.commonReducer.voiceToText
      );
    }
  }
  onKeyPress = event => {
    if (event.keyCode === 13) {
      this.nextStep();
    }
  };
  getOption = (index, optionValue) => {
    let obj;
    let options = [];
    switch (optionValue) {
      case "type":
        if (this.props.dashboardData.spoc[0].solutions[index].type) {
          this.props.dashboardData.spoc[0].solutions[index].type.map(data => {
            obj = {};
            obj.value = data;
            obj.label = data;
            options.push(obj);
          });
        }
        break;
      case "domain":
        if (this.props.dashboardData.spoc[0].solutions[index].domain) {
          this.props.dashboardData.spoc[0].solutions[index].domain.map(data => {
            obj = {};
            obj.value = data;
            obj.label = data;
            options.push(obj);
          });
        }
        break;
      case "relevance":
        if (this.props.dashboardData.spoc[0].solutions[index].relevance) {
          this.props.dashboardData.spoc[0].solutions[index].relevance.map(
            data => {
              obj = {};
              obj.value = data;
              obj.label = data;
              options.push(obj);
            }
          );
        }
        break;
      case "stack":
        if (this.props.dashboardData.spoc[0].solutions[index].stack) {
          this.props.dashboardData.spoc[0].solutions[index].stack.map(data => {
            obj = {};
            obj.value = data;
            obj.label = data;
            options.push(obj);
          });
        }
      default:
        break;
      case "providerName":
        if (this.props.dashboardData.spoc[0].solutions[index].provider.name) {
          this.props.dashboardData.spoc[0].solutions[index].provider.name.map(
            data => {
              obj = {};
              obj.value = data;
              obj.label = data;
              options.push(obj);
            }
          );
        }
    }

    return options;
  };

  setDefaultValue = () => {
    let index = 0;
    if (
      this.props.dashboardData.spoc[0].solutions &&
      this.props.dashboardData.spoc[0].solutions.length >= 0
    ) {
      index = this.props.dashboardData.spoc[0].solutions.length - 1;
    }
    if (this.props.dashboardData.spoc[0].solutions && index >= 0) {
      if (
        this.props.dashboardData.spoc[0].solutions[index].isCompleted === false
      ) {
        this.Form1.get("solutionName").setValue(
          this.props.dashboardData.spoc[0].solutions[index].name
            ? this.props.dashboardData.spoc[0].solutions[index].name
            : ""
        );
        this.Form1.get("type").setValue(
          this.props.dashboardData.spoc[0].solutions[index].type
            ? this.getOption(index, "type")
            : ""
        );
        this.Form1.get("domainserved").setValue(
          this.props.dashboardData.spoc[0].solutions[index].domain
            ? this.getOption(index, "domain")
            : ""
        );
        this.Form2.get("solutionRelavent").setValue(
          this.props.dashboardData.spoc[0].solutions[index].relevance
            ? this.getOption(index, "relevance")
            : ""
        );

        this.Form2.get("technologyStack").setValue(
          this.props.dashboardData.spoc[0].solutions[index].stack
            ? this.getOption(index, "stack")
            : ""
        );
        this.Form3.get("briefDecription").setValue(
          this.props.dashboardData.spoc[0].solutions[index].description
            ? this.props.dashboardData.spoc[0].solutions[index].description
            : ""
        );
        this.Form3.get("challengeSolving").setValue(
          this.props.dashboardData.spoc[0].solutions[index].challenges
            ? this.props.dashboardData.spoc[0].solutions[index].challenges
            : ""
        );
        this.Form4.get("uspsSolution").setValue(
          this.props.dashboardData.spoc[0].solutions[index].usp
            ? this.props.dashboardData.spoc[0].solutions[index].usp
            : ""
        );
        this.Form4.get("benefite").setValue(
          this.props.dashboardData.spoc[0].solutions[index].impact
            ? this.props.dashboardData.spoc[0].solutions[index].impact
            : ""
        );

        this.Form6.get("sofarImplementation").setValue(
          this.props.dashboardData.spoc[0].solutions[index].totalImplementations
            ? this.props.dashboardData.spoc[0].solutions[index]
                .totalImplementations
            : ""
        );
        this.Form6.get("indiaImplementation").setValue(
          this.props.dashboardData.spoc[0].solutions[index].implementations
            ? this.props.dashboardData.spoc[0].solutions[index].implementations
            : ""
        );
        this.Form6.get("timelineImplementation").setValue(
          this.props.dashboardData.spoc[0].solutions[index]
            .implementationsTimeline
            ? this.props.dashboardData.spoc[0].solutions[index]
                .implementationsTimeline
            : ""
        );
        this.Form7.get("commercialModel").setValue(
          this.props.dashboardData.spoc[0].solutions[index].commercialModels
            ? this.props.dashboardData.spoc[0].solutions[index].commercialModels
            : ""
        );
        this.Form7.get("futureRoadmap").setValue(
          this.props.dashboardData.spoc[0].solutions[index].futureRoadmap
            ? this.props.dashboardData.spoc[0].solutions[index].futureRoadmap
            : ""
        );
        this.Form8.get("providerName").setValue(
          this.props.dashboardData.spoc[0].solutions[index].provider &&
            this.props.dashboardData.spoc[0].solutions[index].provider.name
            ? this.getOption(index, "providerName")
            : ""
        );
        this.Form8.get("references").setValue(
          this.props.dashboardData.spoc[0].solutions[index].provider &&
            this.props.dashboardData.spoc[0].solutions[index].provider.reference
            ? this.props.dashboardData.spoc[0].solutions[index].provider
                .reference
            : ""
        );
        this.Form8.get("count").setValue(
          this.props.dashboardData.spoc[0].solutions[index].provider &&
            this.props.dashboardData.spoc[0].solutions[index].provider.count
            ? this.props.dashboardData.spoc[0].solutions[index].provider.count
            : ""
        );
      }
    }
  };

  subscribeFormChangeEvent(step) {
    switch (step) {
      case 2:
        this.Form1.get("solutionName").valueChanges.subscribe(value => {
          this.setState({
            solutionName: value
          });
        });
        this.Form1.get("type").valueChanges.subscribe(value => {
          this.setState({
            type: value !== null ? value : ""
          });
        });
        this.Form1.get("domainserved").valueChanges.subscribe(value => {
          this.setState({
            domainserved: value !== null ? value : ""
          });
        });
        break;
      case 3:
        this.Form2.get("solutionRelavent").valueChanges.subscribe(value => {
          this.setState({
            solutionRelavent: value !== null ? value : ""
          });
        });
        this.Form2.get("technologyStack").valueChanges.subscribe(value => {
          this.setState({
            technologyStack: value !== null ? value : ""
          });
        });
        break;
      case 4:
        this.Form3.get("briefDecription").valueChanges.subscribe(value => {
          this.setState({
            briefDecription: value
          });
        });
        this.Form3.get("challengeSolving").valueChanges.subscribe(value => {
          this.setState({
            challengeSolving: value
          });
        });
        break;
      case 6:
        this.Form4.get("uspsSolution").valueChanges.subscribe(value => {
          this.setState({
            uspsSolution: value
          });
        });
        this.Form4.get("benefite").valueChanges.subscribe(value => {
          this.setState({
            benefite: value
          });
        });
        break;
      case 7:
        this.Form5.get("clientName").valueChanges.subscribe(value => {
          this.setState({
            clientName: value
          });
        });
        this.Form5.get("country").valueChanges.subscribe(value => {
          this.setState({
            country: value
          });
        });
        this.Form5.get("websiteURL").valueChanges.subscribe(value => {
          this.setState({
            websiteURL: value
          });
        });
        break;
      case 8:
        this.Form6.get("sofarImplementation").valueChanges.subscribe(value => {
          if (value !== "." && value !== "e") {
            this.setState({
              sofarImplementation: value
            });
          }
        });
        this.Form6.get("indiaImplementation").valueChanges.subscribe(value => {
          this.setState({
            indiaImplementation: value
          });
        });
        this.Form6.get("timelineImplementation").valueChanges.subscribe(
          value => {
            this.setState({
              timelineImplementation: value
            });
          }
        );
        break;
      case 9:
        this.Form7.get("commercialModel").valueChanges.subscribe(value => {
          this.setState({
            commercialModel: value
          });
        });
        this.Form7.get("futureRoadmap").valueChanges.subscribe(value => {
          this.setState({
            futureRoadmap: value
          });
        });
        break;
      case 11:
        this.Form8.get("providerName").valueChanges.subscribe(value => {
          this.setState({
            providerName: value !== null ? value : ""
          });
        });
        this.Form8.get("references").valueChanges.subscribe(value => {
          this.setState({
            references: value
          });
        });
        this.Form8.get("count").valueChanges.subscribe(value => {
          this.setState({
            count: value
          });
        });
        break;
      default:
    }
  }

  Form1 = FormBuilder.group({
    solutionName: [
      "",
      [
        Validators.required,
        Validators.maxLength(100),
        Validators.pattern("^[A-Za-z0-9 _-]+$")
      ],
      isEmpty
    ],
    type: [[], Validators.required, isEmpty],
    domainserved: [[], Validators.required, isEmpty]
  });

  Form2 = FormBuilder.group({
    solutionRelavent: [[], Validators.required, isEmpty],
    technologyStack: [[]]
  });
  Form3 = FormBuilder.group({
    briefDecription: [
      "",
      [
        Validators.required,
        Validators.maxLength(1000),
        Validators.pattern("^[A-za-z0-9][^!@#$%^&*()_+={}?/<>~]+$")
      ],
      isEmpty
    ],
    challengeSolving: [
      "",
      [
        Validators.required,
        Validators.maxLength(1000),
        Validators.pattern("^[A-za-z0-9][^!@#$%^&*()_+={}?/<>~]+$")
      ],
      isEmpty
    ]
  });
  Form4 = FormBuilder.group({
    uspsSolution: [
      "",
      [
        Validators.required,
        Validators.maxLength(1000),
        Validators.pattern("^[A-za-z0-9][^!@#$%^&*()_+={}?/<>~]+$")
      ],
      isEmpty
    ],
    benefite: [
      "",
      [
        Validators.maxLength(1000),
        Validators.pattern("^[A-za-z0-9][^!@#$%^&*()_+={}?/<>~]+$")
      ]
    ]
  });
  Form5 = FormBuilder.group({
    clientName: [
      "",
      [
        Validators.required,
        Validators.maxLength(50),
        Validators.pattern("^[a-zA-Z0-9 ]+$")
      ],
      isEmpty
    ],
    country: ["Select", Validators.required, isRequired],
    websiteURL: [
      "",
      [
        Validators.required,
        Validators.pattern(
          "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?"
        )
      ],
      isEmpty
    ]
  });
  Form6 = FormBuilder.group({
    sofarImplementation: ["", Validators.required, isEmpty],
    indiaImplementation: ["", Validators.required, isEmpty],
    timelineImplementation: [
      "",
      [Validators.maxLength(50), Validators.pattern("^[a-zA-Z0-9 ]+$")]
    ]
  });
  Form7 = FormBuilder.group({
    commercialModel: [
      "",
      [
        Validators.required,
        Validators.maxLength(1000),
        Validators.pattern("^[A-za-z0-9][^!@#$%^&*()_+={}?/<>~]+$")
      ],
      isEmpty
    ],
    futureRoadmap: [
      "",
      [
        Validators.maxLength(1000),
        Validators.pattern("^[A-za-z0-9][^!@#$%^&*()_+={}?/<>~]+$")
      ]
    ]
  });
  Form8 = FormBuilder.group({
    providerName: [[], Validators.required],
    references: [
      "",
      [Validators.maxLength(50), Validators.pattern("^[a-zA-Z0-9 ]+$")]
    ],
    count: [""]
  });

  onSubmitForm = () => {
    if (this.state.step === 13 && this.props.company) {
      const solutions = [this.state];
      if (this.props.company.spoc) {
        this.props.company.spoc[0].solutions.push(solutions);
        const data = this.props.company;
        store.dispatch(submitForm(data));
        store.dispatch(getDashboardData(data));

        this.setState({ isSubmitted: false });
      }
    }
    this.nextStep();
  };

  handleSubmit = () => {};
  switchChange = operational => {
    this.setState({ operational });
  };

  nextStep = () => {
    const { step } = this.state;
    let val = step + 1;
    this.setState({
      step: step + 1
    });

    if (this.state.providerName) {
      this.addProvider();
    }
    if (step > 1) {
      store.dispatch(solutionDetails(this.state, step));
      store.dispatch(getDashboardData());
    }
  };

  prevStep = () => {
    const { step } = this.state;
    if (step > 1) {
      this.setState({
        step: step - 1
      });
    } else {
      this.setState({
        step: 1
      });
    }
  };
  addClient = () => {
    const client = {
      clientName: this.state.clientName,
      country: this.state.country,
      websiteURL: this.state.websiteURL
    };
    let clients = this.state.clients;
    clients.push(client);
    this.Form5.reset();
    this.setState({
      clientName: "",
      websiteURL: "",
      country: "",
      clients
    });
  };
  addClientOnNext = () => {
    this.addClient();
    this.nextStep();
  };

  addProvider = () => {
    let providerName = this.state.providerName.map(value => {
      return value.label;
    });
    const provider = {
      providerName: providerName,
      references: this.state.references,
      count: this.state.count
    };
    let providers = this.state.providers;
    providers.push(provider);
  };

  fillForm = selectedClient => {
    this.setState({
      clientfill: true
    });
    store.dispatch(solutionDetails(this.state, this.state.step));
    this.Form5.get("clientName").setValue(selectedClient.clientName);
    this.Form5.get("country").setValue(selectedClient.country);
    this.Form5.get("websiteURL").setValue(selectedClient.websiteURL);
  };

  onFileChange(title, uploadingContentKey, mandatory, e) {
    e.preventDefault();
    let file = e.target.files[0];
    this.setState({
      [uploadingContentKey]: file
    });
    let storeValue = store.dispatch(uploadDocument(file, title, mandatory));
    return storeValue;
  }

  displayUploadTag() {
    if (parseInt(document.getElementById("dropDownId").value) === 1) {
      document.getElementById("uploadLink6").className = "uploadLink";
      let uploadLink1 = document.getElementById("dropDownId");
      uploadLink1.remove(uploadLink1.selectedIndex);
    }
    if (parseInt(document.getElementById("dropDownId").value) === 2) {
      document.getElementById("uploadLink7").className = "uploadLink";
      let uploadLink2 = document.getElementById("dropDownId");
      uploadLink2.remove(uploadLink2.selectedIndex);
    }
    if (parseInt(document.getElementById("dropDownId").value) === 3) {
      document.getElementById("uploadLink8").className = "uploadLink";
      let uploadLink3 = document.getElementById("dropDownId");
      uploadLink3.remove(uploadLink3.selectedIndex);
    }
    if (parseInt(document.getElementById("dropDownId").value) === 4) {
      document.getElementById("uploadLink9").className = "uploadLink";
      let uploadLink4 = document.getElementById("dropDownId");
      uploadLink4.remove(uploadLink4.selectedIndex);
    }
  }

  deleteUploadFile = mandatoryValue => {
    this.setState({
      architecturefile: ""
    });
    const doc_id = mandatoryValue
      ? this.props.company.spoc[0].solutions[0].documents &&
        this.props.company.spoc[0].solutions[0].documents.mandatory[0]._id
      : this.props.company.spoc[0].solutions[0].documents &&
        this.props.company.spoc[0].solutions[0].documents.optional[0]._id;
    const mandatory = mandatoryValue;
    const company_id = this.props.company._id;
    const solution_id = this.props.company.spoc[0].solutions[0]._id;
    const spoc_id = this.props.company.spoc[0]._id;
    store.dispatch(
      deleteSolutionDocument(
        doc_id,
        mandatory,
        company_id,
        solution_id,
        spoc_id
      )
    );
  };

  isUploading(uploadingContentKey) {
    let isContentExists = this.props.uploadingContents
      ? this.props.uploadingContents.find(content => {
          return content === uploadingContentKey;
        })
      : false;
    return (
      <div>
        {isContentExists ? <LinearDeterminate></LinearDeterminate> : ""}
      </div>
    );

    // ' {this.state.isSubmitting ?
    //   <LinearDeterminate  />
    // : 'Hello'}'
  }

  clearForm = () => {
    this.setState({
      clientfill: false
    });
    this.Form5.reset();
  };

  checkedSoundValue = checked => {
    this.setState({
      isVoiceEnabled: checked
    });
  };
  createValue = (identifier, name, value) => {
    let obj;
    obj = {};
    obj.label = value;
    obj.value = value;
    let arr = [];
    if (name === "type") {
      arr = this.state.type ? this.state.type : [];
    } else if (name === "domainserved") {
      arr = this.state.domainserved ? this.state.domainserved : [];
    } else if (name === "solutionRelavent") {
      arr = this.state.solutionRelavent ? this.state.solutionRelavent : [];
    } else if (name === "technologyStack") {
      arr = this.state.technologyStack ? this.state.technologyStack : [];
    } else if (name === "providerName") {
      arr = this.state.providerName ? this.state.providerName : [];
    }
    if (arr.length > 0) {
      arr.push(obj);
    } else {
      arr = [obj];
      if (name === "type") {
        this.Form1.get(name).setValue(obj);
      } else if (name === "domainserved") {
        this.Form1.get(name).setValue(obj);
      } else if (name === "solutionRelavent") {
        this.Form2.get(name).setValue(obj);
      } else if (name === "technologyStack") {
        this.Form2.get(name).setValue(obj);
      } else if (name === "providerName") {
        this.Form7.get(name).setValue(obj);
      }
    }
    this.setState({
      [name]: arr
    });
    store.dispatch(postMasterData(identifier, value));
  };
  handleCreate = (name, newValue) => {
    this.setState({
      [name]: newValue
    });
  };
  skip = () => {
    this.nextStep();
  };
  render() {
    const { step } = this.state;
    let index = 0;
    if (
      this.props.dashboardData.spoc[0].solutions &&
      this.props.dashboardData.spoc[0].solutions.length > 0
    ) {
      index = this.props.dashboardData.spoc[0].solutions
        ? this.props.dashboardData.spoc[0].solutions.length - 1
        : "";
    }
    let name = this.props.dashboardData.spoc
      ? this.props.dashboardData.spoc[0].fullName
      : "";
    this.subscribeFormChangeEvent(step);

    return (
      <React.Fragment>
        <Background />
        <div className="landingContainer">
          <Header />
          <TextToVoice />
          <Validator />
          <div className="articleContainer">
            <div
              className={
                this.props.isVoiceEnabledReducer ? "soundOn" : "soundOff"
              }
            >
              <ButtonToggle
                checkedSoundValue={this.checkedSoundValue.bind(this)}
                type="sound"
              />
            </div>
          </div>
          <div className="companyformContainer scrollsolution">
            {
              {
                1: (
                  <React.Fragment>
                    <ReactCSSTransitionGroup
                      transitionName="page"
                      transitionAppear={true}
                      transitionAppearTimeout={1000}
                      transitionEnter={true}
                      transitionLeave={true}
                      transitionLeaveTimeout={1000}
                    >
                      <div className="fieldClassCenter">
                        <ComponentGenerator
                          label="Let's deep dive into your solution now"
                          type="title"
                        />
                      </div>
                    </ReactCSSTransitionGroup>
                    <Navigator
                      nextStep={this.nextStep}
                      prevStep={this.prevStep}
                      handleSubmit={this.handleSubmit}
                      step={this.state.step}
                      onKeyPressEvent={this.onKeyPress}
                    />
                  </React.Fragment>
                ),
                2: (
                  <React.Fragment>
                    <FieldGroup
                      key="Form1"
                      control={this.Form1}
                      render={({ invalid, get }) => (
                        <form className="compny1">
                          <ReactCSSTransitionGroup
                            transitionName="page"
                            transitionAppear={true}
                            transitionAppearTimeout={1000}
                            transitionEnter={true}
                            transitionLeave={true}
                            transitionLeaveTimeout={1000}
                          >
                            <ComponentGenerator
                              className="formTitle"
                              label="Just a solution, or a magic pill? Tell me something more"
                              type="title"
                            />
                            <ComponentGenerator
                              type="input"
                              placeholder="Type Solution Name"
                              name="solutionName"
                              label="Name of the solution your organization provides*"
                              inputType="text"
                              disabled={false}
                              maxlength={100}
                              voiceText="Name of the solution your organization provides"
                              clippyText={
                                "Please enter name of solution you provide"
                              }
                            />
                            <ComponentGenerator
                              type="multiSelect"
                              name="type"
                              label="Can you mention the types of solutions*"
                              inputType="dropdown"
                              identifier="SolutionType"
                              disabled={false}
                              values={this.state.type}
                              dropdownData={
                                this.props.dropdownData.SolutionType
                              }
                              voiceText={
                                this.state.type
                                  ? "You can enter multiple value"
                                  : "Can you mention the types of solutions"
                              }
                              clippyText={"Select or enter your own value"}
                              createValue={this.createValue.bind(this)}
                            />
                            <ComponentGenerator
                              type="multiSelect"
                              name="domainserved"
                              label="Your business solution domain is into*"
                              inputType="dropdown"
                              identifier="SolutionDomain"
                              disabled={false}
                              values={this.state.domainserved}
                              dropdownData={
                                this.props.dropdownData.SolutionDomain
                              }
                              voiceText={
                                this.state.domainserved
                                  ? "You can enter multiple value"
                                  : "Your business solution domain is into"
                              }
                              clippyText={"Select solution domain "}
                              createValue={this.createValue.bind(this)}
                            />
                          </ReactCSSTransitionGroup>
                          <Navigator
                            nextStep={this.nextStep}
                            prevStep={this.prevStep}
                            invalid={invalid}
                            onKeyPressEvent={this.onKeyPress}
                          />
                        </form>
                      )}
                    />
                  </React.Fragment>
                ),
                3: (
                  <React.Fragment>
                    <FieldGroup
                      key="Form2"
                      control={this.Form2}
                      render={({ invalid, get }) => (
                        <div>
                          <form
                            onSubmit={this.handleSubmit}
                            className="solution1"
                          >
                            <FieldGroup
                              key="Form1"
                              control={this.Form1}
                              render={({ invalid, get }) => (
                                <form className=" blur  margin-top-bottom">
                                  <ReactCSSTransitionGroup
                                    transitionName="page"
                                    transitionAppear={true}
                                    transitionAppearTimeout={1000}
                                    transitionLeave={true}
                                  >
                                    <ComponentGenerator
                                      type="multiSelect"
                                      name="domainserved"
                                      label="Your business solutions domain is into*"
                                      inputType="dropdown"
                                      identifier="SolutionDomain"
                                      disabled={false}
                                      values={this.state.domainserved}
                                      dropdownData={
                                        this.props.dropdownData.SolutionDomain
                                      }
                                      voiceText={
                                        this.state.domainserved
                                          ? "You can enter multiple value"
                                          : "Your business solutions domain is into"
                                      }
                                      createValue={this.createValue.bind(this)}
                                    />
                                  </ReactCSSTransitionGroup>
                                </form>
                              )}
                            />
                            <ReactCSSTransitionGroup
                              transitionName="page"
                              transitionAppear={true}
                              transitionAppearTimeout={5000}
                              transitionEnter={true}
                              transitionLeave={true}
                            >
                              <ComponentGenerator
                                type="multiSelect"
                                name="solutionRelavent"
                                label="The business solution is relevant for*"
                                inputType="dropdown"
                                disabled={false}
                                identifier="SolutionRelevance"
                                values={this.state.solutionRelavent}
                                dropdownData={
                                  this.props.dropdownData.SolutionRelevance
                                }
                                onFocus={true}
                                voiceText={
                                  this.state.solutionRelavent
                                    ? "You can enter multiple value"
                                    : "The business solution is relevant for"
                                }
                                clippyText={"Select or enter your own value"}
                                createValue={this.createValue.bind(this)}
                              />
                              <ComponentGenerator
                                type="multiSelect"
                                placeholder="Domain served"
                                name="technologyStack"
                                label="Which technology stack does this solution belong to?"
                                inputType="dropdown"
                                disabled={false}
                                identifier="SolutionTechnologyStack"
                                values={this.state.technologyStack}
                                dropdownData={
                                  this.props.dropdownData
                                    .SolutionTechnologyStack
                                }
                                voiceText={
                                  this.state.technologyStack
                                    ? "You can enter multiple value"
                                    : "Which technology stack does this solution belong to?"
                                }
                                clippyText={"Select technology stack"}
                                createValue={this.createValue.bind(this)}
                              />
                            </ReactCSSTransitionGroup>
                            <Navigator
                              nextStep={this.nextStep}
                              prevStep={this.prevStep}
                              invalid={invalid}
                              onKeyPressEvent={this.onKeyPress}
                            />
                          </form>
                        </div>
                      )}
                    />
                  </React.Fragment>
                ),
                4: (
                  <FieldGroup
                    key="Form3"
                    control={this.Form3}
                    render={({ invalid, get }) => (
                      <>
                        <form
                          onSubmit={this.handleSubmit}
                          className="textarewrapper"
                        >
                          <FieldGroup
                            key="Form2"
                            control={this.Form2}
                            render={({ invalid, get }) => (
                              <form
                                onSubmit={this.handleSubmit}
                                className="solution1 blur"
                              >
                                <ReactCSSTransitionGroup
                                  transitionName="page"
                                  transitionAppear={true}
                                  transitionAppearTimeout={5000}
                                  transitionEnter={true}
                                  transitionLeave={true}
                                >
                                  <ComponentGenerator
                                    type="multiSelect"
                                    placeholder="Domain served"
                                    name="technologyStack"
                                    label="Which technology stack does this solution belong to?"
                                    inputType="dropdown"
                                    disabled={false}
                                    identifier="SolutionTechnologyStack"
                                    values={this.state.technologyStack}
                                    dropdownData={
                                      this.props.dropdownData
                                        .SolutionTechnologyStack
                                    }
                                    voiceText={
                                      this.state.technologyStack
                                        ? "You can enter multiple value"
                                        : "Which technology stack does this solution belong to?"
                                    }
                                    createValue={this.createValue.bind(this)}
                                  />
                                </ReactCSSTransitionGroup>
                              </form>
                            )}
                          />
                          <ReactCSSTransitionGroup
                            transitionName="page"
                            transitionAppear={true}
                            transitionAppearTimeout={5000}
                            transitionEnter={true}
                            transitionLeave={true}
                          >
                            <ComponentGenerator
                              type="textArea"
                              name="briefDecription"
                              label="Can you share a brief description about your solution?*"
                              placeholder="Solution description"
                              inputType="text"
                              disabled={false}
                              values={
                                this.props.commonReducer.voiceToText &&
                                this.props.commonReducer.name ===
                                  "briefDecription"
                                  ? this.props.commonReducer.voiceToText
                                  : ""
                              }
                              voiceText="Can you share a brief description about your solution?"
                              clippyText={`Allowed special characters are  (- : ; " ' , .) `}
                              onFocus={true}
                            />
                            <ComponentGenerator
                              type="textArea"
                              name="challengeSolving"
                              label="What are the current challenges you are facing? What strategies have you adopted to solve those challenges?*"
                              placeholder="Challenges solved"
                              inputType="text"
                              disabled={false}
                              voiceText="What are the current challenges you are facing? What strategies have you adopted to solve those challenges?"
                              clippyText={`Allowed special characters are  (- : ; " ' , .) `}
                            />
                          </ReactCSSTransitionGroup>
                          <Navigator
                            nextStep={this.nextStep}
                            prevStep={this.prevStep}
                            handleSubmit={this.handleSubmit}
                            invalid={
                              invalid ||
                              this.Form3.value.briefDecription.match(
                                "[^!@#$%^&*()_+={}?/<>~]+$"
                              ) === null ||
                              this.Form3.value.challengeSolving.match(
                                "[^!@#$%^&*()_+={}?/<>~]+$"
                              ) === null
                                ? true
                                : false
                            }
                            onKeyPressEvent={this.onKeyPress}
                          />
                        </form>
                      </>
                    )}
                  />
                ),
                5: (
                  <React.Fragment>
                    <ReactCSSTransitionGroup
                      transitionName="page"
                      transitionAppear={true}
                      transitionAppearTimeout={5000}
                      transitionEnter={true}
                      transitionLeave={true}
                    >
                      <div className="marginTop">
                        <ComponentGenerator
                          label={
                            "Phew!! That was long. You are almost there " +
                            name +
                            "!"
                          }
                          type="title"
                        />
                      </div>
                    </ReactCSSTransitionGroup>
                    <Navigator
                      nextStep={this.nextStep}
                      prevStep={this.prevStep}
                      handleSubmit={this.handleSubmit}
                      onKeyPressEvent={this.onKeyPress}
                      // invalid={invalid}
                    />
                  </React.Fragment>
                ),
                6: (
                  <FieldGroup
                    key="Form4"
                    control={this.Form4}
                    render={({ invalid, get }) => (
                      <form
                        onSubmit={this.handleSubmit}
                        className="textarewrapper"
                      >
                        <FieldGroup
                          key="Form3"
                          control={this.Form3}
                          render={({ invalid, get }) => (
                            <form
                              onSubmit={this.handleSubmit}
                              className="blur margin-top"
                            >
                              <ReactCSSTransitionGroup
                                transitionName="page"
                                transitionAppear={true}
                                transitionAppearTimeout={5000}
                                transitionEnter={true}
                                transitionLeave={true}
                              >
                                <ComponentGenerator
                                  type="textArea"
                                  name="challengeSolving"
                                  label="What are the current challenges you are facing? What strategies have you adopted to solve those challenges?*"
                                  placeholder="Challenges solved"
                                  inputType="text"
                                  disabled={false}
                                  voiceText="What are the current challenges you are facing? What strategies have you adopted to solve those challenges?"
                                />
                              </ReactCSSTransitionGroup>
                            </form>
                          )}
                        />
                        <ReactCSSTransitionGroup
                          transitionName="page"
                          transitionAppear={true}
                          transitionAppearTimeout={5000}
                          transitionEnter={true}
                          transitionLeave={true}
                        >
                          <ComponentGenerator
                            label="I am sure you have hit a home run! This section is going to make you proud, can you tell me something more?"
                            type="title"
                            clippyText={`You can also give voice command to fill this field `}
                          />
                          <ComponentGenerator
                            type="textArea"
                            name="uspsSolution"
                            label="What are the strength of your USPs and how has it helped in creating a value proposition?*"
                            placeholder="Type USP here"
                            inputType="text"
                            disabled={false}
                            voiceText="What are the strength of your USPs and how has it helped in creating a value proposition?"
                            clippyText={`Allowed special characters are  (- : ; " ' , .) `}
                          />
                          <ComponentGenerator
                            type="textArea"
                            name="benefite"
                            label="How is it going to impact your business? How can BFL benefit from your solutions?"
                            placeholder="Type here"
                            inputType="text"
                            disabled={false}
                            voiceText="How is it going to impact your business? How can BFL benefit from your solutions?"
                            clippyText={`Allowed special characters are  (- : ; " ' , .) `}
                          />
                        </ReactCSSTransitionGroup>
                        <Navigator
                          nextStep={this.nextStep}
                          prevStep={this.prevStep}
                          handleSubmit={this.handleSubmit}
                          invalid={
                            invalid ||
                            this.Form4.value.uspsSolution.match(
                              "[^!@#$%^&*()_+={}?/<>~]+$"
                            ) === null ||
                            (this.Form4.value.benefite
                              ? this.Form4.value.benefite.match(
                                  "[^!@#$%^&*()_+={}?/<>~]+$"
                                ) === null
                              : false)
                              ? true
                              : false
                          }
                          onKeyPressEvent={this.onKeyPress}
                        />
                      </form>
                    )}
                  />
                ),
                7: (
                  <React.Fragment>
                    <FieldGroup
                      key="Form5"
                      control={this.Form5}
                      render={({ invalid, get }) => (
                        <form
                          onSubmit={this.handleSubmit}
                          className="textarewrapper"
                        >
                          <FieldGroup
                            key="Form4"
                            control={this.Form4}
                            render={({ invalid, get }) => (
                              <form
                                onSubmit={this.handleSubmit}
                                className="blur margin-top"
                              >
                                <ReactCSSTransitionGroup
                                  transitionName="page"
                                  transitionAppear={true}
                                  transitionAppearTimeout={5000}
                                  transitionEnter={true}
                                  transitionLeave={true}
                                >
                                  <ComponentGenerator
                                    type="textArea"
                                    name="benefite"
                                    label="How is it going to impact your business? How can BFL benefit from your solutions?? "
                                    placeholder="Type here"
                                    inputType="text"
                                    disabled={false}
                                    voiceText="How is it going to impact your business? How can BFL benefit from your solutions?"
                                  />
                                </ReactCSSTransitionGroup>
                              </form>
                            )}
                          />
                          <ReactCSSTransitionGroup
                            transitionName="page"
                            transitionAppear={true}
                            transitionAppearTimeout={5000}
                            transitionEnter={true}
                            transitionLeave={true}
                          >
                            <ComponentGenerator
                              className="formTitle"
                              label="Throw some bunch of names to impress us. "
                              type="title"
                            />
                            <div className="fieldClass partnerTab">
                              {this.state.clients.map(value => {
                                if (
                                  this.state.clientName !== value.clientName
                                ) {
                                  return (
                                    <Button
                                      key={value.clientName}
                                      onClick={this.fillForm.bind(this, value)}
                                    >
                                      {value.clientName}
                                      <span className="icon-arrowhead-pointing-to-the-right icon"></span>
                                    </Button>
                                  );
                                } else {
                                  return (
                                    <Button
                                      key={value.clientName}
                                      onClick={this.clearForm.bind(this)}
                                    >
                                      {value.clientName}
                                      <span className="icon-sort-down icon"></span>
                                    </Button>
                                  );
                                }
                              })}
                            </div>
                            <ComponentGenerator
                              type="input"
                              name="clientName"
                              label="Name of the organization your client works for*"
                              inputType="text"
                              disabled={false}
                              placeholder="Organization Name"
                              voiceText="Name of the organization your client works for"
                              clippyText={
                                "Only alphanumeric values (a-z, A-Z, 0-9) are allowed"
                              }
                            />
                            <ComponentGenerator
                              type="dropdown"
                              name="country"
                              label="Your client's organization is headquartered at*"
                              inputType="dropdown"
                              disabled={false}
                              placeholder="Select"
                              values={this.Form5.value.country}
                              dropdownData={this.props.dropdownData.Country}
                              voiceText="Your client's organization is headquartered at"
                              clippyText={
                                "Your client's organization headquarter"
                              }
                            />
                            <ComponentGenerator
                              type="input"
                              placeholder="Website URL"
                              name="websiteURL"
                              label="Can you provide the website URL of your client's organization?*"
                              inputType="text"
                              disabled={false}
                              maxlength={100}
                              voiceText="Can you provide the website URL of your client's organization?"
                              clippyText={"Please enter valid URL"}
                            />
                            <div className="morebtn">
                              <Button
                                onClick={this.addClient}
                                disabled={
                                  this.state.clientfill === true
                                    ? true
                                    : invalid
                                }
                              >
                                <span className="icon-addition-sign icon"></span>
                                {this.state.clients.length > 0 ||
                                (this.props.dashboardData.spoc[0] &&
                                  this.props.dashboardData.spoc[0].solutions[
                                    index
                                  ].clients &&
                                  this.props.dashboardData.spoc[0].solutions[
                                    index
                                  ].clients.length > 0)
                                  ? "Add more client"
                                  : " Add client"}
                              </Button>
                            </div>
                          </ReactCSSTransitionGroup>
                          <Navigator
                            nextStep={
                              (this.state.clientName &&
                                this.state.country !== "Select" &&
                                this.state.websiteURL) !== ""
                                ? this.addClientOnNext
                                : this.nextStep
                            }
                            prevStep={this.prevStep}
                            handleSubmit={this.handleSubmit}
                            onKeyPressEvent={this.onKeyPress}
                            invalid={
                              this.state.clients.length > 0 || !invalid
                                ? false
                                : true
                            }
                          />
                        </form>
                      )}
                    />
                  </React.Fragment>
                ),
                8: (
                  <React.Fragment>
                    <FieldGroup
                      key="Form6"
                      control={this.Form6}
                      render={({ invalid, get }) => (
                        <form
                          onSubmit={this.handleSubmit}
                          className="marginTop5"
                        >
                          <FieldGroup
                            key="Form5"
                            control={this.Form5}
                            render={({ invalid, get }) => (
                              <form
                                onSubmit={this.handleSubmit}
                                className="blur margin-top-5"
                              >
                                <ReactCSSTransitionGroup
                                  transitionName="page"
                                  transitionAppear={true}
                                  transitionAppearTimeout={5000}
                                  transitionEnter={true}
                                  transitionLeave={true}
                                >
                                  <ComponentGenerator
                                    type="input"
                                    placeholder="Type website URL"
                                    name="websiteURL"
                                    label="Can you provide the website URL of your client's organization?*"
                                    inputType="text"
                                    disabled={false}
                                    maxlength={100}
                                    voiceText="Can you provide the website URL of your client's organization?"
                                  />
                                  <div className="morebtn">
                                    <Button
                                      onClick={this.addClient}
                                      disabled={
                                        this.state.clientfill === true
                                          ? true
                                          : invalid
                                      }
                                    >
                                      <span className="icon-addition-sign icon"></span>
                                      {this.state.clients.length > 0 ||
                                      (this.props.dashboardData.spoc[0] &&
                                        this.props.dashboardData.spoc[0]
                                          .solutions[index].clients &&
                                        this.props.dashboardData.spoc[0]
                                          .solutions[index].clients.length > 0)
                                        ? "Add more client"
                                        : " Add client"}
                                    </Button>
                                  </div>
                                </ReactCSSTransitionGroup>
                              </form>
                            )}
                          />
                          <ReactCSSTransitionGroup
                            transitionName="page"
                            transitionAppear={true}
                            transitionAppearTimeout={5000}
                            transitionEnter={true}
                            transitionLeave={true}
                          >
                            <ComponentGenerator
                              className="formTitle"
                              label="I was wondering if you can brag a bit more "
                              type="title"
                            />
                            <ComponentGenerator
                              type="number"
                              name="sofarImplementation"
                              label="Number of implementations done so far*"
                              inputType="number"
                              decimalScale={0}
                              disabled={false}
                              placeholder="0000"
                              maxlength={4}
                              voiceText="Number of implementations done so far"
                              clippyText={"Only numbers (0-9) are allowed"}
                            />
                            <ComponentGenerator
                              type="number"
                              name="indiaImplementation"
                              label="Total number of implementations done in India*"
                              inputType="number"
                              disabled={false}
                              decimalScale={0}
                              placeholder="0000"
                              voiceText="Total number of implementations done in India"
                              clippyText={"Only numbers (0-9) are allowed"}
                            />
                            <ComponentGenerator
                              type="input"
                              placeholder="Type implementation timeline "
                              name="timelineImplementation"
                              label="Total implementation timeline ( in months) "
                              inputType="text"
                              disabled={false}
                              maxlength={100}
                              voiceText="Total implementation timeline ( in months)"
                              clippyText={
                                "Only alphanumeric values (a-z, A-Z, 0-9) are allowed"
                              }
                            />
                          </ReactCSSTransitionGroup>
                          <Navigator
                            nextStep={this.nextStep}
                            prevStep={this.prevStep}
                            handleSubmit={this.handleSubmit}
                            invalid={invalid}
                            onKeyPressEvent={this.onKeyPress}
                          />
                        </form>
                      )}
                    />
                  </React.Fragment>
                ),
                9: (
                  <FieldGroup
                    key="Form7"
                    control={this.Form7}
                    render={({ invalid, get }) => (
                      <form
                        onSubmit={this.handleSubmit}
                        className="textarewrapper"
                      >
                        <FieldGroup
                          key="Form6"
                          control={this.Form6}
                          render={({ invalid, get }) => (
                            <form
                              onSubmit={this.handleSubmit}
                              className="blur "
                            >
                              <ReactCSSTransitionGroup
                                transitionName="page"
                                transitionAppear={true}
                                transitionAppearTimeout={5000}
                                transitionEnter={true}
                                transitionLeave={true}
                              >
                                <ComponentGenerator
                                  type="input"
                                  placeholder="Type implementation timeline "
                                  name="timelineImplementation"
                                  label="Type implementation timeline (In Months) "
                                  inputType="text"
                                  disabled={false}
                                  maxlength={100}
                                  voiceText="Type implementation timeline"
                                />
                              </ReactCSSTransitionGroup>
                            </form>
                          )}
                        />
                        <ReactCSSTransitionGroup
                          transitionName="page"
                          transitionAppear={true}
                          transitionAppearTimeout={5000}
                          transitionEnter={true}
                          transitionLeave={true}
                        >
                          <ComponentGenerator
                            label="You're just a few more details away."
                            type="title"
                            clippyText={
                              "You can also give voice command to fill this field"
                            }
                          />
                          <ComponentGenerator
                            type="textArea"
                            name="commercialModel"
                            label="Could you please mention the types of commercial models?*"
                            placeholder="Type commercial model"
                            inputType="text"
                            disabled={false}
                            voiceText="Could you please mention the types of commercial models?"
                            clippyText={`Allowed special characters are  (- : ; " ' , .) `}
                          />
                          <ComponentGenerator
                            type="textArea"
                            name="futureRoadmap"
                            label="I would be interested to know the future roadmap of your solution. Could you talk about it? "
                            placeholder="Type future roadmap here"
                            inputType="text"
                            disabled={false}
                            voiceText="I would be interested to know the future roadmap of your solution. Could you talk about it?"
                            clippyText={`Allowed special characters are  (- : ; " ' , .) `}
                          />
                        </ReactCSSTransitionGroup>
                        <Navigator
                          nextStep={this.nextStep}
                          prevStep={this.prevStep}
                          handleSubmit={this.handleSubmit}
                          invalid={
                            invalid ||
                            this.Form7.value.commercialModel.match(
                              "[^!@#$%^&*()_+={}?/<>~]+$"
                            ) === null ||
                            (this.Form7.value.futureRoadmap
                              ? this.Form7.value.futureRoadmap.match(
                                  "[^!@#$%^&*()_+={}?/<>~]+$"
                                ) === null
                              : false)
                              ? true
                              : false
                          }
                          onKeyPressEvent={this.onKeyPress}
                        />
                      </form>
                    )}
                  />
                ),
                10: (
                  <React.Fragment>
                    <ReactCSSTransitionGroup
                      transitionName="page"
                      transitionAppear={true}
                      transitionAppearTimeout={5000}
                      transitionEnter={true}
                      transitionLeave={true}
                    >
                      <div className="marginTop">
                        <ComponentGenerator
                          label="We need to be a lil more Energetic and high-spirited. Some more details please.
                          Tired! You can skip this section."
                          type="title"
                        />
                      </div>
                    </ReactCSSTransitionGroup>
                    <Navigator
                      nextStep={this.nextStep}
                      prevStep={this.prevStep}
                      handleSubmit={this.handleSubmit}
                      onKeyPressEvent={this.onKeyPress}
                    />
                  </React.Fragment>
                ),
                11: (
                  <React.Fragment>
                    <FieldGroup
                      key="Form8"
                      control={this.Form8}
                      render={({ invalid, get }) => (
                        <form
                          onSubmit={this.handleSubmit}
                          className="textarewrapper"
                        >
                          <FieldGroup
                            key="Form7"
                            control={this.Form7}
                            render={({ invalid, get }) => (
                              <form
                                onSubmit={this.handleSubmit}
                                className="blur margin-top"
                              >
                                <ReactCSSTransitionGroup
                                  transitionName="page"
                                  transitionAppear={true}
                                  transitionAppearTimeout={5000}
                                  transitionEnter={true}
                                  transitionLeave={true}
                                >
                                  <ComponentGenerator
                                    type="textArea"
                                    name="futureRoadmap"
                                    label="I would be interested to know the future roadmap of your solution. Could you talk about it? "
                                    placeholder="Type future roadmap here"
                                    inputType="text"
                                    disabled={false}
                                    voiceText="I would be interested to know the future roadmap of your solution. Could you talk about it?"
                                  />
                                </ReactCSSTransitionGroup>
                              </form>
                            )}
                          />
                          <ReactCSSTransitionGroup
                            transitionName="page"
                            transitionAppear={true}
                            transitionAppearTimeout={5000}
                            transitionEnter={true}
                            transitionLeave={true}
                          >
                            {/* <ComponentGenerator
                              className="formTitle"
                              label="“The cloud services is for everyone. The cloud is a democracy.” - Marc Benioff, Founder, CEO and Chairman of Salesforce."
                              type="title"
                            /> */}
                            <ComponentGenerator
                              className="formTitle"
                              label="Can you provide some details about the cloud support in your organization?"
                              type="title"
                              clippyText={"This is optional, you can skip this"}
                            />
                            <ComponentGenerator
                              type="multiSelect"
                              name="providerName"
                              label="Can you mention the name of your cloud provider?*"
                              inputType="dropdown"
                              disabled={false}
                              identifier="SolutionCloudProvider"
                              values={this.state.providerName}
                              dropdownData={
                                this.props.dropdownData.SolutionCloudProvider
                              }
                              voiceText={
                                this.state.providerName
                                  ? "You can enter multiple value"
                                  : "Can you mention the name of your cloud provider?"
                              }
                              clippyText={"Throw some light on cloud providers"}
                              createValue={this.createValue.bind(this)}
                            />
                            <ComponentGenerator
                              type="input"
                              placeholder="Enter names"
                              name="references"
                              label="Do you have any references?"
                              inputType="text"
                              disabled={false}
                              maxlength={100}
                              voiceText="Do you have any references?"
                              clippyText={"Any references"}
                            />
                            <ComponentGenerator
                              type="number"
                              name="count"
                              label="What is the number of cloud deployment?"
                              inputType="number"
                              decimalScale={0}
                              disabled={false}
                              placeholder="0000"
                              voiceText="What is the number of cloud deployment?"
                              clippyText={"Only numbers (0-9) are allowed"}
                            />
                            <div className="skip">
                              <Button
                                label="Skip"
                                type="title"
                                onClick={this.skip.bind(this)}
                                disabled={invalid ? false : true}
                              >
                                Skip
                              </Button>
                            </div>
                          </ReactCSSTransitionGroup>
                          <Navigator
                            nextStep={this.nextStep}
                            prevStep={this.prevStep}
                            invalid={invalid}
                            handleSubmit={this.handleSubmit}
                            onKeyPressEvent={this.onKeyPress}
                          />
                        </form>
                      )}
                    />
                  </React.Fragment>
                ),
                12: (
                  <React.Fragment>
                    <ReactCSSTransitionGroup
                      transitionName="page"
                      transitionAppear={true}
                      transitionAppearTimeout={5000}
                      transitionEnter={true}
                      transitionLeave={true}
                    >
                      <ComponentGenerator
                        className="formTitle"
                        label="Any solutions in mind? Go ahead and upload!"
                        type="title"
                        clippyText={"Only jpg, png & pdf files allowed"}
                      />
                      <div className="uploadContainer">
                        <div className="uploadLink">
                          <div className="uploadLocation">
                            <ComponentGenerator
                              type="file"
                              placeholder="Upload"
                              name="architecturefile"
                              mandatory={true}
                              values="Solution Architecture*"
                              inputType="text"
                              disabled={false}
                              file={this.state.architecturefile}
                              maxlength={100}
                              deleteUploadFile={this.deleteUploadFile}
                              onFileChange={this.onFileChange.bind(this)}
                            />

                            {this.isUploading("solution-architecture")}
                          </div>
                          <span className="icon-upload"></span>
                        </div>
                        <div className="uploadTitle">
                          <div className="uploadTitle">
                            <div className="fieldClass">
                              <h2>Select and Upload</h2>
                            </div>
                          </div>
                        </div>
                        <div id="uploadLink6" className="uploadLink none">
                          <div className="uploadLocation">
                            <ComponentGenerator
                              type="file"
                              placeholder="Upload"
                              name="demoFile"
                              values="Product Sheet"
                              inputType="text"
                              disabled={false}
                              mandatory={false}
                              maxlength={100}
                              uploadLink="uploadLink6"
                              deleteUploadFile={this.deleteUploadFile}
                              onFileChange={this.onFileChange.bind(this)}
                            />

                            {this.isUploading("demo-video")}
                          </div>
                          <span className="icon-upload"></span>
                        </div>
                        <div id="uploadLink7" className="uploadLink none">
                          <div className="uploadLocation">
                            <ComponentGenerator
                              type="file"
                              placeholder="Upload"
                              name="demoFile1"
                              values="Other"
                              inputType="text"
                              disabled={false}
                              maxlength={100}
                              mandatory={false}
                              uploadLink="uploadLink7"
                              deleteUploadFile={this.deleteUploadFile}
                              onFileChange={this.onFileChange.bind(this)}
                            />

                            {this.isUploading("demo-video")}
                          </div>
                          <span className="icon-upload"></span>
                        </div>
                        {/* <div id="uploadLink8" className="uploadLink none">
                          <div className="uploadLocation">
                            <ComponentGenerator
                              type="file"
                              placeholder="Upload"
                              name="demoFile2"
                              values="Demo Video 3"
                              inputType="text"
                              disabled={false}
                              maxlength={100}
                              mandatory={false}
                              uploadLink="uploadLink8"
                              deleteUploadFile={this.deleteUploadFile}
                              onFileChange={this.onFileChange.bind(this)}
                            />

                            {this.isUploading("demo-video")}
                          </div>
                          <span className="icon-upload"></span>
                        </div>
                        <div id="uploadLink9" className="uploadLink none">
                          <div className="uploadLocation">
                            <ComponentGenerator
                              type="file"
                              placeholder="Upload"
                              name="demoFile3"
                              values="Demo Video 4"
                              inputType="text"
                              disabled={false}
                              maxlength={100}
                              mandatory={false}
                              uploadLink="uploadLink9"
                              deleteUploadFile={this.deleteUploadFile}
                              onFileChange={this.onFileChange.bind(this)}
                            />

                            {this.isUploading("demo-video")}
                          </div>
                          <span className="icon-upload"></span>
                        </div> */}
                        <div className="uploadLinkdropdown">
                          <div className="uploadLocation">
                            <select
                              id="dropDownId"
                              onChange={this.displayUploadTag}
                              value={this.value}
                            >
                              <option selected="selected">Select</option>
                              <option value="1">Product Sheet</option>
                              <option value="2">other</option>
                              {/* <option value="3">Demo Video 3</option>
                              <option value="4">Demo Video 4</option> */}
                            </select>
                          </div>
                        </div>
                      </div>
                    </ReactCSSTransitionGroup>
                    <Navigator
                      nextStep={this.nextStep}
                      prevStep={this.prevStep}
                      handleSubmit={this.handleSubmit}
                      invalid={
                        this.state.architecturefile &&
                        this.state.architecturefile !== ""
                          ? false
                          : true
                      }
                      onKeyPressEvent={this.onKeyPress}
                    />
                  </React.Fragment>
                ),
                13: (
                  <React.Fragment>
                    <div className="widthSunmit">
                      <ReactCSSTransitionGroup
                        transitionName="page"
                        transitionAppear={true}
                        transitionAppearTimeout={5000}
                        transitionEnter={true}
                        transitionLeave={true}
                      >
                        <ComponentGenerator
                          label="Now that you have arrived at your destination. I will help you submit your application for further process."
                          type="title"
                        />
                        <div className="submitbtn">
                          <Button onClick={this.onSubmitForm}>Submit</Button>
                        </div>
                      </ReactCSSTransitionGroup>
                      <div className="stepButtons">
                        <Button type="button" onClick={this.prevStep}>
                          <span className="icon-prev"></span>
                        </Button>
                      </div>
                    </div>
                  </React.Fragment>
                ),
                14: (
                  <ReactCSSTransitionGroup
                    transitionName="page"
                    transitionAppear={true}
                    transitionAppearTimeout={5000}
                    transitionEnter={true}
                    transitionLeave={true}
                  >
                    <ComponentGenerator
                      label="Congratulations! Your application has been submitted successfully. Redirecting you to your dashboard......"
                      type="title"
                      redirect={true}
                      state={this.state}
                    />
                  </ReactCSSTransitionGroup>
                ),
                15: (
                  <Redirect
                    to={{
                      pathname: "/dashboard"
                    }}
                  />
                )
              }[step]
            }
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    company: state.registerSpocReducer.data,
    dropdownData: state.masterDataReducer.data,
    dashboardData: state.dashboardDataReducer.data,
    isVoiceEnabledReducer: state.commonReducer.isVoiceEnabled,
    commonReducer: state.commonReducer
  };
};

export default connect(mapStateToProps)(SolutionForm);
