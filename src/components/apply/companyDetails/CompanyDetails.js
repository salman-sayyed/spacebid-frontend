import React, { Component } from "react";
import "./WhySpaceBid.scss";
import LeftSideDrawer from "../../common/leftSideDrawer/LeftSideDrawer";
import ButtonToggle from "../common/buttonToggle/ButtonToggle";
import Stars from "../common/stars/Stars";
import "../common/stars/Stars.scss";

class CompanyName extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="whySpaceBid">
        <div
          className={
            this.state.isDarkThemeEnabled ? "dark-theme" : "light-theme"
          }
        >
          <nav>
            <LeftSideDrawer />
            <ButtonToggle checkedValue={this.checkedValue} type="theme" />
          </nav>
          <Stars />
          <section className="section1">
            <div className="titleText">
              <h2>Let’s Untangle SpaceBid</h2>
              <span>
                BFL (Bajaj Finance Ltd.) is hunting top-notch solutions that are
                real-world problem-solvers. If you as a venture have
                out-of-the-box solutions, you are invited to apply. If you as a
                venture think like there’s no box – even better.
              </span>
            </div>
            <div className="rocket" />
            <div className="topGrandient" />
            <div className="planet" />
            <div className="rotatedBackground" />
            <div className="nightCloud" />
          </section>
          <section className="section2">
            <div className="infoText">
              <h2>
                If your startup can shoot well, we have got enough{" "}
                <span>firepower for you.</span>{" "}
              </h2>
              <p>
                Do you identify yourself with any of these words: cutting-edge,
                disruptive, unconventional, unorthodox, innovative? If yes, then
                you should have already applied by yesterday.
              </p>
              <p>
                SpaceBid is the next-level platform to showcase your ability to
                the world…time for some limelight perh{" "}
              </p>
              <p>
                A handful of best-in-class solutions stand a chance to get
                deployed at an NBFC giant BFL, which is home to some 20,000+
                employees and 1400+ branches. Pumped up already?{" "}
              </p>
              <h4> Pumped up already? </h4>
            </div>
            <div className="infoImage">
              {/* <img src={require('../../assets/images/man-speaking.png')}></img> */}
            </div>
          </section>
          <section className="section3">
            <div className="instructTitle">
              <h2>
                The Easy Four-Step <span>Journey</span>{" "}
              </h2>
            </div>
            <div className="instructs1">
              <h6>ENGAGEMENT & EVALUATION</h6>
              <p>
                All the details around solution(s) deployment, delivery &
                support will be discussed.
              </p>
            </div>
            <div className="instructs2">
              <h6>CONVERSIONS & ROLLOUTS</h6>
              <p>
                If the solution(s) is selected, the internal stakeholders will
                engage with you on a detailed plan for a rollout.
              </p>
            </div>
            <div className="instructs3">
              <h6>PRE- EVALUATION</h6>
              <p>
                A preliminary screening of the solution(s) proposed by startups.
              </p>
            </div>
            <div className="instructs4">
              <h6>ASSESSMENT & TICC DEPLOYMENT</h6>
              <p>
                Upon a thorough assessment of the proposed solution, PoC &
                Prototyping of the solution will kick-in.
              </p>
            </div>
          </section>
          <section className="section4">
            <div className="optionTitle">
              <h2>
                Your<span>Takeaways</span>{" "}
              </h2>
            </div>
            <div className="cardContainer">
              <div className="cardItems">
                <div className="card"></div>
                <div className="card"></div>
                <div className="card"></div>
              </div>
              <div className="cardItems">
                <div className="card"></div>
                <div className="card"></div>
                <div className="card"></div>
              </div>
            </div>
          </section>
          <footer className="footer">
            <h6>
              Need Help ? <span>Contact us</span>
            </h6>
            <div className="girlImage">
              <img src={require("../../assets/images/girl2.svg")}></img>
            </div>
            <div className="twiterImage">
              <img src={require("../../assets/images/twittericon.svg")}></img>
            </div>
            <div className="facebookImage">
              <img src={require("../../assets/images/facebookicon.svg")}></img>
            </div>
          </footer>
        </div>
      </div>
    );
  }
}

export default CompanyName;
