import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import Welcome from "./components/landing/Welcome";
import Landing from "./components/landing/Landing";
import Introduction from "./components/introduction/Introduction";
import LearnAboutTIC from "./components/learnAboutTIC/LearnAboutTIC";
import VirtualFist from "./components/landing/VirtualFist";
import WhySpaceBid from "./components/whySpaceBid/WhySpaceBid";
import Dashboard from "./components/dashboard/Dashboard";
import LoginForm from "./components/loginForm/LoginForm";
import SolutionDetail from "./components/dashboard/SolutionDetail";
import PersonalDetailForm from "./components/form/PersonalDetailForm";
import DisplaySuccessMessage from "./components/common/formDetails/displaySuccessMessage/DisplaySuccessMessage";
import CompanyFormDetail from "./components/form/companyForm/CompanyFormDetail";
import SolutionForm from "./components/form/solutionForm/SolutionForm";
import Query from "./components/query/Query";
class Main extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.location.state === "desiredState") {
      debugger;

      this.props.history.push(nextProps.location.pathname);
    }
  }
  render() {
    return (
      <main>
        <Switch>
          <div>
            <Route exact path="/" component={Landing} />
            <Route exact path="/landing" component={Landing} />
            <Route path="/welcome" component={Welcome} />
            <Route path="/virtual-fist" component={VirtualFist} />
            <Route path="/introduction" component={Introduction} />
            <Route path="/learn-about-TICC" component={LearnAboutTIC} />
            <Route path="/why-space-bid" component={WhySpaceBid} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/login-form" component={LoginForm} />
            <Route path="/solution-detail" component={SolutionDetail} />
            <Route
              path="/personal-detail-form"
              component={PersonalDetailForm}
            />
            <Route
              path="/display-success-message"
              component={DisplaySuccessMessage}
            />
            {/* <Route path={"/company-form" | "/company-form/page=3" |"/company-form/page=4" |"/company-form/page=6" |"/company-form/page=7"  } component={CompanyFormDetail} /> */}
            <Route path="/company-form" component={CompanyFormDetail} />
            <Route path="/solution-form" component={SolutionForm} />
            <Route path="/query" component={Query} />
          </div>
        </Switch>
      </main>
    );
  }
}
export default Main;
